-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2019 at 08:36 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tickets`
--

-- --------------------------------------------------------

--
-- Table structure for table `airlines`
--

CREATE TABLE `airlines` (
  `id` bigint(20) NOT NULL,
  `airline_short_code` text DEFAULT NULL,
  `airline_name` text DEFAULT NULL,
  `status` enum('Draft','Published') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `airlines`
--

INSERT INTO `airlines` (`id`, `airline_short_code`, `airline_name`, `status`) VALUES
(15, 'GF', 'Gulf Air', 'Published'),
(16, 'KM', 'KLM', 'Published'),
(17, 'SV', 'Saudi Arabian Airlines', 'Published'),
(18, 'QR', 'Qatar Airways', 'Published'),
(20, 'EY', 'Etihad Airways', 'Published'),
(21, 'TK', 'Turkish Airlines', 'Published'),
(22, 'PI', 'Pakistan International Airline', 'Published'),
(25, 'AF', 'Air France', 'Published'),
(26, 'EA', 'Egypt Air', 'Published'),
(38, 'WY', 'Oman Airways', 'Published'),
(39, 'EI', 'Aer Lingus', 'Published'),
(40, 'AC', 'Air Canada', 'Published'),
(41, 'AI', 'Air India', 'Published'),
(42, 'BA', 'British Airways', 'Published'),
(43, 'DL', 'Delta Airline', 'Published'),
(44, 'U2', 'Easy Jet', 'Published'),
(45, 'EK', 'Emirates', 'Published'),
(46, 'ET', 'Ethopian Alrline', 'Published'),
(47, 'HX', 'Hongkong Airlines', 'Published'),
(48, 'KQ', 'Kenya Airways', 'Published'),
(49, 'KE', 'Korean Air', 'Published'),
(50, 'MA', 'Multiple Airlines', 'Published'),
(51, 'RJ', 'Royal Jordanian', 'Published'),
(52, 'BI', 'Royal Brunei', 'Published'),
(53, 'FR', 'Ryanair', 'Published'),
(54, 'UL', 'SriLankan Airlines', 'Published'),
(55, 'LX', 'Swiss Air', 'Published'),
(56, 'TG', 'Thai Airways International', 'Published'),
(57, 'UA', 'United Airlines', 'Published');

-- --------------------------------------------------------

--
-- Table structure for table `airline_logos`
--

CREATE TABLE `airline_logos` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `ext` char(5) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `airline_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `date` timestamp NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `airline_logos`
--

INSERT INTO `airline_logos` (`id`, `title`, `ext`, `size`, `path`, `thumb`, `airline_id`, `created_at`, `date`) VALUES
(3, 'air-france.png', '.png', 142, 'uploads/images/airlines/6c77377916301a01992517e3d83ae514.png', 'uploads/images/airlines/thumb/6c77377916301a01992517e3d83ae514.png', 25, '2019-10-10 19:04:23', '2019-05-09 08:22:42'),
(0, 'egyptair.png', '.png', 151, 'uploads/images/airlines/61c1a913726a4a2b7f79aeee8da7ec9b.png', 'uploads/images/airlines/thumb/61c1a913726a4a2b7f79aeee8da7ec9b.png', 30, '2019-05-09 05:24:03', '2019-05-09 08:15:48'),
(4, 'turkish.png', '.png', 145, 'uploads/images/airlines/0d3ad360762f288be716f432d696906d.png', 'uploads/images/airlines/thumb/0d3ad360762f288be716f432d696906d.png', 21, '2019-10-10 19:10:57', '2019-05-09 08:46:58'),
(5, 'klm.png', '.png', 130, 'uploads/images/airlines/7d98938add726dff370a1f8e522c1757.png', 'uploads/images/airlines/thumb/7d98938add726dff370a1f8e522c1757.png', 16, '2019-10-10 19:05:12', '2019-05-09 08:47:37'),
(6, 'egyptair.png', '.png', 151, 'uploads/images/airlines/c4950dac56368d53e0751f88e16f6d73.png', 'uploads/images/airlines/thumb/c4950dac56368d53e0751f88e16f6d73.png', 27, '2019-05-09 06:09:47', '2019-05-09 09:09:47'),
(7, '39.png', '.png', 48, 'uploads/images/airlines/6ae5e04b05cc52ce7e3129795dfc46cd.png', 'uploads/images/airlines/thumb/6ae5e04b05cc52ce7e3129795dfc46cd.png', 28, '2019-06-27 20:00:08', '2019-06-20 13:46:07'),
(13, 'contact-page-logo.png', '.png', 29, 'uploads/images/airlines/901cbe4dc417c1208d8369e6871f958c.png', 'uploads/images/airlines/thumb/901cbe4dc417c1208d8369e6871f958c.png', 35, '2019-09-23 16:37:45', '2019-09-23 19:37:45'),
(26, 'omanair.jpg', '.jpg', 72, 'uploads/images/airlines/40b2678324627ef4bb92ed4d582f566a.jpg', 'uploads/images/airlines/thumb/40b2678324627ef4bb92ed4d582f566a.jpg', 38, '2019-10-10 19:26:47', '2019-10-10 10:26:47'),
(27, 'aerlingus.jpg', '.jpg', 96, 'uploads/images/airlines/e36166c7e461abbb17d33559c28c30aa.jpg', 'uploads/images/airlines/thumb/e36166c7e461abbb17d33559c28c30aa.jpg', 39, '2019-10-10 19:32:06', '2019-10-10 10:32:06'),
(20, 'gulfair.jpg', '.jpg', 83, 'uploads/images/airlines/00b66d520c545007a97552c7b8bbf7d5.jpg', 'uploads/images/airlines/thumb/00b66d520c545007a97552c7b8bbf7d5.jpg', 15, '2019-10-10 19:04:56', '2019-10-10 10:04:56'),
(23, 'qatarairways.jpg', '.jpg', 87, 'uploads/images/airlines/2e92d1c561b6e3d814fc2e91bd84f2f4.jpg', 'uploads/images/airlines/thumb/2e92d1c561b6e3d814fc2e91bd84f2f4.jpg', 18, '2019-10-10 19:12:41', '2019-10-10 10:12:41'),
(22, 'pia.jpg', '.jpg', 87, 'uploads/images/airlines/116d0ed5f20a47a64d0be61307011c64.jpg', 'uploads/images/airlines/thumb/116d0ed5f20a47a64d0be61307011c64.jpg', 22, '2019-10-10 19:10:12', '2019-10-10 10:10:12'),
(28, 'air-canada.jpg', '.jpg', 75, 'uploads/images/airlines/857e12e7cbf6e04710f923ebbf51065c.jpg', 'uploads/images/airlines/thumb/857e12e7cbf6e04710f923ebbf51065c.jpg', 40, '2019-10-10 19:32:56', '2019-10-10 10:32:56'),
(18, 'etihad.jpg', '.jpg', 70, 'uploads/images/airlines/7704459ba1ae097d9f859158714d9eb9.jpg', 'uploads/images/airlines/thumb/7704459ba1ae097d9f859158714d9eb9.jpg', 20, '2019-10-10 19:04:45', '2019-10-08 11:29:10'),
(24, 'Saudi_Arabian_Airlines.jpg', '.jpg', 81, 'uploads/images/airlines/e18afb549115cfc872cca7da07af516c.jpg', 'uploads/images/airlines/thumb/e18afb549115cfc872cca7da07af516c.jpg', 17, '2019-10-10 19:23:33', '2019-10-10 10:23:33'),
(25, 'egypt.png', '.png', 151, 'uploads/images/airlines/fcffa806c9a751321a45a90c5c58bc6a.png', 'uploads/images/airlines/thumb/fcffa806c9a751321a45a90c5c58bc6a.png', 26, '2019-10-10 19:24:05', '2019-10-10 10:24:05'),
(29, 'airindia.jpg', '.jpg', 99, 'uploads/images/airlines/fb7e93c68eefb05b3c05119aee4bfd6c.jpg', 'uploads/images/airlines/thumb/fb7e93c68eefb05b3c05119aee4bfd6c.jpg', 41, '2019-10-10 19:35:20', '2019-10-10 10:35:20'),
(30, 'britishairways.jpg', '.jpg', 74, 'uploads/images/airlines/648b5a7a623c9dfa80ba7a8d4b96e95d.jpg', 'uploads/images/airlines/thumb/648b5a7a623c9dfa80ba7a8d4b96e95d.jpg', 42, '2019-10-10 19:36:36', '2019-10-10 10:36:36'),
(31, 'deltaairline.jpg', '.jpg', 67, 'uploads/images/airlines/ed00fc262a4837faa6132a7c2e88a8dd.jpg', 'uploads/images/airlines/thumb/ed00fc262a4837faa6132a7c2e88a8dd.jpg', 43, '2019-10-10 19:37:36', '2019-10-10 10:37:36'),
(32, 'easy-jet.jpg', '.jpg', 76, 'uploads/images/airlines/a900aebc80f1ccdf05b858c91be4880d.jpg', 'uploads/images/airlines/thumb/a900aebc80f1ccdf05b858c91be4880d.jpg', 44, '2019-10-10 19:38:47', '2019-10-10 10:38:47'),
(33, 'emirates.jpg', '.jpg', 97, 'uploads/images/airlines/47b56e000f29241569f516a97a5075ce.jpg', 'uploads/images/airlines/thumb/47b56e000f29241569f516a97a5075ce.jpg', 45, '2019-10-10 19:39:38', '2019-10-10 10:39:38'),
(34, 'ethopia.jpg', '.jpg', 90, 'uploads/images/airlines/974d61355c26471e25df2b2fa8a70677.jpg', 'uploads/images/airlines/thumb/974d61355c26471e25df2b2fa8a70677.jpg', 46, '2019-10-10 19:41:19', '2019-10-10 10:41:19'),
(35, 'hongkongairline.jpg', '.jpg', 71, 'uploads/images/airlines/fef7f9004025ef099aad4b9f78e48155.jpg', 'uploads/images/airlines/thumb/fef7f9004025ef099aad4b9f78e48155.jpg', 47, '2019-10-10 19:42:22', '2019-10-10 10:42:22'),
(36, 'kenyaairways.jpg', '.jpg', 86, 'uploads/images/airlines/493ae10a72980d0a46ba15684def1ae6.jpg', 'uploads/images/airlines/thumb/493ae10a72980d0a46ba15684def1ae6.jpg', 48, '2019-10-10 19:43:14', '2019-10-10 10:43:14'),
(37, 'koreanair.jpg', '.jpg', 91, 'uploads/images/airlines/d44d360991ab0d9e3a7a5ed275e62919.jpg', 'uploads/images/airlines/thumb/d44d360991ab0d9e3a7a5ed275e62919.jpg', 49, '2019-10-10 19:43:46', '2019-10-10 10:43:46'),
(38, 'multiple-airline.jpg', '.jpg', 83, 'uploads/images/airlines/073d32f0ffd28561017d80b49296692a.jpg', 'uploads/images/airlines/thumb/073d32f0ffd28561017d80b49296692a.jpg', 50, '2019-10-10 19:44:20', '2019-10-10 10:44:20'),
(39, 'rj.jpg', '.jpg', 98, 'uploads/images/airlines/6ac10fdb00480537ba054039c69884b5.jpg', 'uploads/images/airlines/thumb/6ac10fdb00480537ba054039c69884b5.jpg', 51, '2019-10-10 19:45:06', '2019-10-10 10:45:06'),
(40, 'Royal_Brunei.jpg', '.jpg', 74, 'uploads/images/airlines/79ea90bb88ca8fedc21f48be320713d3.jpg', 'uploads/images/airlines/thumb/79ea90bb88ca8fedc21f48be320713d3.jpg', 52, '2019-10-10 19:46:03', '2019-10-10 10:46:03'),
(41, 'ryanair.jpg', '.jpg', 73, 'uploads/images/airlines/16bf07aa250aad59a22a5403c910cf28.jpg', 'uploads/images/airlines/thumb/16bf07aa250aad59a22a5403c910cf28.jpg', 53, '2019-10-10 19:46:51', '2019-10-10 10:46:51'),
(42, 'srilinkanairline.jpg', '.jpg', 94, 'uploads/images/airlines/99395cb415e8506d54dd4678cb7776d3.jpg', 'uploads/images/airlines/thumb/99395cb415e8506d54dd4678cb7776d3.jpg', 54, '2019-10-10 19:47:57', '2019-10-10 10:47:57'),
(43, 'swissair.jpg', '.jpg', 68, 'uploads/images/airlines/d6c38087d65ca0454f111a59754736d8.jpg', 'uploads/images/airlines/thumb/d6c38087d65ca0454f111a59754736d8.jpg', 55, '2019-10-10 19:48:52', '2019-10-10 10:48:52'),
(44, 'thai.jpg', '.jpg', 104, 'uploads/images/airlines/6ed11f7bcf05024687920bd1445e96ed.jpg', 'uploads/images/airlines/thumb/6ed11f7bcf05024687920bd1445e96ed.jpg', 56, '2019-10-10 19:49:40', '2019-10-10 10:49:40'),
(45, 'unitedairlines.jpg', '.jpg', 69, 'uploads/images/airlines/b41610266389142032eb4d5c112c12d4.jpg', 'uploads/images/airlines/thumb/b41610266389142032eb4d5c112c12d4.jpg', 57, '2019-10-10 19:50:19', '2019-10-10 10:50:19');

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

CREATE TABLE `blocks` (
  `id` int(11) NOT NULL,
  `position` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `term_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blocks`
--

INSERT INTO `blocks` (`id`, `position`, `title`, `term_id`) VALUES
(22, 'home_flight_offers', 'Cheap Flights to Europe', 77),
(82, 'home_lowest_fares', 'Lowest Fares', 98),
(85, 'home_flight_offers', 'Cheap Flights to Asia', 76),
(86, 'home_flight_offers', 'Cheap Flights to Africa', 75);

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(11) UNSIGNED NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `flight_from` varchar(255) DEFAULT NULL,
  `flight_to` varchar(255) DEFAULT NULL,
  `takeoff_time` varchar(255) DEFAULT NULL,
  `landing_time` varchar(255) DEFAULT NULL,
  `adults` int(11) UNSIGNED DEFAULT NULL,
  `kids` int(11) UNSIGNED DEFAULT NULL,
  `infants` int(11) UNSIGNED DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `published_date` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `first_name`, `last_name`, `email`, `phone`, `flight_from`, `flight_to`, `takeoff_time`, `landing_time`, `adults`, `kids`, `infants`, `type`, `class`, `published_date`, `status`) VALUES
(10, 'Kashif Aman', NULL, 'kashlion@gmail.com', '03367366922', 'Lahore', 'Entebbe', '05/24/2019', 'punjab', 1, 4, 4, '', '', 0, 0),
(11, 'Kashif Aman', NULL, 'kashlion@gmail.com', '03367366922', 'Lahore', 'Harare', '05/24/2019', 'punjab', 2, 3, 3, '', '', 0, 0),
(12, 'Musa', NULL, 'iammusabutt@gmail.com', '03367366914', 'Lahore', 'asdasd', '05/24/2019', 'punjab', 6, 6, 6, 'Return Flight', 'Economy', 0, 0),
(13, 'Musa', NULL, 'iammusabutt@gmail.com', '03367366914', 'Lahore', 'asdasd', '05/24/2019', 'punjab', 6, 6, 6, 'Return Flight', 'Economy', 0, 0),
(14, 'Musa', NULL, 'iammusabutt@gmail.com', '03367366914', 'Lahore', 'asdasd', '05/24/2019', 'punjab', 6, 6, 6, 'Return Flight', 'Economy', 0, 0),
(15, 'Musa', NULL, 'iammusabutt@gmail.com', '03367366914', 'Lahore', 'asdasd', '05/24/2019', 'punjab', 6, 6, 6, 'Return Flight', 'Economy', 0, 0),
(16, 'Musa', NULL, 'iammusabutt@gmail.com', '03367366914', 'Lahore', 'asdasd', '05/24/2019', 'punjab', 6, 6, 6, 'Return Flight', 'Economy', 0, 0),
(17, 'Kashif Aman', NULL, 'kashlion@gmail.com', '03367366922', 'Lahore', 'Entebbe', '05/24/2019', 'punjab', 7, 7, 5, 'Return Flight', 'Economy', 0, 0),
(18, 'Kashif Aman', NULL, 'kashlion@gmail.com', '03367366922', 'Lahore', 'WQDD', '05/21/2019', 'punjab', 5, 6, 4, 'Return Flight', 'Economy', 0, 0),
(19, 'Kashif Aman', NULL, 'kashlion@gmail.com', '03367366922', 'Lahore', 'Harare', '05/24/2019', 'punjab', 5, 0, 0, 'Return Flight', 'Economy', 0, 0),
(20, 'Kashif Aman', NULL, 'kashlion@gmail.com', '03367366922', 'Lahore', 'Entebbe', '05/24/2019', 'punjab', 3, 4, 4, 'Return Flight', 'Economy', 0, 0),
(21, 'Kashif Aman', NULL, 'kashlion@gmail.com', '03367366922', 'Lahore', 'Entebbe', '05/24/2019', 'punjab', 2, 0, 0, 'Return Flight', 'Economy', 0, 0),
(22, 'Kashif Aman', NULL, 'kashlion@gmail.com', '03367366922', 'Lahore', 'Entebbe', '05/21/2019', 'punjab', 6, 0, 0, 'Return Flight', 'Economy', 0, 0),
(23, 'asdasd', NULL, 'iammusabutt@gmail.com', '03367366914', 'Lahore', 'Entebbe', '05/21/2019', 'punjab', 6, 0, 0, 'Return Flight', 'Economy', 0, 0),
(24, 'asdasd', NULL, 'iammusabutt@gmail.com', '03367366914', 'Lahore', 'Entebbe', '05/21/2019', 'punjab', 6, 0, 0, 'Return Flight', 'Economy', 0, 0),
(25, 'Musa', NULL, 'iammusabutt@gmail.com', '03367366914', 'Lahore', 'Harare', '05/24/2019', 'punjab', 2, 0, 0, 'Return Flight', 'Economy', 0, 0),
(26, 'Musa', NULL, 'iammusabutt@gmail.com', '03367366914', 'Lahore', 'Harare', '05/24/2019', 'punjab', 2, 0, 0, 'Return Flight', 'Economy', 0, 0),
(27, 'Kashif Aman', NULL, 'kashlion@gmail.com', '03367366922', 'Lahore', 'Entebbe', '05/24/2019', 'punjab', 3, 3, 4, 'Return Flight', 'Economy', 0, 0),
(28, 'Kashif Aman', NULL, 'kashlion@gmail.com', '03367366922', 'Lahore', 'Entebbe', '05/24/2019', '05/24/2019', 3, 4, 4, 'Return Flight', 'Economy', 0, 0),
(29, 'Kashif Aman', NULL, 'kashlion@gmail.com', '03367366922', 'Lahore', 'Entebbe', '05/24/2019', '05/24/2019', 3, 4, 4, 'Return Flight', 'Economy', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) UNSIGNED NOT NULL,
  `country_id` int(11) UNSIGNED NOT NULL,
  `city_code` varchar(3) DEFAULT NULL,
  `city_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `country_id`, `city_code`, `city_name`) VALUES
(1, 167, 'LHE', 'Lahore'),
(2, 230, 'LHR', 'London'),
(3, 167, 'KHI', 'Karachi'),
(4, 81, 'ACC', 'Accra'),
(5, 132, 'LLW', 'Lilongwe'),
(6, 246, 'HRE', 'Harare'),
(7, 227, 'EBB', 'Entebbe'),
(8, 113, 'NBO', 'Nairobi'),
(10, 161, 'LOS', 'Lagos'),
(11, 1, 'JNB', 'Johannesburg'),
(12, 216, 'DAR', 'Dar-Es-Salaam'),
(13, 67, 'ADD', 'Addis Ababa'),
(14, 200, 'CPT', 'Cape Town'),
(15, 167, 'ISB', 'Islamabad'),
(16, 167, 'PEW', 'Peshawar'),
(17, 99, 'DEL', 'Delhi'),
(18, 99, 'BOM', 'Mumbai'),
(19, 99, 'AMD', 'Ahmedabad'),
(20, 99, 'CCU', 'Kolkata'),
(21, 99, 'BLR', 'Bengaluru'),
(22, 99, 'HYD', 'Hyderabad'),
(23, 49, 'FIH', 'Kinshasa'),
(24, 200, 'DUR', 'Durban'),
(25, 191, 'DKR', 'Dakar'),
(26, 44, 'PVG', 'Shanghai'),
(27, 44, 'CAN', 'Guangzhou'),
(28, 44, 'BJS', 'Beijing'),
(29, 217, 'BKK', 'Bangkok'),
(30, 199, 'HGA', 'Hargeisa'),
(31, 229, 'AUH', 'Abu Dhabi'),
(32, 190, 'DMM', 'Dammam'),
(33, 179, 'DOH', 'Doha'),
(34, 2, 'TSE', 'Tirana'),
(35, 190, '', 'Jeddah'),
(36, 190, '', 'Medina'),
(37, 161, '', 'Abuja'),
(38, 80, '', 'Frankfurt'),
(40, 72, '', 'Venice'),
(41, 203, '', 'Barcelona'),
(42, 106, '', 'Rome'),
(44, 56, '', 'Copenhagen');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` bigint(20) NOT NULL,
  `aircraft_class_name` text DEFAULT NULL,
  `aircraft_class_description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `aircraft_class_name`, `aircraft_class_description`) VALUES
(9, 'Economy Class', ''),
(10, 'Business Class', ''),
(14, 'First Class', ''),
(15, 'Charter', '');

-- --------------------------------------------------------

--
-- Table structure for table `continents`
--

CREATE TABLE `continents` (
  `id` int(11) UNSIGNED NOT NULL,
  `continent_code` varchar(3) DEFAULT NULL,
  `continent_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `continents`
--

INSERT INTO `continents` (`id`, `continent_code`, `continent_name`) VALUES
(1, 'AF', 'Africa'),
(2, 'AS', 'Asia'),
(3, 'AU', 'Australia'),
(4, 'EU', 'Europe'),
(5, 'NA', 'North America'),
(6, 'SA', 'South America'),
(7, 'AN', 'Antarctica');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `continent_id` int(11) NOT NULL,
  `country_code` varchar(3) DEFAULT NULL,
  `country_name` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `continent_id`, `country_code`, `country_name`) VALUES
(1, 2, 'AF', 'Afghanistan'),
(2, 4, 'AL', 'Albania'),
(3, 1, 'DZ', 'Algeria'),
(4, 3, 'DS', 'American Samoa'),
(5, 4, 'AD', 'Andorra'),
(6, 1, 'AO', 'Angola'),
(7, 5, 'AI', 'Anguilla'),
(8, 7, 'AQ', 'Antarctica'),
(9, 5, 'AG', 'Antigua and Barbuda'),
(10, 6, 'AR', 'Argentina'),
(11, 4, 'AM', 'Armenia'),
(12, 6, 'AW', 'Aruba'),
(13, 3, 'AU', 'Australia'),
(14, 4, 'AT', 'Austria'),
(15, 4, 'AZ', 'Azerbaijan'),
(16, 5, 'BS', 'Bahamas'),
(17, 2, 'BH', 'Bahrain'),
(18, 2, 'BD', 'Bangladesh'),
(19, 5, 'BB', 'Barbados'),
(20, 4, 'BY', 'Belarus'),
(21, 4, 'BE', 'Belgium'),
(22, 5, 'BZ', 'Belize'),
(23, 1, 'BJ', 'Benin'),
(24, 5, 'BM', 'Bermuda'),
(25, 2, 'BT', 'Bhutan'),
(26, 6, 'BO', 'Bolivia'),
(27, 4, 'BA', 'Bosnia and Herzegovina'),
(28, 1, 'BW', 'Botswana'),
(29, 7, 'BV', 'Bouvet Island'),
(30, 6, 'BR', 'Brazil'),
(31, 2, 'IO', 'British Indian Ocean Territory'),
(32, 2, 'BN', 'Brunei Darussalam'),
(33, 4, 'BG', 'Bulgaria'),
(34, 1, 'BF', 'Burkina Faso'),
(35, 1, 'BI', 'Burundi'),
(36, 6, 'KH', 'Cambodia'),
(37, 1, 'CM', 'Cameroon'),
(38, 5, 'CA', 'Canada'),
(39, 1, 'CV', 'Cape Verde'),
(40, 5, 'KY', 'Cayman Islands'),
(41, 1, 'CF', 'Central African Republic'),
(42, 1, 'TD', 'Chad'),
(43, 6, 'CL', 'Chile'),
(44, 2, 'CN', 'China'),
(45, 2, 'CX', 'Christmas Island'),
(46, 2, 'CC', 'Cocos (Keeling) Islands'),
(47, 2, 'CO', 'Colombia'),
(48, 1, 'KM', 'Comoros'),
(49, 2, 'CG', 'Congo'),
(50, 3, 'CK', 'Cook Islands'),
(51, 5, 'CR', 'Costa Rica'),
(52, 4, 'HR', 'Croatia (Hrvatska)'),
(53, 5, 'CU', 'Cuba'),
(54, 4, 'CY', 'Cyprus'),
(55, 4, 'CZ', 'Czech Republic'),
(56, 4, 'DK', 'Denmark'),
(57, 1, 'DJ', 'Djibouti'),
(58, 5, 'DM', 'Dominica'),
(59, 5, 'DO', 'Dominican Republic'),
(60, 2, 'TP', 'East Timor'),
(61, 6, 'EC', 'Ecuador'),
(62, 1, 'EG', 'Egypt'),
(63, 5, 'SV', 'El Salvador'),
(64, 1, 'GQ', 'Equatorial Guinea'),
(65, 1, 'ER', 'Eritrea'),
(66, 4, 'EE', 'Estonia'),
(67, 1, 'ET', 'Ethiopia'),
(68, 6, 'FK', 'Falkland Islands (Malvinas)'),
(69, 4, 'FO', 'Faroe Islands'),
(70, 3, 'FJ', 'Fiji'),
(71, 4, 'FI', 'Finland'),
(72, 4, 'FR', 'France'),
(73, 4, 'FX', 'France, Metropolitan'),
(74, 4, 'GF', 'French Guiana'),
(75, 4, 'PF', 'French Polynesia'),
(76, 4, 'TF', 'French Southern Territories'),
(77, 1, 'GA', 'Gabon'),
(78, 1, 'GM', 'Gambia'),
(79, 4, 'GE', 'Georgia'),
(80, 4, 'DE', 'Germany'),
(81, 1, 'GH', 'Ghana'),
(82, 4, 'GI', 'Gibraltar'),
(83, 4, 'GK', 'Guernsey'),
(84, 4, 'GR', 'Greece'),
(85, 5, 'GL', 'Greenland'),
(86, 5, 'GD', 'Grenada'),
(87, 5, 'GP', 'Guadeloupe'),
(88, 3, 'GU', 'Guam'),
(89, 5, 'GT', 'Guatemala'),
(90, 1, 'GN', 'Guinea'),
(91, 1, 'GW', 'Guinea-Bissau'),
(92, 6, 'GY', 'Guyana'),
(93, 5, 'HT', 'Haiti'),
(94, 7, 'HM', 'Heard and Mc Donald Islands'),
(95, 5, 'HN', 'Honduras'),
(96, 2, 'HK', 'Hong Kong'),
(97, 4, 'HU', 'Hungary'),
(98, 4, 'IS', 'Iceland'),
(99, 2, 'IN', 'India'),
(100, 4, 'IM', 'Isle of Man'),
(101, 2, 'ID', 'Indonesia'),
(102, 2, 'IR', 'Iran (Islamic Republic of)'),
(103, 2, 'IQ', 'Iraq'),
(104, 4, 'IE', 'Ireland'),
(105, 2, 'IL', 'Israel'),
(106, 4, 'IT', 'Italy'),
(107, 1, 'CI', 'Ivory Coast'),
(108, 4, 'JE', 'Jersey'),
(109, 5, 'JM', 'Jamaica'),
(110, 2, 'JP', 'Japan'),
(111, 2, 'JO', 'Jordan'),
(112, 2, 'KZ', 'Kazakhstan'),
(113, 1, 'KE', 'Kenya'),
(114, 3, 'KI', 'Kiribati'),
(115, 2, 'KP', 'Korea, Democratic People\'s Republic of'),
(116, 2, 'KR', 'Korea, Republic of'),
(117, 4, 'XK', 'Kosovo'),
(118, 2, 'KW', 'Kuwait'),
(119, 2, 'KG', 'Kyrgyzstan'),
(120, 2, 'LA', 'Lao People\'s Democratic Republic'),
(121, 4, 'LV', 'Latvia'),
(122, 2, 'LB', 'Lebanon'),
(123, 1, 'LS', 'Lesotho'),
(124, 1, 'LR', 'Liberia'),
(125, 1, 'LY', 'Libyan Arab Jamahiriya'),
(126, 4, 'LI', 'Liechtenstein'),
(127, 4, 'LT', 'Lithuania'),
(128, 4, 'LU', 'Luxembourg'),
(129, 2, 'MO', 'Macau'),
(130, 4, 'MK', 'Macedonia'),
(131, 1, 'MG', 'Madagascar'),
(132, 1, 'MW', 'Malawi'),
(133, 2, 'MY', 'Malaysia'),
(134, 2, 'MV', 'Maldives'),
(135, 1, 'ML', 'Mali'),
(136, 4, 'MT', 'Malta'),
(137, 3, 'MH', 'Marshall Islands'),
(138, 5, 'MQ', 'Martinique'),
(139, 1, 'MR', 'Mauritania'),
(140, 1, 'MU', 'Mauritius'),
(141, 1, 'TY', 'Mayotte'),
(142, 5, 'MX', 'Mexico'),
(143, 3, 'FM', 'Micronesia, Federated States of'),
(144, 4, 'MD', 'Moldova, Republic of'),
(145, 4, 'MC', 'Monaco'),
(146, 2, 'MN', 'Mongolia'),
(147, 4, 'ME', 'Montenegro'),
(148, 5, 'MS', 'Montserrat'),
(149, 1, 'MA', 'Morocco'),
(150, 1, 'MZ', 'Mozambique'),
(151, 2, 'MM', 'Myanmar'),
(152, 1, 'NA', 'Namibia'),
(153, 3, 'NR', 'Nauru'),
(154, 2, 'NP', 'Nepal'),
(155, 4, 'NL', 'Netherlands'),
(156, 4, 'AN', 'Netherlands Antilles'),
(157, 3, 'NC', 'New Caledonia'),
(158, 3, 'NZ', 'New Zealand'),
(159, 5, 'NI', 'Nicaragua'),
(160, 1, 'NE', 'Niger'),
(161, 1, 'NG', 'Nigeria'),
(162, 3, 'NU', 'Niue'),
(163, 3, 'NF', 'Norfolk Island'),
(164, 3, 'MP', 'Northern Mariana Islands'),
(165, 4, 'NO', 'Norway'),
(166, 2, 'OM', 'Oman'),
(167, 2, 'PK', 'Pakistan'),
(168, 3, 'PW', 'Palau'),
(169, 2, 'PS', 'Palestine'),
(170, 5, 'PA', 'Panama'),
(171, 3, 'PG', 'Papua New Guinea'),
(172, 6, 'PY', 'Paraguay'),
(173, 6, 'PE', 'Peru'),
(174, 2, 'PH', 'Philippines'),
(175, 3, 'PN', 'Pitcairn'),
(176, 4, 'PL', 'Poland'),
(177, 4, 'PT', 'Portugal'),
(178, 5, 'PR', 'Puerto Rico'),
(179, 2, 'QA', 'Qatar'),
(180, 1, 'RE', 'Reunion'),
(181, 4, 'RO', 'Romania'),
(182, 2, 'RU', 'Russian Federation'),
(183, 1, 'RW', 'Rwanda'),
(184, 5, 'KN', 'Saint Kitts and Nevis'),
(185, 5, 'LC', 'Saint Lucia'),
(186, 5, 'VC', 'Saint Vincent and the Grenadines'),
(187, 3, 'WS', 'Samoa'),
(188, 4, 'SM', 'San Marino'),
(189, 1, 'ST', 'Sao Tome and Principe'),
(190, 2, 'SA', 'Saudi Arabia'),
(191, 1, 'SN', 'Senegal'),
(192, 4, 'RS', 'Serbia'),
(193, 1, 'SC', 'Seychelles'),
(194, 1, 'SL', 'Sierra Leone'),
(195, 2, 'SG', 'Singapore'),
(196, 4, 'SK', 'Slovakia'),
(197, 4, 'SI', 'Slovenia'),
(198, 3, 'SB', 'Solomon Islands'),
(199, 1, 'SO', 'Somalia'),
(200, 1, 'ZA', 'South Africa'),
(201, 7, 'GS', 'South Georgia South Sandwich Islands'),
(202, 1, 'SS', 'South Sudan'),
(203, 4, 'ES', 'Spain'),
(204, 2, 'LK', 'Sri Lanka'),
(205, 1, 'SH', 'St. Helena'),
(206, 5, 'PM', 'St. Pierre and Miquelon'),
(207, 1, 'SD', 'Sudan'),
(208, 6, 'SR', 'Suriname'),
(209, 4, 'SJ', 'Svalbard and Jan Mayen Islands'),
(210, 1, 'SZ', 'Swaziland'),
(211, 4, 'SE', 'Sweden'),
(212, 4, 'CH', 'Switzerland'),
(213, 2, 'SY', 'Syrian Arab Republic'),
(214, 2, 'TW', 'Taiwan'),
(215, 2, 'TJ', 'Tajikistan'),
(216, 1, 'TZ', 'Tanzania, United Republic of'),
(217, 2, 'TH', 'Thailand'),
(218, 1, 'TG', 'Togo'),
(219, 3, 'TK', 'Tokelau'),
(220, 3, 'TO', 'Tonga'),
(221, 5, 'TT', 'Trinidad and Tobago'),
(222, 1, 'TN', 'Tunisia'),
(223, 2, 'TR', 'Turkey'),
(224, 2, 'TM', 'Turkmenistan'),
(225, 5, 'TC', 'Turks and Caicos Islands'),
(226, 3, 'TV', 'Tuvalu'),
(227, 1, 'UG', 'Uganda'),
(228, 4, 'UA', 'Ukraine'),
(229, 2, 'AE', 'United Arab Emirates'),
(230, 4, 'GB', 'United Kingdom'),
(231, 5, 'US', 'United States'),
(232, 5, 'UM', 'United States minor outlying islands'),
(233, 6, 'UY', 'Uruguay'),
(234, 2, 'UZ', 'Uzbekistan'),
(235, 3, 'VU', 'Vanuatu'),
(236, 4, 'VA', 'Vatican City State'),
(237, 6, 'VE', 'Venezuela'),
(238, 2, 'VN', 'Vietnam'),
(239, 5, 'VG', 'Virgin Islands (British)'),
(240, 5, 'VI', 'Virgin Islands (U.S.)'),
(241, 3, 'WF', 'Wallis and Futuna Islands'),
(242, 1, 'EH', 'Western Sahara'),
(243, 2, 'YE', 'Yemen'),
(244, 1, 'ZR', 'Zaire'),
(245, 1, 'ZM', 'Zambia'),
(246, 1, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `destinations`
--

CREATE TABLE `destinations` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` enum('city','country','continent') DEFAULT NULL,
  `select_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `status` enum('Draft','Published') DEFAULT NULL,
  `featured` enum('0','1') NOT NULL DEFAULT '0',
  `date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `destinations`
--

INSERT INTO `destinations` (`id`, `type`, `select_id`, `title`, `slug`, `description`, `author`, `status`, `featured`, `date`) VALUES
(1, 'country', 1, 'Cheap Flights to Afghanistan', 'afghanistan', '<p>Hajj Service will keep up your interest as the number one priority for Hajj 2019 from Karachi, Pakistan. Our trusted partners are locals and in Saudi Arabia makes it all easy for you. From 5 Star accommodation to transportation, food and hospitality to guidance and training before and during Hajj 2019 is part of our exclusive Hajj services. Al-Khair provides premium private transportation, meals, best hotels in Makkah, Madina and Azizia, with proper assistance in Qurbani to Ziarat of Holy Places. Al-Khair is a complete luxury from travel to Saudi Arabia and back home which count us in best Hajj travel agencies in Pakistan.</p><h3>Afghanistan</h3><p>Should you have any questions on any of the Sort Hajj packages, please feel free to contact us. Our customer support is always there to help you in booking your Hajj Tour.<br></p><p></p><p><br></p><p><br></p>', '1', 'Published', '1', '2019-09-21 03:37:10'),
(14, 'country', 2, 'Cheap Flights to Albania', 'albania', '<h3 style=\"font-family: Quicksand, sans-serif; line-height: 1.1; color: rgb(37, 61, 82); margin-top: 0px; margin-bottom: 20px; font-size: 24px;\">When is the best time to fly to Albania?</h3><p>Aliquam blandit nisl sem. Mauris quis enim purus. Vivamus nec tortor bibendum risus placerat vulputate at gravida ante. Nam sit amet tellus enim. Phasellus consectetur porttitor lobortis. Integer cursus odio at mattis porttitor. In hac habitasse platea dictumst. Nunc sit amet cursus felis. Etiam venenatis auctor metus, et lacinia elit dignissim non. Aenean auctor semper erat porta dictum.Aliquam blandit nisl sem. Mauris quis enim purus. Vivamus nec tortor bibendum risus placerat vulputate at gravida ante. Nam sit amet tellus enim. Phasellus consectetur porttitor lobortis. Integer cursus odio at mattis porttitor. In hac habitasse platea dictumst. Nunc sit amet cursus felis. Etiam venenatis auctor metus, et lacinia elit dignissim non. Aenean auctor semper erat porta dictum.Aliquam blandit nisl sem. Mauris quis enim purus. Vivamus nec tortor bibendum risus placerat vulputate at gravida ante. Nam sit amet tellus enim. Phasellus consectetur porttitor lobortis. Integer cursus odio at mattis porttitor. In hac habitasse platea dictumst. Nunc sit amet cursus felis. Etiam venenatis auctor metus, et lacinia elit dignissim non. Aenean auctor semper erat porta dictum.</p><h3 style=\"margin-top: 0px; margin-bottom: 20px; font-family: Quicksand, sans-serif; line-height: 1.1; color: rgb(37, 61, 82); font-size: 24px;\">When is the best time to book flights to Albania?</h3><p></p><p>Aliquam blandit nisl sem. Mauris quis enim purus. Vivamus nec tortor bibendum risus placerat vulputate at gravida ante. Nam sit amet tellus enim. Phasellus consectetur porttitor lobortis. Integer cursus odio at mattis porttitor. In hac habitasse platea dictumst. Nunc sit amet cursus felis. Etiam venenatis auctor metus, et lacinia elit dignissim non. Aenean auctor semper erat porta dictum.Aliquam blandit nisl sem. Mauris quis enim purus. Vivamus nec tortor bibendum risus placerat vulputate at gravida ante. Nam sit amet tellus enim. Phasellus consectetur porttitor lobortis. Integer cursus odio at mattis porttitor.&nbsp;</p><h3 style=\"margin-top: 0px; margin-bottom: 20px; font-family: Quicksand, sans-serif; line-height: 1.1; color: rgb(37, 61, 82); font-size: 24px;\">When to fly to Albania?</h3><p>Aliquam blandit nisl sem. Mauris quis enim purus. Vivamus nec tortor bibendum risus placerat vulputate at gravida ante. Nam sit amet tellus enim. Phasellus consectetur porttitor lobortis. Integer cursus odio at mattis porttitor. In hac habitasse platea dictumst. Nunc sit amet cursus felis. Etiam venenatis auctor metus, et lacinia elit dignissim non. Aenean auctor semper erat porta dictum.Aliquam blandit nisl sem. Mauris quis enim purus. Vivamus nec tortor bibendum risus placerat vulputate at gravida ante. Nam sit amet tellus enim. Phasellus consectetur porttitor lobortis. Integer cursus odio at mattis porttitor. In hac habitasse platea dictumst. Nunc sit amet cursus felis. Etiam venenatis auctor metus, et lacinia elit dignissim non. Aenean auctor semper erat porta dictum.</p><p></p>', '1', 'Published', '1', '2019-05-09 10:12:09'),
(16, 'country', 44, 'Cheap Flights to Beijing, China', 'beijing', '', '1', 'Published', '1', '2019-05-02 11:10:30'),
(17, 'city', 2, 'Cheap Flights to London', 'london', '', '1', 'Published', '1', '2019-05-03 06:44:03'),
(18, 'city', 3, 'Cheap Flights to Karachi', 'karachi', '', '1', 'Published', '1', '2019-05-03 06:44:43'),
(25, 'continent', 5, 'Cheap Flights to North America', 'north_america', '<p>Cheap Flights to North America<br></p>', '1', 'Published', '1', '2019-05-22 08:33:45'),
(26, 'country', 14, 'Cheap Flights to Austria', 'austria', '<p>Cheap Flights to Austria<br></p>', '1', 'Published', '1', '2019-05-22 08:38:02'),
(27, 'city', 6, 'Cheap Flights to Harare', 'harare', '<p>Cheap Flights to Harare<br></p>', '1', 'Published', '1', '2019-05-22 08:42:28'),
(28, 'country', 30, 'Cheap Flights to Brazil', 'brazil', '<p>Cheap Flights to Brazil<br></p>', '1', 'Published', '1', '2019-05-22 08:44:54'),
(30, 'city', 24, 'Cheap Flights to Durban', 'drban', '<p>Cheap Flights to Durban<br></p>', '1', 'Published', '1', '2019-05-22 09:33:52'),
(31, 'country', 62, 'Cheap Flights to Egypt', 'egypt', '', '1', 'Published', '1', '2019-05-22 12:05:46'),
(39, 'continent', 1, 'Cheap Flights to Africa', 'africa', '', NULL, 'Published', '1', '2019-07-24 19:37:13'),
(41, 'country', 199, 'Cheap Flights to Somalia', 'somalia', '', NULL, 'Published', '1', '2019-06-19 17:39:35'),
(42, 'country', 190, 'Cheap Flights to Saudi Arabia', 'saudi_arabia', '', NULL, 'Published', '1', '2019-06-19 18:03:03'),
(43, 'continent', 2, 'Cheap Flights to Asia', 'asia', '', NULL, 'Published', '1', '2019-06-27 12:57:47'),
(45, 'city', 27, 'Cheap Flights to Guangzhou', 'guangzhou', '<h3>Guangzhou Overview</h3><p>The city of Guangzhou is situated on the banks of China’s glorious Pearl River. Known for its richness of cultural aspect and tradition, the city is home to the ‘Eight Sights of Guangzhou’. Rulers of each successive age have made sure that certain eight places are designated to be handsome sights for the tourists. Places like the magnificent ‘White Cloud Mountain Park’, the splendid Pearl River, and the Nansha Wetland Park are some of the sights that are included in the Eight Sights of Guangzhou. If you want to wander about amazingly peaceful yet mysterious scenery, then White Cloud Mountain Park is an ultimate fit for you. On the other hand, if you are looking for exotic yet peaceful places of nature for your honeymoon, then this city’s parks are luscious attractions. However, the elegance of parks have not just attracted couples alone, but in fact, several tourist families, single visitors and so forth come and enjoy the tranquility that they have to offer all year round. As for the modern places of attraction, the Guangzhou Science Park has attracted a number of tourists in recent years.&nbsp;</p><p>The city of Guangzhou is home to several historical religious places like temples and mosques. Places of worship allow people to get acquainted with religions of a diverse array of people. Such places, therefore, should be visited and some of them are Sacred Heart Cathedral, Huaisheng Mosque and the Temple of the Six Banyan Trees.</p><h3>Best time to fly to Guangzhou</h3><p>Guangzhou attracts several tourists all-year-around because of its pleasant climate and moderate temperatures. Some tend to avoid the rainy months such as May and June. However, if rain does not bother you or hinder you from enjoying what the city has to offer than the rainy season is the best season to visit the city. Firstly, because you can find inexpensive flights to Guangzhou during this time, and with that during this time of the year, the city is not packed with tourists. Moreover, the rain can clear out smog and dust that has occupied the air.</p><p>Nevertheless, if you are not very fond of showers then the most ideal time to fly to Guangzhou would be in October and November when the climatic conditions are overall mild. However, this time of the year is packed with tourists and the flights are expensive. The Chinese New Year in January/ February and the Lantern Festival in March are other great times of the year to visit the city.</p><h3>Getting around Guangzhou&nbsp;</h3><p>The best way to explore Guangzhou is on foot. Yes, you heard that right, and that is because the city is full of everything that has got to do the nature and beauty of nature. For the purpose, there even exists a separate road i.e. the Zhongshan 6 Road exclusively designated for tourists. This road is linked to the major attractions of the city. Apart from walking on foot, you can be benefited from several bus transportation. Besides, if you are looking for that extra bit of convenience, then go for the subway.</p>', NULL, 'Published', '1', '2019-08-28 22:17:23');

-- --------------------------------------------------------

--
-- Table structure for table `destination_images`
--

CREATE TABLE `destination_images` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `ext` char(5) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `destination_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `date` timestamp NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `destination_images`
--

INSERT INTO `destination_images` (`id`, `title`, `ext`, `size`, `path`, `thumb`, `destination_id`, `created_at`, `date`) VALUES
(1, '60774740_327016267942607_2160410795186323456_o.jpg', '.jpg', 181, 'uploads/images/destinations/2874d1f991d1f7515930e040d62b22ad.jpg', 'uploads/images/destinations/thumb/2874d1f991d1f7515930e040d62b22ad.jpg', 1, '2019-10-21 19:03:54', '2019-05-02 11:35:12'),
(2, 'destination-djibouti.png', '.png', 1227, 'uploads/images/destinations/b27c2ff49754c3edbc4e6055d105dec2.png', 'uploads/images/destinations/thumb/b27c2ff49754c3edbc4e6055d105dec2.png', 14, '2019-09-20 15:52:11', '2019-05-02 11:58:06'),
(3, 'destinations-05.png', '.png', 691, 'uploads/images/destinations/c92a9b8780847cd248d9737324db0128.png', 'uploads/images/destinations/thumb/c92a9b8780847cd248d9737324db0128.png', 16, '2019-10-08 19:12:28', '2019-05-02 14:10:27'),
(4, 'flight-delhi.png', '.png', 100, 'uploads/images/destinations/45b63ce23fe2af8797799fe2e8589c15.png', 'uploads/images/destinations/thumb/45b63ce23fe2af8797799fe2e8589c15.png', 19, '2019-05-02 05:26:21', '2019-05-02 20:26:21'),
(5, 'destinations-02.png', '.png', 620, 'uploads/images/destinations/bf1d2560e40333a1601fee47d7153393.png', 'uploads/images/destinations/thumb/bf1d2560e40333a1601fee47d7153393.png', 17, '2019-10-08 19:14:28', '2019-05-03 09:44:00'),
(6, 'destinations-guide-02.png', '.png', 759, 'uploads/images/destinations/15c22bb390247739d7b56678fe9cbf57.png', 'uploads/images/destinations/thumb/15c22bb390247739d7b56678fe9cbf57.png', 18, '2019-10-10 19:54:24', '2019-05-03 09:44:54'),
(15, 'about-img-01.png', '.png', 1267, 'uploads/images/destinations/667e3f007d1b3b755020699994019659.png', 'uploads/images/destinations/thumb/667e3f007d1b3b755020699994019659.png', 25, '2019-10-10 19:54:14', '2019-05-22 10:58:06'),
(16, 'destinations-guide-01.png', '.png', 789, 'uploads/images/destinations/d192e4fbdeff670baaf896de5bb905cd.png', 'uploads/images/destinations/thumb/d192e4fbdeff670baaf896de5bb905cd.png', 26, '2019-10-10 19:53:29', '2019-05-22 11:39:31'),
(17, 'destinations-05.png', '.png', 691, 'uploads/images/destinations/07d0846ff282b84505036ddb7bf06ccb.png', 'uploads/images/destinations/thumb/07d0846ff282b84505036ddb7bf06ccb.png', 27, '2019-10-10 19:56:08', '2019-05-22 11:43:53'),
(18, 'destinations-02.png', '.png', 620, 'uploads/images/destinations/a9df4816d95443ac75918631bea25055.png', 'uploads/images/destinations/thumb/a9df4816d95443ac75918631bea25055.png', 28, '2019-10-10 19:56:33', '2019-05-22 11:44:48'),
(19, 'destinations-05.png', '.png', 691, 'uploads/images/destinations/8d1c58b56f027b08a71d3945cea3773a.png', 'uploads/images/destinations/thumb/8d1c58b56f027b08a71d3945cea3773a.png', 30, '2019-10-10 19:56:38', '2019-05-22 12:33:48'),
(20, 'taj-mahal.jpg', '.jpg', 403, 'uploads/images/destinations/8430bf338f7438a6170b7d164883e9df.jpg', 'uploads/images/destinations/thumb/8430bf338f7438a6170b7d164883e9df.jpg', 33, '2019-06-25 05:00:39', '2019-05-22 14:42:57'),
(27, '220px-Ninjas_in_Pyjamaslogo_square.png', '.png', 30, 'uploads/images/destinations/816be9aaba5ccbffba806e4060280141.png', 'uploads/images/destinations/thumb/816be9aaba5ccbffba806e4060280141.png', 36, '2019-06-17 06:12:06', '2019-06-17 21:12:06'),
(28, 'aus.jpg', '.jpg', 39, 'uploads/images/destinations/8b1d6becfd7f7b23b51e81ce116dc383.jpg', 'uploads/images/destinations/thumb/8b1d6becfd7f7b23b51e81ce116dc383.jpg', 37, '2019-06-17 06:15:12', '2019-06-17 21:15:12'),
(29, '220px-Ninjas_in_Pyjamaslogo_square.png', '.png', 30, 'uploads/images/destinations/644b1dd129388af78f56e6959015c196.png', 'uploads/images/destinations/thumb/644b1dd129388af78f56e6959015c196.png', 38, '2019-06-17 06:22:05', '2019-06-17 21:22:05'),
(30, 'destinations-06.png', '.png', 613, 'uploads/images/destinations/4265627ce0ffd8fb829a7acdb1a8395a.png', 'uploads/images/destinations/thumb/4265627ce0ffd8fb829a7acdb1a8395a.png', 39, '2019-10-10 19:53:00', '2019-06-17 21:45:11'),
(32, 'destinations-guide-04.png', '.png', 807, 'uploads/images/destinations/454808c0ea6e3790bd9168780b7548ea.png', 'uploads/images/destinations/thumb/454808c0ea6e3790bd9168780b7548ea.png', 42, '2019-10-10 19:54:07', '2019-07-05 09:21:03'),
(33, 'destinations-03.png', '.png', 476, 'uploads/images/destinations/d7284def1f3444ceb2ee8055918e47b5.png', 'uploads/images/destinations/thumb/d7284def1f3444ceb2ee8055918e47b5.png', 45, '2019-10-10 18:34:34', '2019-08-28 13:17:45'),
(34, 'destinations-01.png', '.png', 701, 'uploads/images/destinations/fe5d6e0b16aeb67cb94423710f10d654.png', 'uploads/images/destinations/thumb/fe5d6e0b16aeb67cb94423710f10d654.png', 43, '2019-10-10 19:53:07', '2019-08-28 13:31:56'),
(36, 'destination-cairo.png', '.png', 1126, 'uploads/images/destinations/0c4237a887b9a9cd657d7ba273f4445e.png', 'uploads/images/destinations/thumb/0c4237a887b9a9cd657d7ba273f4445e.png', 47, '2019-09-23 15:51:05', '2019-09-23 18:51:05'),
(42, 'destinations-02.png', '.png', 620, 'uploads/images/destinations/adc52abcfa20c95fa339a28f00d6e5c8.png', 'uploads/images/destinations/thumb/adc52abcfa20c95fa339a28f00d6e5c8.png', NULL, '2019-10-08 18:28:51', '2019-10-08 09:28:51'),
(43, 'destinations-guide-05.png', '.png', 799, 'uploads/images/destinations/a6fd3ae60e61af8057ae6d6274387e79.png', 'uploads/images/destinations/thumb/a6fd3ae60e61af8057ae6d6274387e79.png', 41, '2019-10-10 19:53:59', '2019-10-10 10:53:59'),
(44, 'destinations-04.png', '.png', 524, 'uploads/images/destinations/3f9b78367b28428de9714e301a4f0885.png', 'uploads/images/destinations/thumb/3f9b78367b28428de9714e301a4f0885.png', 31, '2019-10-10 19:56:44', '2019-10-10 10:56:44');

-- --------------------------------------------------------

--
-- Table structure for table `email_config`
--

CREATE TABLE `email_config` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` varchar(11) DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `sender_email` varchar(255) DEFAULT NULL,
  `sender_cc` varchar(255) DEFAULT NULL,
  `sender_bcc` varchar(255) DEFAULT NULL,
  `sender_subject` varchar(255) DEFAULT NULL,
  `receiver_subject` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_config`
--

INSERT INTO `email_config` (`id`, `type`, `sender_name`, `sender_email`, `sender_cc`, `sender_bcc`, `sender_subject`, `receiver_subject`) VALUES
(1, 'thank_you', 'London Star Travel', 'iammusabutt@gmail.com', '', '', 'Inquiry', 'Inquiry'),
(2, 'contact_us', 'London Star Travel', 'd3z3l@yahoo.com', '', '', 'Contact Us', 'Contact Us');

-- --------------------------------------------------------

--
-- Table structure for table `flights`
--

CREATE TABLE `flights` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `flight_from` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flight_stopover` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flight_to` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flight_airline` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flight_route` enum('Return','One way','Multi-city') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flight_price` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flight_aircraft_class` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flight_protection` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flight_status` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flight_count` int(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `flights`
--

INSERT INTO `flights` (`id`, `flight_from`, `flight_stopover`, `flight_to`, `flight_airline`, `flight_route`, `flight_price`, `flight_aircraft_class`, `flight_protection`, `flight_status`, `flight_count`) VALUES
(2820, '14', NULL, '47', '18', 'Return', '569', '10', '13', NULL, NULL),
(2819, '43', NULL, '47', '18', 'Return', '200', '10', '13', NULL, NULL),
(2818, '40', NULL, '22', '16', 'Return', '200', '14', '13', NULL, NULL),
(2817, '42', NULL, '44', '18', 'Return', '555', '15', '13', NULL, NULL),
(2816, '31', NULL, '30', '22', 'Return', '456', '10', '13', NULL, NULL),
(2821, '45', NULL, '47', '16', 'Return', '473', '15', '13', NULL, NULL),
(2822, '45', NULL, '47', '17', 'Return', '345', '10', '13', NULL, NULL),
(2823, '31', NULL, '47', '21', 'Return', '723', '10', '13', NULL, NULL),
(2824, '14', NULL, '43', '17', 'Return', '673', '14', '14', NULL, NULL),
(2825, '14', NULL, '45', '16', 'Return', '200', '9', '13', NULL, NULL),
(2826, '14', NULL, '49', '38', 'Return', '673', '10', '13', NULL, NULL),
(2827, '16', NULL, '45', '15', 'Return', '544', '14', '13', NULL, NULL),
(2828, '14', NULL, '48', '18', 'Return', '204', '10', '13', NULL, NULL),
(2830, '14', NULL, '45', '15', 'Return', '673', '10', '13', NULL, NULL),
(2831, '15', NULL, '45', '22', 'Return', '204', '14', '13', NULL, NULL),
(2840, '17', NULL, '14', '38', 'One way', '204', '10', '14', NULL, NULL),
(2833, '16', NULL, '49', '38', 'Return', '204', '14', '13', NULL, NULL),
(2834, '14', NULL, '37', '22', 'Return', '200', '9', '14', NULL, NULL),
(2835, '15', NULL, '16', '16', 'Return', '555', '9', '14', NULL, NULL),
(2836, '14', NULL, '15', '16', 'One way', '876', '9', '13', NULL, NULL),
(2837, '14', NULL, '15', '16', 'One way', '544', '9', '13', NULL, NULL),
(2838, '14', NULL, '15', '16', 'One way', '534', '9', '13', NULL, NULL),
(2839, '14', NULL, '15', '16', 'One way', '234', '9', '13', NULL, NULL),
(2841, '17', NULL, '14', '38', 'One way', '204', '10', '14', NULL, NULL),
(2842, '14', NULL, '52', '17', 'Return', '555', '10', '13', NULL, NULL),
(2843, '14', NULL, '51', '20', 'Return', '675', '9', '13', NULL, NULL),
(2844, '14', NULL, '50', '38', 'Return', '988', '14', '14', NULL, NULL),
(2845, '14', NULL, '19', '20', 'Return', '673', '9', '13', NULL, NULL),
(2846, '14', NULL, '18', '17', 'Return', '544', '10', '13', NULL, NULL),
(2847, '14', NULL, '23', '38', 'Return', '546', '14', '14', NULL, NULL),
(2848, '14', NULL, '32', '18', 'Return', '654', '14', '14', NULL, NULL),
(2849, '14', NULL, '53', '26', 'Return', '677', '10', '13', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'agents', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `itinerary`
--

CREATE TABLE `itinerary` (
  `id` int(11) UNSIGNED NOT NULL,
  `package_id` int(11) UNSIGNED DEFAULT NULL,
  `type` enum('hajj','umrah') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day` int(11) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `itinerary`
--

INSERT INTO `itinerary` (`id`, `package_id`, `type`, `day`, `title`, `detail`) VALUES
(7, 1, 'hajj', 1, 'Arrival and Evening Dhow Cruise', 'Arrive at the airport and transfer to hotel. Check-in time at the hotel will be at 2:00 PM. In the evening, enjoy a tasty dinner on the Dhow cruise. Later, head back to the hotel for a comfortable overnight stay.'),
(8, 1, 'hajj', 2, 'City Tour and Evening Free for Leisure', 'After breakfast, proceed for tour of Dubai city. Visit Jumeirah Mosque, World Trade Centre, Palaces and Dubai Museum. Enjoy your overnight stay at the hotel.In the evening, enjoy a tasty dinner on the Dhow cruise. Later, head back to the hotel for a comfo'),
(9, 1, 'hajj', 3, 'Desert Safari with Dinner', 'Relish a yummy breakfast and later, proceed to explore the city on your own. Enjoy shopping at Mercato Shopping Mall, Dubai Mall and Wafi City. In the evening, enjoy the desert safari experience and belly dance performance. Relish a mouth-watering barbecu'),
(10, 1, 'hajj', 4, 'Day at leisure', 'Savour a satiating breakfast and relax for a while. Day Free for leisure. Overnight stay will be arranged in Dubai. In the evening, enjoy a tasty dinner on the Dhow cruise. Later, head back to the hotel for a comfortable overnight stay.'),
(11, 1, 'hajj', 5, 'Departure', 'Fill your tummy with yummy breakfast and relax for a while. Later, check out from the hotel and proceed for your onward journey.In the evening, enjoy a tasty dinner on the Dhow cruise. Later, head back to the hotel for a comfortable overnight stay.'),
(12, 14, 'umrah', 1, 'Departure', 'Fill your tummy with yummy breakfast and relax for a while. Later, check out from the hotel and proceed for your onward journey.In the evening, enjoy a tasty dinner on the Dhow cruise. Later, head back to the hotel for a comfortable overnight stay.'),
(13, 14, 'umrah', 2, 'Day at leisure', 'Savour a satiating breakfast and relax for a while. Day Free for leisure. Overnight stay will be arranged in Dubai. In the evening, enjoy a tasty dinner on the Dhow cruise. Later, head back to the hotel for a comfortable overnight stay.');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(6, '::1', 'wordpress', 1571722668);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `sale_price` int(11) DEFAULT NULL,
  `no_of_days` int(3) UNSIGNED DEFAULT NULL,
  `departure_date` varchar(255) DEFAULT NULL,
  `return_date` varchar(255) DEFAULT NULL,
  `status` enum('Draft','Published') DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `type`, `title`, `sub_title`, `price`, `sale_price`, `no_of_days`, `departure_date`, `return_date`, `status`, `date`) VALUES
(24, 'international', 'Travel Package to California', 'Huge Savings on Early Booking!', 700, 600, 5, '1571814000', '1572505200', 'Published', NULL),
(25, 'hajj', '5 Star Hajj Package', 'Huge Savings on Early Booking!', 700, 600, 8, '1571814000', '1571814000', 'Published', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `package_images`
--

CREATE TABLE `package_images` (
  `id` int(11) NOT NULL,
  `type` enum('featured','gallery') DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `ext` char(5) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `date` timestamp NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `package_images`
--

INSERT INTO `package_images` (`id`, `type`, `title`, `ext`, `size`, `path`, `thumb`, `package_id`, `created_at`, `date`) VALUES
(107, 'featured', 'destinations-02.png', '.png', 620, 'uploads/images/packages/galleries/34ec939c1d6160ec085bcf4c19b07ede.png', 'uploads/images/packages/galleries/thumb/34ec939c1d6160ec085bcf4c19b07ede.png', 1, '2019-10-08 15:52:00', '2019-06-03 12:49:08'),
(157, 'gallery', '0e7a2aa1b7f1da8715d98c4a7b12cdc9.jpg', '.jpg', 86, 'uploads/images/packages/galleries/0c7fcf2472f9c18cf19e325149fcad37.jpg', 'uploads/images/packages/galleries/thumb/0c7fcf2472f9c18cf19e325149fcad37.jpg', 1, '2019-09-20 19:34:44', '2019-09-20 10:34:44'),
(102, 'featured', 'flight-delhi.png', '.png', 100, 'uploads/images/packages/galleries/ca369e175fbdf137f32fe5a8d6dbf0b9.png', 'uploads/images/packages/galleries/thumb/ca369e175fbdf137f32fe5a8d6dbf0b9.png', 11, '2019-06-02 20:02:53', '2019-06-03 11:02:53'),
(134, 'featured', '39.png', '.png', 48, 'uploads/images/packages/galleries/bb877014432ee85d67c2716bba5e89a9.png', 'uploads/images/packages/galleries/thumb/bb877014432ee85d67c2716bba5e89a9.png', 15, '2019-06-17 06:55:24', '2019-06-17 21:55:08'),
(158, 'gallery', 'b2fd2a560612d0bf05a8c463cd01c7f7.jpg', '.jpg', 171, 'uploads/images/packages/galleries/bacedfc2d27a5e421bad5859af467232.jpg', 'uploads/images/packages/galleries/thumb/bacedfc2d27a5e421bad5859af467232.jpg', 1, '2019-09-20 19:34:53', '2019-09-20 10:34:53'),
(196, 'featured', '60774740_327016267942607_2160410795186323456_o_(1).jpg', '.jpg', 181, 'uploads/images/packages/galleries/adfd648144e9ace30a2db670ed2b3c04.jpg', 'uploads/images/packages/galleries/thumb/adfd648144e9ace30a2db670ed2b3c04.jpg', 23, '2019-10-22 16:21:53', '2019-10-22 07:21:53'),
(104, 'featured', 'home-umrah-01.png', '.png', 1006, 'uploads/images/packages/galleries/b880750b7fd41291d0ff556508dbfaa3.png', 'uploads/images/packages/galleries/thumb/b880750b7fd41291d0ff556508dbfaa3.png', 14, '2019-09-23 15:40:03', '2019-06-03 12:08:40'),
(103, 'featured', 'flight-hongkong.png', '.png', 88, 'uploads/images/packages/galleries/bbf07047a8f46dec4166342a536c42b6.png', 'uploads/images/packages/galleries/thumb/bbf07047a8f46dec4166342a536c42b6.png', 13, '2019-06-02 20:05:17', '2019-06-03 11:05:17'),
(101, 'featured', 'flight-hongkong.png', '.png', 88, 'uploads/images/packages/galleries/88d62145ac167ef39ae5dc2ff4763685.png', 'uploads/images/packages/galleries/thumb/88d62145ac167ef39ae5dc2ff4763685.png', 10, '2019-06-03 07:53:44', '2019-06-03 10:53:44'),
(152, 'featured', 'Fnatic_logo.png', '.png', 11, 'uploads/images/packages/galleries/dfc9161e4f82ce56b1df69bf77da795b.png', 'uploads/images/packages/galleries/thumb/dfc9161e4f82ce56b1df69bf77da795b.png', 15, '2019-07-24 09:52:36', '2019-07-24 12:52:36'),
(153, 'featured', 'Fnatic_logo.png', '.png', 11, 'uploads/images/packages/galleries/9b3069b1c93f6e789e70acf6bf9da72e.png', 'uploads/images/packages/galleries/thumb/9b3069b1c93f6e789e70acf6bf9da72e.png', 16, '2019-07-24 10:00:04', '2019-07-24 13:00:04'),
(154, 'featured', 'Fnatic_logo.png', '.png', 11, 'uploads/images/packages/galleries/7121bc30958f5815d9245ea15ffe6a0e.png', 'uploads/images/packages/galleries/thumb/7121bc30958f5815d9245ea15ffe6a0e.png', 17, '2019-07-25 11:31:06', '2019-07-25 12:06:17'),
(159, 'gallery', '59c8a4b4e0bdff31da87d04eecf70dd4.jpg', '.jpg', 128, 'uploads/images/packages/galleries/8a110359c3b245e5e38b2a455016cef4.jpg', 'uploads/images/packages/galleries/thumb/8a110359c3b245e5e38b2a455016cef4.jpg', 1, '2019-09-20 19:34:58', '2019-09-20 10:34:58'),
(186, 'featured', 'package-03.png', '.png', 703, 'uploads/images/packages/galleries/39f1d84caf9804bcc23f3d4d4c412f70.png', 'uploads/images/packages/galleries/thumb/39f1d84caf9804bcc23f3d4d4c412f70.png', 19, '2019-10-09 15:57:17', '2019-10-09 06:57:17'),
(194, 'featured', '60774740_327016267942607_2160410795186323456_o_(1).jpg', '.jpg', 181, 'uploads/images/packages/galleries/9f9e17aa4537ef066835d62927be5591.jpg', 'uploads/images/packages/galleries/thumb/9f9e17aa4537ef066835d62927be5591.jpg', NULL, '2019-10-21 09:49:09', '2019-10-21 12:49:09'),
(195, 'featured', '60774740_327016267942607_2160410795186323456_o.jpg', '.jpg', 181, 'uploads/images/packages/galleries/5a61cf20034731fba216a1a9cd6a0060.jpg', 'uploads/images/packages/galleries/thumb/5a61cf20034731fba216a1a9cd6a0060.jpg', 22, '2019-10-21 09:49:21', '2019-10-21 12:49:21'),
(191, 'featured', '66218311_545768989290649_2213734661002100736_n.jpg', '.jpg', 236, 'uploads/images/packages/galleries/97424488e4fd6d02b84fbdb2f4b33dda.jpg', 'uploads/images/packages/galleries/thumb/97424488e4fd6d02b84fbdb2f4b33dda.jpg', 18, '2019-10-17 15:54:23', '2019-10-17 06:54:23'),
(192, 'featured', '60774740_327016267942607_2160410795186323456_o_(1).jpg', '.jpg', 181, 'uploads/images/packages/galleries/44159c1664d1bb033c06b3d59aab1e99.jpg', 'uploads/images/packages/galleries/thumb/44159c1664d1bb033c06b3d59aab1e99.jpg', 20, '2019-10-17 16:31:29', '2019-10-17 07:31:29'),
(193, 'featured', '60774740_327016267942607_2160410795186323456_o.jpg', '.jpg', 181, 'uploads/images/packages/galleries/920ff8707370f43eaf96499dcf403ddf.jpg', 'uploads/images/packages/galleries/thumb/920ff8707370f43eaf96499dcf403ddf.jpg', 21, '2019-10-21 08:49:35', '2019-10-21 11:49:35'),
(197, 'featured', '60774740_327016267942607_2160410795186323456_o_(1).jpg', '.jpg', 181, 'uploads/images/packages/galleries/64ce69d8135b8c0e0bbef0a3fed38946.jpg', 'uploads/images/packages/galleries/thumb/64ce69d8135b8c0e0bbef0a3fed38946.jpg', 24, '2019-10-22 16:39:41', '2019-10-22 07:39:41'),
(198, 'featured', '60774740_327016267942607_2160410795186323456_o_(1).jpg', '.jpg', 181, 'uploads/images/packages/galleries/b5e3d1992e9707cc3946e2d7cadbe6a2.jpg', 'uploads/images/packages/galleries/thumb/b5e3d1992e9707cc3946e2d7cadbe6a2.jpg', 25, '2019-10-22 16:46:17', '2019-10-22 07:46:17');

-- --------------------------------------------------------

--
-- Table structure for table `package_meta`
--

CREATE TABLE `package_meta` (
  `pmeta_id` int(11) UNSIGNED NOT NULL,
  `package_id` int(11) UNSIGNED DEFAULT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `postmeta`
--

CREATE TABLE `postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `protections`
--

CREATE TABLE `protections` (
  `id` int(11) UNSIGNED NOT NULL,
  `protection_name` varchar(255) DEFAULT NULL,
  `protection_description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `protections`
--

INSERT INTO `protections` (`id`, `protection_name`, `protection_description`) VALUES
(13, 'IATA', ''),
(14, 'ATOL', '');

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE `relations` (
  `id` int(11) UNSIGNED NOT NULL,
  `action_type` varchar(255) DEFAULT NULL,
  `relation_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `relations`
--

INSERT INTO `relations` (`id`, `action_type`, `relation_value`) VALUES
(215, 'home', '14'),
(219, 'home', '16'),
(220, 'home', '17'),
(224, 'home', '45');

-- --------------------------------------------------------

--
-- Table structure for table `seo`
--

CREATE TABLE `seo` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(11) UNSIGNED DEFAULT NULL,
  `page` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seo`
--

INSERT INTO `seo` (`id`, `page_id`, `page`, `title`, `description`) VALUES
(1, NULL, 'home', '', ''),
(2, 39, 'destination', 'Cheap Flights to Africa', 'Cheap Flights to Africa'),
(3, 1, 'destination', 'Cheap Flights to Afghanistan', 'Cheap Flights to Afghanistan'),
(4, 44, 'destination', 'Cheap Flights to Saudi Dakar', 'Cheap Flights to Saudi Dakar'),
(5, 1, 'hajj', '3 Weeks Economy Hajj Package!', '3 Weeks Economy Hajj Package!'),
(9, 17, 'umrah', 'Laravel', 'qwasdasd'),
(10, 14, 'umrah', '3 days trip to Mushkpuri', '3 days trip to Mushkpuri'),
(11, 45, 'destination', '', ''),
(12, 46, 'destination', '', ''),
(13, 47, 'destination', '', ''),
(14, 18, 'hajj', '5 Star Hajj Package', 'Huge Savings on Early Booking!'),
(15, 19, 'umrah', '', ''),
(16, 20, 'trip', '', ''),
(17, 21, 'International', '', ''),
(18, 22, 'international', '', ''),
(19, NULL, 'destination', '', ''),
(20, 48, 'destination', '', ''),
(21, 23, 'international', '', ''),
(22, 24, 'international', 'Travel Package to California Now!', 'Travel Package to California'),
(23, 25, 'hajj', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `setting_name` varchar(225) DEFAULT NULL,
  `setting_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `setting_name`, `setting_value`) VALUES
(1, 'site_title', 'Almis Travel'),
(2, 'site_description', 'Almis Travel'),
(3, 'site_url', ''),
(4, 'primary_phone', '+44 7467 529599'),
(5, 'secondary_phone', '+44 7467 8469271'),
(6, 'admin_email', 'iammusabutt@gmail.com'),
(7, 'medium_facebook', 'https://www.facebook.com/'),
(8, 'medium_twitter', 'https://www.twitter.com/'),
(9, 'medium_linkedin', 'https://www.linkedin.com/'),
(10, 'medium_gplus', 'https://www.plus.google.com/'),
(11, 'medium_instagram', 'https://www.instagram.com/'),
(12, 'medium_pinterest', 'https://www.pinterest.com/'),
(13, 'currency_unit', '£'),
(14, 'admin_address', '288 Old Kent Road, London SE1 5UE'),
(42, 'post_type', 'a:3:{s:14:\"post_type_name\";s:13:\"international\";s:5:\"label\";s:13:\"International\";s:14:\"singular_label\";s:13:\"International\";}'),
(44, 'post_type', 'a:3:{s:14:\"post_type_name\";s:4:\"hajj\";s:5:\"label\";s:4:\"Hajj\";s:14:\"singular_label\";s:4:\"Hajj\";}'),
(45, 'post_type', 'a:3:{s:14:\"post_type_name\";s:5:\"umrah\";s:5:\"label\";s:5:\"Umrah\";s:14:\"singular_label\";s:5:\"Umrah\";}');

-- --------------------------------------------------------

--
-- Table structure for table `terminals`
--

CREATE TABLE `terminals` (
  `id` bigint(20) NOT NULL,
  `city_id` int(11) UNSIGNED NOT NULL,
  `country_id` int(11) UNSIGNED NOT NULL,
  `terminal_name` text DEFAULT NULL,
  `terminal_short_code` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terminals`
--

INSERT INTO `terminals` (`id`, `city_id`, `country_id`, `terminal_name`, `terminal_short_code`) VALUES
(14, 2, 230, 'London Heathrow', 'LHR'),
(15, 5, 132, 'Lilongwe', 'LLW'),
(16, 6, 246, 'Harare', 'HRE'),
(17, 1, 167, 'Lahore', 'LHE'),
(18, 4, 81, 'Accra', 'ACC'),
(19, 7, 227, 'Entebbe', 'EBB'),
(20, 8, 113, 'Nairobi', 'NBO'),
(22, 10, 161, 'Lagos', 'LOS'),
(23, 11, 1, 'Johannesburg', 'JNB'),
(24, 12, 216, 'Dar-Es-Salaam', 'DAR'),
(25, 13, 67, 'Addis Ababa', 'ADD'),
(26, 14, 200, 'Cape Town', 'CPT'),
(27, 3, 167, 'Karachi', 'KHI'),
(28, 15, 167, 'Islamabad', 'ISB'),
(29, 16, 167, 'Peshawar', 'PEW'),
(30, 17, 99, 'Delhi', 'DEL'),
(31, 18, 99, 'Mumbai', 'BOM'),
(32, 19, 99, 'Ahmedabad', 'AMD'),
(33, 20, 99, 'Kolkata', 'CCU'),
(34, 21, 99, 'Bengaluru', 'BLR'),
(35, 22, 99, 'Hyderabad', 'HYD'),
(36, 23, 49, 'Kinshasa', 'FIH'),
(37, 24, 200, 'Durban', 'DUR'),
(38, 25, 191, 'Dakar', 'DKR'),
(39, 26, 44, 'Shanghai', 'PVG'),
(40, 27, 44, 'Guangzhou', 'CAN'),
(41, 28, 44, 'Beijing', 'BJS'),
(42, 29, 217, 'Bangkok', 'BKK'),
(43, 30, 199, 'Hargeisa', 'HGA'),
(44, 31, 229, 'Abu Dhabi', 'AUH'),
(45, 32, 190, 'Dammam', 'DMM'),
(46, 33, 179, 'Doha', 'DOH'),
(47, 34, 2, 'Tirana', 'TIA'),
(48, 35, 190, 'Jeddah', 'JED'),
(49, 36, 190, 'Medina', 'MED'),
(50, 38, 80, 'Frankfurt', 'FRA'),
(51, 42, 106, 'Rome', 'FCO'),
(52, 40, 72, 'Venice', 'VCE'),
(53, 44, 56, 'Copenhagen', 'CPH');

-- --------------------------------------------------------

--
-- Table structure for table `termmeta`
--

CREATE TABLE `termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(85, 'Cheap Flights to Africa', 'cheap_flights_to_africa', 0),
(87, 'Cheap Flights to Europe', 'cheap_flights_to_europe', 0),
(97, 'Cheap Flights to Asia', 'cheap_flights_to_asia', 0),
(108, 'Lowest Fare', 'lowest_fare', 0);

-- --------------------------------------------------------

--
-- Table structure for table `term_relationships`
--

CREATE TABLE `term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `term_relationships`
--

INSERT INTO `term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(2816, 76, 0),
(2817, 76, 0),
(2818, 75, 0),
(2818, 76, 0),
(2819, 98, 0),
(2824, 75, 0),
(2825, 98, 0),
(2826, 98, 0),
(2827, 76, 0),
(2828, 76, 0),
(2834, 75, 0),
(2839, 98, 0),
(2840, 77, 0),
(2841, 98, 0),
(2842, 77, 0),
(2843, 77, 0),
(2844, 77, 0),
(2845, 75, 0),
(2846, 75, 0),
(2847, 75, 0),
(2848, 76, 0),
(2849, 77, 0);

-- --------------------------------------------------------

--
-- Table structure for table `term_taxonomy`
--

CREATE TABLE `term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `term_taxonomy`
--

INSERT INTO `term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(75, 85, 'flights_category', '', 0, 0),
(76, 97, 'flights_category', '', 0, 0),
(77, 87, 'flights_category', '', 0, 0),
(98, 108, 'flights_category', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `comment` text DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `status` enum('Draft','Published') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `comment`, `name`, `designation`, `company`, `status`) VALUES
(1, 'Falcons Travel is a <em>fabulous Travel Agent</em> to purchase affordable tickets to Entebbe. Every recommendation they made was incredible, as they considered our financial plan, I would unquestionably book my late spring flights with Falcons Travel again as they made our trip simple and push free.', 'Helen', 'CEO', 'DrCodeX Technologies', NULL),
(3, 'With a low price, excellent service and helpful staff, Falcons Travel helps me to make my <em>Umrah</em> journey, a best experience of my life.', 'Abdi Jama', '', '', NULL),
(4, 'I have booked with Falcons TRavel in the past and have always had very good service. My last booking went without a hitch and I would recommend there services to all my friends. I will be booking my next flight with this firm next year.', 'John Doe', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', 'BXNTWGCKaA8xKwiRvds8.e5b3907da7a72b4f97b', 1533037866, 'JEaBRDflIOU1wGRFFIcXYe', 1268889823, 1571806762, 1, 'Admin', 'istrator', 'Yippy', '0'),
(3, '::1', 'iammusabutt@gmail.com', '$2y$08$gLkZepVaqRVfDfMurWGxxOOHk1QVpFswmVrZ/xGHjAm4jujTx1SMS', NULL, 'iammusabutt@gmail.com', NULL, 'PrfEYeja3S9MimTsHpgxpu55a9e2a908984cd289', 1532970418, NULL, 1532917044, NULL, 1, 'Musa', 'Aman', 'DrCodeX Technologies', '+923367366914');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(73, 1, 1),
(62, 3, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `airlines`
--
ALTER TABLE `airlines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `airline_logos`
--
ALTER TABLE `airline_logos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `continents`
--
ALTER TABLE `continents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `destination_images`
--
ALTER TABLE `destination_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_config`
--
ALTER TABLE `email_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flights`
--
ALTER TABLE `flights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itinerary`
--
ALTER TABLE `itinerary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_images`
--
ALTER TABLE `package_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_meta`
--
ALTER TABLE `package_meta`
  ADD PRIMARY KEY (`pmeta_id`);

--
-- Indexes for table `postmeta`
--
ALTER TABLE `postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `protections`
--
ALTER TABLE `protections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `relations`
--
ALTER TABLE `relations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terminals`
--
ALTER TABLE `terminals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `termmeta`
--
ALTER TABLE `termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `term_relationships`
--
ALTER TABLE `term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `term_taxonomy`
--
ALTER TABLE `term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `airlines`
--
ALTER TABLE `airlines`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `airline_logos`
--
ALTER TABLE `airline_logos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `blocks`
--
ALTER TABLE `blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `continents`
--
ALTER TABLE `continents`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;

--
-- AUTO_INCREMENT for table `destinations`
--
ALTER TABLE `destinations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `destination_images`
--
ALTER TABLE `destination_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `email_config`
--
ALTER TABLE `email_config`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `flights`
--
ALTER TABLE `flights`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2850;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `itinerary`
--
ALTER TABLE `itinerary`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `package_images`
--
ALTER TABLE `package_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT for table `package_meta`
--
ALTER TABLE `package_meta`
  MODIFY `pmeta_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `postmeta`
--
ALTER TABLE `postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `protections`
--
ALTER TABLE `protections`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `relations`
--
ALTER TABLE `relations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=225;

--
-- AUTO_INCREMENT for table `seo`
--
ALTER TABLE `seo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `terminals`
--
ALTER TABLE `terminals`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `termmeta`
--
ALTER TABLE `termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `term_taxonomy`
--
ALTER TABLE `term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

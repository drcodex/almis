<?php
/**
 * Name:    Posts Model
 * Author:  Ben Edmunds
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Destinations_model extends CI_Model
{
	public function __construct()
    {
		parent::__construct();
	}
	function get_cities_destinations($city_type)
	{
		$this->db->select('destinations.id AS destination_id, destinations.select_id, destinations.type, destinations.title, destinations.slug, destinations.description, destinations.author, destinations.status, destinations.featured, destinations.date, cities.*');
		$this->db->join('cities', 'cities.id = destinations.select_id', 'inner');
		$this->db->where('destinations.type', $city_type);
		$result = $this->db->get('destinations');
		return $result->result_array();
	}
	function get_cities()
	{
		$result = $this->db->get('cities');
		return $result->result();
	}
	function get_cities_convent()
	{
		$this->db->select('cities.id, cities.city_name AS name');
		$result = $this->db->get('cities');
		return $result->result();
	}
	function get_city_by_id($destinations_select)
	{
		$this->db->select('cities.id, cities.city_name AS name');
		$this->db->where('cities.id', $destinations_select);
		$result = $this->db->get('cities');
		return $result->row();
	}
	function get_countries_destinations($country_type)
	{
		$this->db->select('destinations.id AS destination_id, destinations.select_id, destinations.type, destinations.title, destinations.slug, destinations.description, destinations.author, destinations.status, destinations.featured, destinations.date, countries.*');
		$this->db->join('countries', 'countries.id = destinations.select_id', 'inner');
		$this->db->where('destinations.type', $country_type);
		$result = $this->db->get('destinations');
		return $result->result_array();
	}
	function get_countries()
	{
		$result = $this->db->get('countries');
		return $result->result();
	}
	function get_countries_convent()
	{
		$this->db->select('countries.id, countries.country_name AS name');
		$result = $this->db->get('countries');
		return $result->result();
	}
	function get_country_by_id($destinations_select)
	{
		$this->db->select('countries.id, countries.country_name AS name');
		$this->db->where('countries.id', $destinations_select);
		$result = $this->db->get('countries');
		return $result->row();
	}
	function get_continents_destinations($continent_type)
	{
		$this->db->select('destinations.id AS destination_id, destinations.select_id, destinations.type, destinations.title, destinations.slug, destinations.description, destinations.author, destinations.status, destinations.featured, destinations.date, continents.*');
		$this->db->join('continents', 'continents.id = destinations.select_id', 'inner');
		$this->db->where('destinations.type', $continent_type);
		$result = $this->db->get('destinations');
		return $result->result_array();
	}
	function get_continents()
	{
		$result = $this->db->get('continents');
		return $result->result();
	}
	function get_continents_convent()
	{
		$this->db->select('continents.id, continents.continent_name AS name');
		$result = $this->db->get('continents');
		return $result->result();
	}
	function get_continent_by_id($destinations_select)
	{
		$this->db->select('continents.id, continents.continent_name AS name');
		$this->db->where('continents.id', $destinations_select);
		$result = $this->db->get('continents');
		return $result->row();
	}
	function add_destination($additional_data)
	{
		$this->db->insert('destinations',$additional_data);
		return $this->db->insert_id();
	}
	function delete_relations($id)
	{
		$this->db->where('relation_value', $id);
		$this->db->delete('relations');
	}
	function update_destination($destination_id, $additional_data)
	{
		$this->db->where('id', $destination_id);
		$this->db->update('destinations',$additional_data);
	}
	function update_featured_image_destination_id($featured_image_id, $other_data)
	{
		$check = $this->db->where('id', $featured_image_id);
		$this->db->update('destination_images', $other_data);
	}
	function upload_destination_featured_image($destination_id, $data)
	{
		$this->db->where('destination_id', $destination_id);
		$this->db->where('destination_id is NOT NULL', NULL, FALSE);
		$res = $this->db->get('destination_images');

		if ( $res->num_rows() > 0 ) 
		{
			$this->db->where('destination_id', $destination_id);
            $result = $this->db->update('destination_images', $data);
			return $this->db->insert_id();
		} else {
            $result = $this->db->insert('destination_images', $data);
			return $this->db->insert_id();
		}
	}
	function upload_destination_images($destination_id, $data)
	{
		$this->db->where('destination_id', $destination_id);
		$this->db->where('destination_id is NOT NULL', NULL, FALSE);
		$result = $this->db->insert('destination_images', $data);
		return $this->db->insert_id();
	}
	function upload_destination_image($destination_id, $data)
	{
		$this->db->where('destination_id', $destination_id);
		$this->db->where('destination_id is NOT NULL', NULL, FALSE);
		$res = $this->db->get('destination_images');
		if ( $res->num_rows() > 0 ) 
		{
			$this->db->where('destination_id', $destination_id);
            $this->db->update('destination_images', $data);
		} else {
			$this->db->where('destination_id', $destination_id);
            $this->db->insert('destination_images', $data);
		}
	}
	function delete_destination($id)
	{
		$this->db->where('destinations.id', $id);
		$this->db->delete('destinations');
	}
	function get_destinations()
	{
		$this->db->select('destinations.*, destination_images.title AS des_image_title, destination_images.thumb, destination_images.path, relations.relation_value');
		$this->db->join('destination_images', 'destination_images.destination_id = destinations.id', 'left');
		$this->db->join('relations', 'relations.relation_value = destinations.id', 'left');
		$this->db->order_by('destinations.id', 'ASC');
		$result = $this->db->get('destinations');
		return $result->result_array();
	}
	function get_destinations_featured()
	{
		$this->db->select('destinations.*, destination_images.title AS des_image_title, destination_images.thumb, destination_images.path');
		$this->db->join('destination_images', 'destination_images.destination_id = destinations.id', 'left');
		$this->db->where('destinations.featured', '1');
		$this->db->order_by('destinations.id', 'ASC');
		$result = $this->db->get('destinations');
		return $result->result_array();
	}
	function get_destinations_location($type, $table, $select_id)
	{
		$this->db->select("destinations.select_id, `$table`.`$type` AS name");
		$this->db->join($table, "`$table`.`id` = destinations.select_id");
		$this->db->where('select_id', $select_id);
		$this->db->order_by('destinations.id', 'ASC');
		$result = $this->db->get('destinations');
		return $result->row_array();
	}
	function get_destinations_by_type()
	{
		$this->db->select('destinations.*, destination_images.title AS des_image_title, destination_images.thumb, destination_images.path');
		$this->db->join('destination_images', 'destination_images.destination_id = destinations.id', 'left');
		$this->db->order_by('destinations.id', 'ASC');
		$result = $this->db->get('destinations');
		return $result->result_array();
	}
	function get_destinations_by_id($destination_id)
	{
		$this->db->where('destinations.id', $destination_id);
		$result = $this->db->get('destinations');
		return $result->result();
	}
	function get_destination_by_id($destination_id)
	{
		$this->db->select('destinations.*, destination_images.id AS image_id, destination_images.thumb');
		$this->db->join('destination_images', 'destination_images.destination_id = destinations.id', 'left');
		$this->db->where('destinations.id', $destination_id);
		$result = $this->db->get('destinations');
		return $result->row();
	}
	function get_destination_by_id_join_country($destination_id)
	{
		$this->db->select('destinations.id AS destinatio_id, destinations.country_id, destinations.title, destinations.slug, destinations.description, destinations.author, destinations.status, destinations.date, countries.*');
		$this->db->join('countries', 'countries.id = destinations.country_id', 'inner');
		$this->db->where('destinations.id', $destination_id);
		$result = $this->db->get('destinations');
		return $result->row();
	}
	function get_destination_by_id_join_city($destination_id)
	{
		$this->db->select('destinations.id AS destinatio_id, destinations.country_id, destinations.title, destinations.slug, destinations.description, destinations.author, destinations.status, destinations.date, cities.*');
		$this->db->join('cities', 'cities.id = destinations.city_id', 'inner');
		$this->db->where('destinations.id', $destination_id);
		$result = $this->db->get('destinations');
		return $result->row();
	}
	function get_destination_by_city_id($city_id)
	{
		$this->db->where('city_id', $city_id);
		$result = $this->db->get('destinations');
		return $result->row();
	}
	function get_destination_by_country_id($country_id)
	{
		$this->db->where('country_id', $country_id);
		$result = $this->db->get('destinations');
		return $result->row();
	}
	function get_destination_image($destination_id)
	{
		$this->db->where('destination_id', $destination_id);
		$result = $this->db->get('destination_images');
		return $result->row();
	}
	function get_destination_images_by_destination_id($destination_id, $type)
	{
		$this->db->where('destination_id', $destination_id);
		$this->db->where('type', $type);
		$this->db->order_by('destination_images.id', 'DESC');
		$result = $this->db->get('destination_images');
		return $result->result();
	}
	function get_destination_image_by_last_id($last_id)
	{
		$this->db->where('id', $last_id);
		$result = $this->db->get('destination_images');
		return $result->row();
	}
	function get_destination_by_slug($slug)
	{
		$this->db->select('destinations.*, destination_images.title AS des_image_title, destination_images.thumb, destination_images.path');
		$this->db->where('slug', $slug);
		$this->db->join('destination_images', 'destination_images.destination_id = destinations.id', 'left');
		$result = $this->db->get('destinations');
		return $result->row();
	}
	function get_destinations_draft()
	{
		$this->db->select('destinations.id AS destination_id, destinations.select_id, destinations.type, destinations.title, destinations.slug, destinations.description, destinations.author, destinations.status, destinations.featured, destinations.date');
		$this->db->where('destinations.select_id', NULL);
		$result = $this->db->get('destinations');
		return $result->result_array();
	}
	function destination_flights_to($value, $to_id)
	{	
		$this->db->select('flights.id, flights.flight_route, flights.flight_price, airlines.airline_name, classes.aircraft_class_name, protections.protection_name, terminals.terminal_name AS flight_to, terminals.terminal_short_code AS flight_to_code');
		$this->db->join('terminals', 'terminals.id = flights.flight_to', 'inner');
		$this->db->join('airlines', 'airlines.id = flights.flight_airline', 'inner');
		$this->db->join('classes', 'classes.id = flights.flight_aircraft_class', 'inner');
		$this->db->join('protections', 'protections.id = flights.flight_protection', 'inner');
		$this->db->where('flights.id', $value);
		$this->db->where('flights.flight_to', $to_id);
		$result = $this->db->get('flights');
		return $result->row_array();
	}
	function destination_flights_from($from_id)
	{	
		$this->db->select('flights.id, flights.flight_route, flights.flight_price, airlines.airline_name, classes.aircraft_class_name, protections.protection_name, terminals.terminal_name AS flight_from, terminals.terminal_short_code AS flight_from_code');
		$this->db->join('terminals', 'terminals.id = flights.flight_from', 'inner');
		$this->db->join('airlines', 'airlines.id = flights.flight_airline', 'inner');
		$this->db->join('classes', 'classes.id = flights.flight_aircraft_class', 'inner');
		$this->db->join('protections', 'protections.id = flights.flight_protection', 'inner');
		$this->db->where('flights.id', $from_id);
		$result = $this->db->get('flights');
		return $result->row_array();
	}
		
	function get_flights_to_terminal_city($select_id)
	{
		$this->db->where('city_id', $select_id);
		$result = $this->db->get('terminals');
		return $result->result_array();
	}
	function get_flights_to_terminal_country($select_id)
	{
		$this->db->where('country_id', $select_id);
		$result = $this->db->get('terminals');
		return $result->result_array();
	}
	function get_flights_to_terminal_continent($select_id)
	{
		$this->db->select('terminals.*, countries.id AS country_id, continents.id AS continent_id');
		$this->db->join('countries', 'countries.id = terminals.country_id', 'inner');
		$this->db->join('continents', 'continents.id = countries.continent_id', 'inner');
		$this->db->where('continents.id', $select_id);
		$result = $this->db->get('terminals');
		return $result->result_array();
	}
	function get_flights_by_terminal_id($terminal_id)
	{
		$this->db->select('flights.id, flights.flight_to, flights.flight_from, flights.flight_route, flights.flight_price');
		$this->db->where('flight_to', $terminal_id);
		$this->db->order_by('flights.flight_price', 'ASC');
		$result = $this->db->get('flights');
		return $result->result_array();
	}
	
}

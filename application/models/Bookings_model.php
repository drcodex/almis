<?php
/**
 * Name:    Terminals Model
 * Author:  DrCodeX Technologies
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Bookings_model extends CI_Model
{
	public function __construct()
    {
		parent::__construct();
	}
	function add_booking($additional_data)
	{
		$this->db->insert('bookings',$additional_data);
	}
	function get_bookings()
	{
		$this->db->order_by('bookings.id', 'DESC');
		$result = $this->db->get('bookings');
		return $result->result();
	}
	function get_single_booking($id)
	{
		$this->db->where('bookings.id', $id);
		$result = $this->db->get('bookings');
		return $result->row();
	}
	function get_email_config($type)
	{
		$this->db->where('type', $type);
		$result = $this->db->get('email_config');
		return $result->row();
	}
}

<?php
/**
 * Name:    Posts Model
 * Author:  Ben Edmunds
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Flights_model extends CI_Model
{
	public function __construct()
    {
		parent::__construct();
	}
	function get_flight($id)
	{	
		$this->db->select('flights.id, flights.flight_route, flights.flight_price, airlines.id AS airline_id, airlines.airline_name, classes.id AS class_id, classes.aircraft_class_name, protections.id AS protection_id, protections.protection_name');
		$this->db->join('airlines', 'airlines.id = flights.flight_airline', 'inner');
		$this->db->join('classes', 'classes.id = flights.flight_aircraft_class', 'inner');
		$this->db->join('protections', 'protections.id = flights.flight_protection', 'inner');
		$this->db->where('airlines.status', 'Published');
		$this->db->where('flights.id', $id);
		$result = $this->db->get('flights');
		return $result->row_array();
	}
	function get_flight_to_by_continent($continent_id)
	{	
		$this->db->select('flights.id, flights.flight_route, flights.flight_price, airlines.airline_name, classes.aircraft_class_name, protections.protection_name, terminals.terminal_name AS flight_to, terminals.terminal_short_code AS flight_to_code');
		$this->db->join('airlines', 'airlines.id = flights.flight_airline', 'inner');
		$this->db->join('classes', 'classes.id = flights.flight_aircraft_class', 'inner');
		$this->db->join('protections', 'protections.id = flights.flight_protection', 'inner');
		$this->db->join('terminals', 'terminals.id = flights.flight_to', 'inner');
		$this->db->join('cities', 'cities.id = terminals.city_id');
		$this->db->join('countries', 'countries.id = cities.country_id');
		$this->db->join('continents', 'continents.id = countries.continent_id');
		$this->db->where('continents.id', $continent_id);
		$this->db->order_by('flights.flight_price', 'ASC');
		$result = $this->db->get('flights');
		return $result->result_array();
	}
	function flight_to($value, $continent_id)
	{	
		$this->db->select('flights.id, flights.flight_route, flights.flight_price, airlines.airline_name, classes.aircraft_class_name, protections.protection_name, terminals.terminal_name AS flight_to, terminals.terminal_short_code AS flight_to_code');
		$this->db->join('airlines', 'airlines.id = flights.flight_airline', 'inner');
		$this->db->join('classes', 'classes.id = flights.flight_aircraft_class', 'inner');
		$this->db->join('protections', 'protections.id = flights.flight_protection', 'inner');
		$this->db->join('terminals', 'terminals.id = flights.flight_to', 'inner');
		$this->db->join('cities', 'cities.id = terminals.city_id');
		$this->db->join('countries', 'countries.id = cities.country_id');
		$this->db->join('continents', 'continents.id = countries.continent_id');
		$this->db->where('flights.id', $value);
		$this->db->where('continents.id', $continent_id);
		$this->db->order_by('flights.flight_price', 'ASC');
		$result = $this->db->get('flights');
		return $result->row_array();
	}
	function get_flight_from_by_continent($continent_id)
	{	
		$this->db->select('flights.id, flights.flight_route, flights.flight_price, airlines.airline_name, classes.aircraft_class_name, protections.protection_name, terminals.terminal_name AS flight_from, terminals.terminal_short_code AS flight_from_code');
		$this->db->join('airlines', 'airlines.id = flights.flight_airline', 'inner');
		$this->db->join('classes', 'classes.id = flights.flight_aircraft_class', 'inner');
		$this->db->join('protections', 'protections.id = flights.flight_protection', 'inner');
		$this->db->join('terminals', 'terminals.id = flights.flight_from', 'inner');
		$this->db->join('cities', 'cities.id = terminals.city_id');
		$this->db->join('countries', 'countries.id = cities.country_id');
		$this->db->join('continents', 'continents.id = countries.continent_id');
		$this->db->where('continents.id', $continent_id);
		$this->db->order_by('flights.flight_price', 'ASC');
		$result = $this->db->get('flights');
		return $result->result_array();
	}
	function flight_from($value)
	{	
		$this->db->select('flights.id, flights.flight_route, flights.flight_price, airlines.airline_name, classes.aircraft_class_name, protections.protection_name, terminals.terminal_name AS flight_from, terminals.terminal_short_code AS flight_from_code');
		$this->db->join('airlines', 'airlines.id = flights.flight_airline', 'inner');
		$this->db->join('classes', 'classes.id = flights.flight_aircraft_class', 'inner');
		$this->db->join('protections', 'protections.id = flights.flight_protection', 'inner');
		$this->db->join('terminals', 'terminals.id = flights.flight_from', 'inner');
		$this->db->join('cities', 'cities.id = terminals.city_id');
		$this->db->join('countries', 'countries.id = cities.country_id');
		$this->db->join('continents', 'continents.id = countries.continent_id');
		$this->db->where('flights.id', $value);
		$this->db->order_by('flights.flight_price', 'ASC');
		$result = $this->db->get('flights');
		return $result->row_array();
	}
	function get_flight_by_id($flight_id)
	{
		$this->db->where('id', $flight_id);
		$result = $this->db->get('flights');
		return $result->row_array();
	}
	function get_flights_from_single($id)
	{	
		$this->db->select('terminals.id AS flight_from_id, terminals.terminal_name AS flight_from, terminals.terminal_short_code AS flight_from_code');
		$this->db->join('terminals', 'terminals.id = flights.flight_from', 'inner');
		$this->db->where('flights.id', $id);
		$result = $this->db->get('flights');
		return $result->row_array();
	}
	function get_flights_to_single($id)
	{	
		$this->db->select('terminals.id AS flight_to_id, terminals.terminal_name AS flight_to, terminals.terminal_short_code AS flight_to_code');
		$this->db->join('terminals', 'terminals.id = flights.flight_to', 'inner');
		$this->db->where('flights.id', $id);
		$result = $this->db->get('flights');
		return $result->row_array();
	}
	function get_flights()
	{	
		$this->db->select('flights.id, flights.flight_route, flights.flight_price, airlines.airline_name, classes.aircraft_class_name, protections.protection_name');
		$this->db->join('airlines', 'airlines.id = flights.flight_airline', 'inner');
		$this->db->join('classes', 'classes.id = flights.flight_aircraft_class', 'inner');
		$this->db->join('protections', 'protections.id = flights.flight_protection', 'inner');
		$this->db->where('airlines.status', 'Published');
		$this->db->order_by('flights.id', 'DESC');
		$result = $this->db->get('flights');
		return $result->result_array();
	}
	function get_flights_list()
	{	
		$this->db->select('flights.id, terminal_from.terminal_name AS terminal_from,terminal_from.terminal_short_code AS terminal_from_code, terminal_to.terminal_name AS terminal_to, terminal_to.terminal_short_code AS terminal_to_code,');
		$this->db->join('terminals terminal_from', 'terminal_from.id = flights.flight_from', 'left');
		$this->db->join('terminals terminal_to', 'terminal_to.id = flights.flight_to', 'left');
		$this->db->order_by('flights.id', 'DESC');
		$result = $this->db->get('flights');
		return $result->result_array();
	}
	function get_flights_by_location($table, $location, $name)
	{
		$this->db->select("flights.id, flights.flight_to, flights.flight_route, flights.flight_price, airlines.airline_name, classes.aircraft_class_name, protections.protection_name, terminals.city_id, cities.city_name, countries.id AS country_id, countries.country_name, `$table`.`id` AS {$location}_id, `$table`.{$location}_name, destinations.id AS destination_id, destinations.slug, destination_images.thumb");
		$this->db->join('airlines', 'airlines.id = flights.flight_airline', 'inner');
		$this->db->join('classes', 'classes.id = flights.flight_aircraft_class', 'inner');
		$this->db->join('protections', 'protections.id = flights.flight_protection', 'inner');
		$this->db->join('terminals', 'terminals.id = flights.flight_to');
		$this->db->join('cities', 'cities.id = terminals.city_id');
		$this->db->join('countries', 'countries.id = cities.country_id');
		$this->db->join('continents', 'continents.id = countries.continent_id');
		$this->db->join('destinations', 'destinations.type = "country" AND destinations.select_id = countries.id');
		$this->db->join('destination_images', 'destination_images.destination_id = destinations.id', 'left');
		$this->db->where("{$location}_name", $name);
		$this->db->order_by('flights.flight_price', 'ASC');
		$result = $this->db->get('flights');
		return $result->result_array();
	}
	function get_flights_from()
	{	
		$this->db->select('terminals.terminal_name AS flight_from, terminals.terminal_short_code AS flight_from_code');
		$this->db->join('terminals', 'terminals.id = flights.flight_from', 'inner');
		$this->db->order_by('flights.id', 'DESC');
		$result = $this->db->get('flights');
		return $result->result_array();
	}
	function get_flights_stopover()
	{	
		$this->db->select('terminals.terminal_name AS flight_stopover, terminals.terminal_short_code AS flight_stopover_code');
		$this->db->join('terminals', 'terminals.id = flights.flight_stopover', 'inner');
		$this->db->order_by('flights.id', 'DESC');
		$result = $this->db->get('flights');
		return $result->result_array();
	}
	function get_flights_to()
	{	
		$this->db->select('terminals.terminal_name AS flight_to, terminals.terminal_short_code AS flight_to_code');
		$this->db->join('terminals', 'terminals.id = flights.flight_to', 'inner');
		$this->db->order_by('flights.id', 'DESC');
		$result = $this->db->get('flights');
		return $result->result_array();
	}
	function get_flight_from_city_by_code($flight_from)
	{
		$this->db->select('terminals.*, cities.city_name');
		$this->db->join('cities', 'cities.id = terminals.city_id', 'inner');
		$this->db->where('terminal_short_code', $flight_from);
		$result = $this->db->get('terminals');
		return $result->row();
	}
	function get_flight_to_city_by_code($flight_to)
	{
		$this->db->select('terminals.*, cities.city_name');
		$this->db->join('cities', 'cities.id = terminals.city_id', 'inner');
		$this->db->where('terminal_short_code', $flight_to);
		$result = $this->db->get('terminals');
		return $result->row();
	}
	function add_flight($additional_data)
	{
		$result = $this->db->insert('flights',$additional_data);
		return $this->db->insert_id();
	}
	function add_term($data)
	{
		$result = $this->db->insert('terms',$data);
		return $this->db->insert_id();
	}
	function add_term_taxonomy($data)
	{
		$this->db->insert('term_taxonomy',$data);
	}
	function get_terms()
	{
		$this->db->join('term_taxonomy', 'term_taxonomy.term_id = terms.term_id', 'inner');
		$result = $this->db->get('terms');
		return $result->result();
	}
	function get_terms_by_taxonomy($taxonomy)
	{
		$this->db->join('term_taxonomy', 'term_taxonomy.term_id = terms.term_id', 'inner');
		$this->db->where('taxonomy', $taxonomy);
		$this->db->order_by('term_taxonomy_id', 'ASC');
		$result = $this->db->get('terms');
		return $result->result_array();
	}
	function get_taxonomy_with_term($taxonomy)
	{
		$this->db->join('terms', 'terms.term_id = term_taxonomy.term_id', 'inner');
		$this->db->where('taxonomy', $taxonomy);
		$this->db->order_by('term_taxonomy_id', 'ASC');
		$result = $this->db->get('term_taxonomy');
		return $result->result_array();
	}
	function get_terms_relationships()
	{
		$this->db->join('term_taxonomy', 'term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id', 'left');
		$this->db->join('terms', 'terms.term_id = term_taxonomy.term_id', 'left');
		$result = $this->db->get('term_relationships');
		return $result->result_array();
	}
	function get_terms_relations($id, $value)
	{
		$this->db->where('object_id', $id);
		$this->db->where('term_taxonomy_id', $value);
		$this->db->where('object_id', $id);
		$result = $this->db->get('term_relationships');
		return $result->row_array();
	}
	function get_term_by_taxonomy_id($term_id)
	{
		$this->db->join('term_taxonomy', 'term_taxonomy.term_id = terms.term_id');
		$this->db->where('term_taxonomy_id', $term_id);
		$result = $this->db->get('terms');
		return $result->row();
	}
	function delete_term($term_id)
	{
		$this->db->where('term_id', $term_id);
		$this->db->delete('terms');
	}
	function delete_term_taxonomy($term_id)
	{
		$this->db->where('term_taxonomy_id', $term_id);
		$this->db->delete('term_taxonomy');
	}
	function get_flight_category_last_id($last_id)
	{
		$this->db->where('term_id', $last_id);
		$result = $this->db->get('terms');
		return $result->row();
	}
	function destination_flights($from_id, $to_id)
	{	
		$this->db->select('flights.id, flights.flight_route, flights.flight_price, airlines.airline_name, classes.aircraft_class_name, protections.protection_name, airline_logos.thumb');
		$this->db->join('airlines', 'airlines.id = flights.flight_airline', 'inner');
		$this->db->join('airline_logos', 'airline_logos.airline_id = airlines.id', 'left');
		$this->db->join('classes', 'classes.id = flights.flight_aircraft_class', 'inner');
		$this->db->join('protections', 'protections.id = flights.flight_protection', 'inner');
		$this->db->where('airlines.status', 'Published');
		$this->db->where('flights.flight_from', $from_id);
		$this->db->where('flights.flight_to', $to_id);
		$this->db->order_by('flights.flight_price', 'ASC');
		$result = $this->db->get('flights');
		return $result->result_array();
	}
	function destination_flights_from($value, $from_id)
	{	
		$this->db->select('terminals.terminal_name AS flight_from, terminals.terminal_short_code AS flight_from_code');
		$this->db->join('terminals', 'terminals.id = flights.flight_from', 'inner');
		$this->db->where('flights.id', $value);
		$this->db->where('flights.flight_from', $from_id);
		$result = $this->db->get('flights');
		return $result->row_array();
	}
	function destination_flights_to($value, $to_id)
	{	
		$this->db->select('flights.id, flights.flight_route, flights.flight_price, airlines.airline_name, classes.aircraft_class_name, protections.protection_name, terminals.terminal_name AS flight_to, terminals.terminal_short_code AS flight_to_code');
		$this->db->join('terminals', 'terminals.id = flights.flight_to', 'inner');
		$this->db->join('airlines', 'airlines.id = flights.flight_airline', 'inner');
		$this->db->join('classes', 'classes.id = flights.flight_aircraft_class', 'inner');
		$this->db->join('protections', 'protections.id = flights.flight_protection', 'inner');
		$this->db->where('airlines.status', 'Published');
		$this->db->where('flights.id', $value);
		$this->db->where('flights.flight_to', $to_id);
		$result = $this->db->get('flights');
		return $result->row_array();
	}
	function update_flight($id, $additional_data)
	{
		$this->db->where('flights.id', $id);
		$result = $this->db->update('flights',$additional_data);
		return $result;
	}
	function delete_flight($id)
	{
		$this->db->where('flights.id', $id);
		$result = $this->db->delete('flights');
		return $result;
	}
	function delete_term_relationship_by_flight_id($id)
	{
		$this->db->where('object_id', $id);
		$result = $this->db->delete('term_relationships');
		return $result;
	}
	function get_single_flight($id)
	{	
		$this->db->select('flights.*, terminals.terminal_name AS flight_from');
		$this->db->join('terminals', 'terminals.id = flights.flight_to', 'inner');
		$this->db->join('airlines', 'airlines.id = flights.flight_airline', 'inner');
		$this->db->join('classes', 'classes.id = flights.flight_aircraft_class', 'inner');
		$this->db->join('protections', 'protections.id = flights.flight_protection', 'inner');
		$this->db->where('airlines.status', 'Published');
		$this->db->where('flights.id', $id);
		return $this->db->get()->row();
	}
	function add_aircraft_class($additional_data)
	{
		$result = $this->db->insert('classes',$additional_data);
		return $this->db->insert_id();
	}
	function get_aircraft_classes()
	{	
		$this->db->select('*');
		$this->db->from('classes');
		return $this->db->get()->result();
	}
	function get_single_aircraft_class($id)
	{
		$this->db->select('*');
		$this->db->from('classes c');
		$this->db->where('c.id', $id);
		return $this->db->get()->row();
	}
	function update_aircraft_class($id, $additional_data)
	{
		$this->db->where('classes.id', $id);
		$result = $this->db->update('classes',$additional_data);
		return $result;
	}
	function delete_aircraft_class($id)
	{
		$this->db->where('classes.id', $id);
		$result = $this->db->delete('classes');
		return $result;
	}
	function add_protection($additional_data)
	{
		$result = $this->db->insert('protections', $additional_data);
		return $this->db->insert_id();
	}
	function get_protections()
	{
		$this->db->select('*');
		$this->db->from('protections');
		return $this->db->get()->result();
	}
	function get_single_protection($id)
	{	
		$this->db->select('*');
		$this->db->from('protections p');
		$this->db->where('p.id', $id);
		return $this->db->get()->row();
	}
	function update_protection($id, $additional_data)
	{
		$this->db->where('protections.id', $id);
		$result = $this->db->update('protections',$additional_data);
		return $result;
	}
	function delete_protection($id)
	{
		$this->db->where('protections.id', $id);
		$result = $this->db->delete('protections');
		return $result;
	}
	function fetch_data($query)
	{
		$this->db->select('*');
		$this->db->from('terminals');
		$this->db->join('countries', 'countries.id = terminals.country_id', 'inner');
		if($query != '')
		{
			$this->db->like('terminal_name', $query);
			$this->db->or_like('terminal_short_code', $query);
		}
		return $this->db->get()->result();
	}
	function check_class_exist_in_flights($id)
	{
		$this->db->join('classes', 'classes.id = flights.flight_aircraft_class', 'inner');
		$this->db->where('classes.id', $id);
		$result = $this->db->get('flights');
		return $result->result();
	}
	function check_protection_exist_in_flights($id)
	{
		$this->db->join('protections', 'protections.id = flights.flight_protection', 'inner');
		$this->db->where('protections.id', $id);
		$result = $this->db->get('flights');
		return $result->result();
	}
	function check_if_term_id_exist($term_taxonomy_id)
	{
		$this->db->where('term_taxonomy_id', $term_taxonomy_id);
		$result = $this->db->get('term_relationships');
		return $result->row_array();
	}
	function check_if_term_id_not_exist($term_taxonomy_id)
	{
		$this->db->where('term_taxonomy_id !=', $term_taxonomy_id);
		$result = $this->db->get('term_relationships');
		return $result->result_array();
	}
	function check_if_term_id_exist_by_id($value, $id)
	{
		$this->db->where('term_taxonomy_id', $value);
		$this->db->where('object_id', $id);
		$result = $this->db->get('term_relationships');
		if ($result->num_rows() > 0){
			return $result->row_array();
		}
		else
		{
			return false;
		}
	}
	function check_if_term_id_not_exist_by_id($value, $id)
	{
		$this->db->where('term_taxonomy_id !=', $value);
		$this->db->where('object_id', $id);
		$result = $this->db->get('term_relationships');
		return $result->result_array();
	}
	function delete_term_relationships_id($object_id, $term_taxonomy_id)
	{
		$this->db->where_in('object_id', $object_id);
		$this->db->where_in('term_taxonomy_id', $term_taxonomy_id);
		$this->db->delete('term_relationships');
	}
	function delete_term_relationships($id)
	{
		$this->db->where_in('object_id', $id);
		$this->db->delete('term_relationships');
	}
	function delete_term_relationships_by_term_taxonomy_is($term_id)
	{
		$this->db->where_in('term_taxonomy_id', $term_id);
		$this->db->delete('term_relationships');
	}
}

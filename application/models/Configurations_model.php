<?php
/**
 * Name:    Terminals Model
 * Author:  DrCodeX Technologies
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Configurations_model extends CI_Model
{
	public function __construct()
    {
		parent::__construct();
	}
	function get_all_email_config()
	{
		$result = $this->db->get('email_config');
		return $result->result();
	}
	function get_email_config($type)
	{
		$this->db->where('type', $type);
		$result = $this->db->get('email_config');
		return $result->row();
	}
	function update_email_config($type, $data)
	{
		$this->db->where('type', $type);
		$res = $this->db->get('email_config');
		if ( $res->num_rows() > 0 ) 
		{
			$this->db->where('type', $type);
            $this->db->update('email_config', $data);
		}
	}
}

<?php
/**
 * Name:    Posts Model
 * Author:  Ben Edmunds
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Airlines_model extends CI_Model
{
	public function __construct()
    {
		parent::__construct();
	}
	public function index()
	{
		
	}
	function get_all_airlines()
	{
		$this->db->select('airlines.id AS airline_id, airline_logos.thumb, airlines.airline_name, airlines.airline_short_code, airlines.status');
		$this->db->join('airline_logos', 'airline_logos.airline_id = airlines.id', 'left');
		$this->db->order_by('airlines.airline_name', 'ASC');
		$result = $this->db->get('airlines');
		return $result->result();
	}
	function get_airlines()
	{
		$this->db->where('airlines.status', 'Published');
		$result = $this->db->get('airlines');
		return $result->result();
	}
	function add_airline($additional_data)
	{
		$result = $this->db->insert('airlines',$additional_data);
		return $this->db->insert_id();
	}
	function update_airline($id, $additional_data)
	{
		$this->db->where('airlines.id', $id);
		$result = $this->db->update('airlines',$additional_data);
		return $result;
	}
	function check_flight_exist_in_airline($id)
	{
		$this->db->join('flights', 'flights.flight_airline = airlines.id', 'left');
		$this->db->where('airlines.id', $id);
		$result = $this->db->get('airlines');
		return $result->result();
	}
	function delete_airline($id)
	{
		$this->db->where('airlines.id', $id);
		$result = $this->db->delete('airlines');
		return $result;
	}
	function delete_airline_logo($airline_id, $path, $thumb)
	{
		$imagepath = FCPATH.$path;
		$imagethumb = FCPATH .$thumb;
		$this->db->where('id', $airline_id);
		if(file_exists($imagepath))
		{
			unlink($path);
		}
		if(file_exists($imagethumb))
		{
			unlink($thumb);
		}
		$this->db->delete('airline_logos');
	}
	function get_single_airline($id)
	{	
		$this->db->select('*');
		$this->db->from('airlines');
		$this->db->where('airlines.id', $id);
		return $this->db->get()->row();
	}
	function get_airline_logo($id)
	{
		$this->db->where('airline_logos.airline_id', $id);
		$result = $this->db->get('airline_logos');
		return $result->row();
	}
	function get_airline_logo_by_airline_id($airline_id)
	{
		$this->db->where('airline_id', $airline_id);
		$result = $this->db->get('airline_logos');
		return $result->row();
	}
	function upload_airline_logo($airline_id, $data)
	{
		$this->db->where('airline_id', $airline_id);
		$this->db->where('airline_id is NOT NULL', NULL, FALSE);
		$res = $this->db->get('airline_logos');
		if ( $res->num_rows() > 0 ) 
		{
			//echo '<pre>'; print_r('update'); echo '</pre>'; die();
			$this->db->where('airline_id', $airline_id);
            $result = $this->db->update('airline_logos', $data);
			return $this->db->insert_id();
		} else {
			//echo '<pre>'; print_r('insert'); echo '</pre>'; die();
            $result = $this->db->insert('airline_logos', $data);
			return $this->db->insert_id();
		}
	}
	function update_featured_image_airline_id($featured_image_id, $other_data)
	{
		$check = $this->db->where('id', $featured_image_id);
		$this->db->update('airline_logos', $other_data);
	}
	function get_airline_image_by_last_id($last_id)
	{
		$this->db->where('id', $last_id);
		$result = $this->db->get('airline_logos');
		return $result->row();
	}
}

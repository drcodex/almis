<?php
/**
 * Name:    Terminals Model
 * Author:  DrCodeX Technologies
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Testimonials_model extends CI_Model
{
	public function __construct()
    {
		parent::__construct();
	}
	function get_testimonials()
	{
		$result = $this->db->get('testimonials');
		return $result->result();
	}
	function add_testimonial($data)
	{
		$result = $this->db->insert('testimonials', $data);
		return $this->db->insert_id();
	}
	function get_testimonial_by_id($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->get('testimonials');
		return $result->row();
	}
	function update_testimonial($id, $data)
	{
		$this->db->where('id', $id);
		$result = $this->db->update('testimonials',$data);
		return $result;
	}
	function delete_testimonial($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->delete('testimonials');
		return $result;
	}
	function get_single_terminal($terminal_id)
	{
		$this->db->join('cities', 'cities.id = terminals.city_id', 'inner');
		$this->db->join('countries', 'countries.id = terminals.country_id', 'inner');
		$this->db->where('terminals.id', $terminal_id);
		$result = $this->db->get('terminals');
		return $result->row();
	}
	function get_all_cities()
	{
		$this->db->join('cities', 'countries.id = cities.country_id');
		$result = $this->db->get('countries');
		return $result->result();
	}
	function check_terminal_exist_in_flights_from($terminal_id)
	{
		$this->db->join('terminals', 'terminals.id = flights.flight_from', 'inner');
		$this->db->where('terminals.id', $terminal_id);
		$this->db->order_by('flights.id', 'DESC');
		$result = $this->db->get('flights');
		return $result->result();
	}
	function check_terminal_exist_in_flights_to($terminal_id)
	{
		$this->db->join('terminals', 'terminals.id = flights.flight_to', 'inner');
		$this->db->where('terminals.id', $terminal_id);
		$this->db->order_by('flights.id', 'DESC');
		$result = $this->db->get('flights');
		return $result->result();
	}
}

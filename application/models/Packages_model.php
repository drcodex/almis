<?php
/**
 * Name:    Terminals Model
 * Author:  DrCodeX Technologies
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Packages_model extends CI_Model
{
	public function __construct()
    {
		parent::__construct();
	}
	function add_package($additional_data)
	{
		$this->db->insert('packages',$additional_data);
		return $this->db->insert_id();
	}
	function add_package_prices($newArray)
	{
		$this->db->insert_batch('package_prices', $newArray); 
	}
	function add_description($data)
	{
		$result = $this->db->insert('package_meta', $data);
		return $this->db->insert_id();
	}
	function add_itinerary($data)
	{
		$this->db->insert_batch('itinerary', $data);
		return $this->db->insert_id();
	}
	function add_location($data)
	{
		$result = $this->db->insert('package_meta', $data);
		return $this->db->insert_id();
	}
	function add_feature($data)
	{
		$this->db->insert_batch('package_meta', $data);
		return $this->db->insert_id();
	}
	function add_amenity($data)
	{
		$this->db->insert_batch('package_meta', $data);
		return $this->db->insert_id();
	}
	function get_all_cities()
	{
		$this->db->join('cities', 'countries.id = cities.country_id');
		$result = $this->db->get('countries');
		return $result->result();
	}
	function get_packages()
	{
		$this->db->order_by('packages.id', 'DESC');
		$result = $this->db->get('packages');
		return $result->result();
	}
	function get_packages_by_type($type)
	{
		$this->db->where('type', $type);
		$this->db->order_by('packages.id', 'DESC');
		$result = $this->db->get('packages');
		return $result->result_array();
	}
	function get_packages_by_type_status($type, $status)
	{
		$this->db->where('type', $type);
		$this->db->where('status', $status);
		$this->db->order_by('packages.id', 'DESC');
		$result = $this->db->get('packages');
		return $result->result_array();
	}
	function get_package_by_id($id, $type)
	{
		$this->db->where('type', $type);
		$this->db->where('id', $id);
		$this->db->order_by('packages.id', 'DESC');
		$result = $this->db->get('packages');
		return $result->row();
	}
	function get_package_by_id_status($id, $type, $status)
	{
		$this->db->where('type', $type);
		$this->db->where('id', $id);
		$this->db->where('status', $status);
		$this->db->order_by('packages.id', 'DESC');
		$result = $this->db->get('packages');
		return $result->row();
	}
	function get_package_meta_by_id($id)
	{
		$this->db->where('package_id', $id);
		$result = $this->db->get('package_meta');
		foreach($result->result() as $row) {
			$options[$row->meta_key] = $row->meta_value;
		}
		if(!empty($options)){
			return $options;
		}
	}
	function get_package_meta_last_id($last_id)
	{
		$this->db->where('pmeta_id', $last_id);
		$result = $this->db->get('package_meta');
		return $result->row();
	}
	function get_package_meta_by_key_result($package_id, $key)
	{
		$this->db->where('package_id', $package_id);
		$this->db->where('meta_key', $key);
		$this->db->order_by('package_meta.pmeta_id', 'DESC');
		$result = $this->db->get('package_meta');
		return $result->result();
	}
	function get_package_meta_by_key_row($package_id, $key)
	{
		$this->db->where('package_id', $package_id);
		$this->db->where('meta_key', $key);
		$this->db->order_by('package_meta.pmeta_id', 'DESC');
		$result = $this->db->get('package_meta');
		return $result->row();
	}
	function get_package_featured_image_by_id($id, $image_type)
	{
		$this->db->where('package_id', $id);
		$this->db->where('type', $image_type);
		$result = $this->db->get('package_images');
		return $result->row();
	}
	function get_package_featured_images($id)
	{
		$this->db->order_by('package_images.id', 'DESC');
		$this->db->where('type', 'featured');
		$this->db->where('package_id', $id);
		$result = $this->db->get('package_images');
		return $result->row();
	}
	function get_package_images_by_package_id($id, $type)
	{
		$this->db->where('package_id', $id);
		$this->db->where('type', $type);
		$this->db->order_by('package_images.id', 'DESC');
		$result = $this->db->get('package_images');
		return $result->result();
	}
	function get_package_image_by_last_id($last_id)
	{
		$this->db->where('id', $last_id);
		$result = $this->db->get('package_images');
		return $result->row();
	}
	function get_package_gallery_images_by_id($id, $image_type)
	{
		$this->db->where('package_id', $id);
		$this->db->where('type', $image_type);
		$result = $this->db->get('package_images');
		return $result->result();
	}
	function get_package_gallery_image_by_id($image_id, $type)
	{
		$this->db->where('id', $image_id);
		$this->db->where('type', $type);
		$result = $this->db->get('package_images');
		return $result->row();
	}
	function get_package_prices_id($id)
	{
		$this->db->select('package_prices.*, cities.id AS city_id, cities.city_name');
		$this->db->where('package_id', $id);
		$this->db->join('cities', 'cities.id = package_prices.departure');
		$result = $this->db->get('package_prices');
		return $result->result_array();
	}
	
	function get_amenities($package_id, $key)
	{
		$this->db->where('package_id', $package_id);
		$this->db->where('meta_key', $key);
		$this->db->order_by('package_meta.pmeta_id', 'DESC');
		$result = $this->db->get('package_meta');
		return $result->result();
	}
	function get_itinerary($package_id, $type)
	{
		$this->db->where('package_id', $package_id);
		$this->db->where('type', $type);
		$this->db->order_by('itinerary.day', 'ASC');
		$result = $this->db->get('itinerary');
		return $result->result();
	}
	function get_last_feature($last_id)
	{
		$this->db->where('pmeta_id', $last_id);
		$result = $this->db->get('package_meta');
		return $result->row();
	}
	function update_description($description_id, $data)
	{
		$this->db->where('pmeta_id', $description_id);
		$result = $this->db->update('package_meta', $data);
		return $this->db->insert_id();
	}
	function update_location($location_id, $data)
	{
		$this->db->where('pmeta_id', $location_id);
		$result = $this->db->update('package_meta', $data);
		return $this->db->insert_id();
	}
	function update_package($additional_data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update('packages',$additional_data);
	}
	function update_featured_image_package_id($featured_image_id, $other_data)
	{
		$check = $this->db->where('id', $featured_image_id);
		$this->db->update('package_images', $other_data);
	}
	function upload_package_images($package_id, $data)
	{
		$this->db->where('package_id', $package_id);
		$this->db->where('package_id is NOT NULL', NULL, FALSE);
		$res = $this->db->get('package_images');
		$result = $this->db->insert('package_images', $data);
		return $this->db->insert_id();
	}
	function upload_package_featured_image($package_id, $data)
	{
		$this->db->where('package_id', $package_id);
		$this->db->where('package_id is NOT NULL', NULL, FALSE);
		$this->db->where('type', 'featured');
		$res = $this->db->get('package_images');

		if ( $res->num_rows() > 0 ) 
		{
			$this->db->where('package_id', $package_id);
			$this->db->where('type', 'featured');
            $result = $this->db->update('package_images', $data);
			return $this->db->insert_id();
		} else {
			$this->db->where('type', 'featured');
            $result = $this->db->insert('package_images', $data);
			return $this->db->insert_id();
		}
	}
	function upload_package_featured_image_create($data)
	{
		$this->db->get('package_images');
		$this->db->insert('package_images', $data);
		return $this->db->insert_id();
	}
	function delete_amenity($amenity_id)
	{
		$this->db->where('pmeta_id', $amenity_id);
		$this->db->delete('package_meta');
	}
	function delete_itinerary($itinerary_id)
	{
		$this->db->where('id', $itinerary_id);
		$this->db->delete('itinerary');
	}
	function delete_feature($feature_id)
	{
		$this->db->where('pmeta_id', $feature_id);
		$this->db->delete('package_meta');
	}
	function delete_featured_image($image_id, $path, $thumb)
	{
		$imagepath = FCPATH.$path;
		$imagethumb = FCPATH .$thumb;
		$this->db->where('package_id', $image_id);
		if(file_exists($imagepath))
		{
			unlink($path);
		}
		if(file_exists($imagethumb))
		{
			unlink($thumb);
		}
		$this->db->delete('package_images');
	}
	function delete_gallery_image($image_id, $path, $thumb)
	{
		$imagepath = FCPATH.$path;
		$imagethumb = FCPATH .$thumb;
		$this->db->where('id', $image_id);
		if(file_exists($imagepath))
		{
			unlink($path);
		}
		if(file_exists($imagethumb))
		{
			unlink($thumb);
		}
		$this->db->delete('package_images');
	}
	function delete_package_prices_by_id($price_id)
	{
		$this->db->where_in('id', $price_id);
		$this->db->delete('package_prices');
	}
	function row_count_package_description($package_id)
	{
		$this->db->where('package_id', $package_id);
		$this->db->where('meta_key', 'description');
		$result = $this->db->get('package_meta');
		return $result->num_rows();
	}
	function row_count_package_features($package_id)
	{
		$this->db->where('package_id', $package_id);
		$this->db->where('meta_key', 'feature');
		$result = $this->db->get('package_meta');
		return $result->num_rows();
	}
	function get_packages_all($status)
	{
		$this->db->where('status', $status);
		$this->db->order_by('packages.id', 'DESC');
		$result = $this->db->get('packages');
		return $result->result_array();
	}
	function get_package_by_id_no_type($id)
	{
		$this->db->where('id', $id);
		$this->db->order_by('packages.id', 'DESC');
		$result = $this->db->get('packages');
		return $result->row();
	}
	function check_if_price_id_exist_by_id($value)
	{
		$this->db->where('id', $value);
		$result = $this->db->get('package_prices');
		if ($result->num_rows() > 0){
			return $result->row_array();
		}
		else
		{
			return false;
		}
	}
	function check_if_price_id_not_exist_by_id($value)
	{
		$this->db->where('id !=', $value);
		$result = $this->db->get('package_prices');
		return $result->result_array();
	}
}

<?php
/**
 * Name:    Terminals Model
 * Author:  DrCodeX Technologies
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Appearance_model extends CI_Model
{
	function check_if_destination_id_exist($destination_id)
	{
		$this->db->where('relation_value', $destination_id);
		$result = $this->db->get('relations');
		return $result->row_array();
	}
	function check_if_destination_id_not_exist($destination_id)
	{
		$this->db->where('relation_value !=', $destination_id);
		$result = $this->db->get('relations');
		return $result->result_array();
	}
	function delete_relation_id($relation_id)
	{
		$this->db->where_in('id', $relation_id);
		$this->db->delete('relations');
	}
	function get_seo_by_page($page)
	{
		$this->db->where('page', $page);
		$result = $this->db->get('seo');
		return $result->row_array();
	}
	function add_update_seo($page, $data)
	{
		$this->db->where('page', $page);
		$res = $this->db->get('seo');

		if ( $res->num_rows() > 0 ) 
		{
			$this->db->where('page', $page);
            $result = $this->db->update('seo', $data);
			return $this->db->insert_id();
		} else {
            $result = $this->db->insert('seo', $data);
			return $this->db->insert_id();
		}
	}
	function get_terms_by_taxonomy($taxonomy)
	{
		$this->db->join('term_taxonomy', 'term_taxonomy.term_id = terms.term_id', 'inner');
		$this->db->where('taxonomy', $taxonomy);
		$result = $this->db->get('terms');
		return $result->result_array();
	}
	function add_block($data)
	{
		$result = $this->db->insert('blocks', $data);
		return $this->db->insert_id();
	}
	function get_block_by_last_id($data)
	{
		$this->db->where('id', $data);
		$result = $this->db->get('blocks');
		return $result->row();
	}
	function get_block_items($position)
	{
		$this->db->where('position', $position);
		$result = $this->db->get('blocks');
		return $result->result();
	}
	function delete_block_item($block_id)
	{
		$this->db->where_in('id', $block_id);
		$this->db->delete('blocks');
	}
}

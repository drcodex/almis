<?php
/**
 * Name:    Terminals Model
 * Author:  DrCodeX Technologies
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Terminals_model extends CI_Model
{
	public function __construct()
    {
		parent::__construct();
	}
	function get_terminals()
	{
		$this->db->select('terminals.id AS terminal_id, countries.id AS country_id, cities.id AS city_id, terminals.terminal_name, terminals.terminal_short_code, cities.city_name, countries.country_name');
		$this->db->join('cities', 'cities.id = terminals.city_id', 'inner');
		$this->db->join('countries', 'countries.id = terminals.country_id', 'inner');
		$result = $this->db->get('terminals');
		return $result->result();
	}
	function add_terminal($additional_data)
	{
		$result = $this->db->insert('terminals',$additional_data);
		return $this->db->insert_id();
	}
	function update_terminal($terminal_id, $additional_data)
	{
		$this->db->where('terminals.id', $terminal_id);
		$result = $this->db->update('terminals',$additional_data);
		return $result;
	}
	function delete_terminal($terminal_id)
	{
		$this->db->where('terminals.id', $terminal_id);
		$result = $this->db->delete('terminals');
		return $result;
	}
	function get_single_terminal($terminal_id)
	{
		$this->db->join('cities', 'cities.id = terminals.city_id', 'inner');
		$this->db->join('countries', 'countries.id = terminals.country_id', 'inner');
		$this->db->where('terminals.id', $terminal_id);
		$result = $this->db->get('terminals');
		return $result->row();
	}
	function get_all_cities()
	{
		$this->db->join('cities', 'countries.id = cities.country_id');
		$result = $this->db->get('countries');
		return $result->result();
	}
	function check_terminal_exist_in_flights_from($terminal_id)
	{
		$this->db->join('terminals', 'terminals.id = flights.flight_from', 'inner');
		$this->db->where('terminals.id', $terminal_id);
		$this->db->order_by('flights.id', 'DESC');
		$result = $this->db->get('flights');
		return $result->result();
	}
	function check_terminal_exist_in_flights_to($terminal_id)
	{
		$this->db->join('terminals', 'terminals.id = flights.flight_to', 'inner');
		$this->db->where('terminals.id', $terminal_id);
		$this->db->order_by('flights.id', 'DESC');
		$result = $this->db->get('flights');
		return $result->result();
	}
}

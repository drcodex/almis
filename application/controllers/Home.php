<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Frontend_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('home_model');
		$this->load->model('configurations_model');
		$this->load->model('destinations_model');
		$this->load->model('flights_model');
		$this->load->model('seo_model');
		$this->load->model('packages_model');
		$this->load->model('testimonials_model');

		$this->lang->load('auth');
	}
	public function index()
	{
		$seo = $this->seo_model->get_seo_by_page($page = 'home');
		$e = array(
			'general' => true, //description
			'og' => true,
			'twitter'=> true,
			'robot'=> true
		);
		meta_tags(
			$e, $title = $seo['title'],
			$desc = $seo['description'],
			$imgurl ='',
			$url = current_url()
		);
		if($_POST)
		{
			$flight_from = $this->input->post('flight_from');
			$flight_to = $this->input->post('flight_to');
			$flight_by_from_name = $this->home_model->get_flight_by_from_name($flight_from);
			$flight_by_to_name = $this->home_model->get_flight_by_to_name($flight_to);
			if(count($flight_by_from_name) == 0 || count($flight_by_to_name) == 0)
			{
				redirect('/');
			}
			else
			{
				$from_short_code = $flight_by_from_name->terminal_short_code;
				$to_short_code = $flight_by_to_name->terminal_short_code;
				redirect("flights/route/".$from_short_code."/".$to_short_code);
			}
		}
		$this->data['flight_offers'] = $this->unique_block($taxonomy = "flights_category", $position = "home_flight_offers");
		//echo '<pre>'; print_r($this->data['flight_offers']); echo '</pre>'; die();
		$this->data['lowest_fares'] = $this->unique_block($taxonomy = "flights_category", $position = "home_lowest_fares");
		$relations = $this->home_model->get_relations_by_type($action_type = 'home');
		if(!empty($relations)){
			foreach ($relations as $relation) {
				$relation_id = $relation['relation_value'];
				$destinations = (array)$this->destinations_model->get_destination_by_id($relation_id);
				$fromArr['id'] = $destinations['id'];
				$fromArr['select_id'] = $destinations['select_id'];
				$fromArr['type'] = $destinations['type'];
				$fromArr['title'] = $destinations['title'];
				$fromArr['slug'] = $destinations['slug'];
				$fromArr['thumb'] = $destinations['thumb'];
				$from_array[] = $fromArr;
			}
		}
		if(!empty($from_array)){
			foreach ($from_array as $destination) {
				$des_type = $destination['type'];
				$select_id = $destination['select_id'];
				if($des_type){
					$type = $des_type .'_name';
					$table = $this->pluralize(2, $des_type);
					$list = $this->destinations_model->get_destinations_location($type, $table, $select_id);
				}
				$list_array[] = $list;
			}
			$destinations_list = array_replace_recursive($from_array, $list_array);
			$this->data['destinations'] = $destinations_list;
		}
		$type = 'hajj';
		$image_type = 'featured';
		$status = 'Published';
		$packages = $this->packages_model->get_packages_by_type_status($type, $status);
		if(!empty($packages)){
			$package_id = array();
			for ($i = 0; $i < count($packages); $i++) {
				$package_id[] = $packages[$i]['id'];
			}
			$new_array = [];
			if(!empty($package_id)){
				foreach ($package_id as $id) {
					$package = (array)$this->packages_model->get_package_by_id($id, $type);
					$image = (array)$this->packages_model->get_package_featured_image_by_id($id, $image_type);
					$tempArr['id'] = !empty($package['id']) ? $package['id'] : NULL;
					$tempArr['type'] = !empty($package['type']) ? $package['type'] : NULL;
					$tempArr['title'] = !empty($package['title']) ? $package['title'] : NULL;
					$tempArr['sub_title'] = !empty($package['sub_title']) ? $package['sub_title'] : NULL;
					$tempArr['price'] = !empty($package['price']) ? $package['price'] : NULL;
					$tempArr['sale_price'] = !empty($package['sale_price']) ? $package['sale_price'] : NULL;
					$tempArr['no_of_days'] = !empty($package['no_of_days']) ? $package['no_of_days'] : NULL;
					$tempArr['departure_date'] = !empty($package['departure_date']) ? $package['departure_date'] : NULL;
					$tempArr['return_date'] = !empty($package['return_date']) ? $package['return_date'] : NULL;
					$tempArr['status'] = !empty($package['status']) ? $package['status'] : NULL;
					$tempArr['date'] = !empty($package['date']) ? $package['date'] : NULL;
					$tempArr['path'] = !empty($image['path']) ? $image['path'] : NULL;
					$package_array[] = (object)$tempArr;
				}
			}
			$this->data['packages'] = $package_array;
		}
		$this->data['testimonials'] = $this->testimonials_model->get_testimonials();
		$this->data['description'] = $seo['description'];
		$this->data['title'] = $seo['title'];
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->_template_home('front' . DIRECTORY_SEPARATOR . 'home', $this->data);
	}
	public function search()
	{
		if($_POST)
		{
			$flight_from = $this->input->post('flight_from');
			$flight_to = $this->input->post('flight_to');
			$flight_by_from_name = $this->home_model->get_flight_by_from_name($flight_from);
			$flight_by_to_name = $this->home_model->get_flight_by_to_name($flight_to);
			if(count($flight_by_from_name) == 0 || count($flight_by_to_name) == 0)
			{
				redirect('/');
			}
			else
			{
				$from_short_code = $flight_by_from_name->terminal_short_code;
				$to_short_code = $flight_by_to_name->terminal_short_code;
				redirect("flights/route/".$from_short_code."/".$to_short_code);
			}
		}
		else
		{
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('front' . DIRECTORY_SEPARATOR . 'home', $this->data);
		}
	}
	public function about_us()
	{
		$e = array(
			'general' => true, //description
			'og' => true,
			'twitter'=> true,
			'robot'=> true
		);
		meta_tags(
			$e, $title = '',
			$desc = '',
			$imgurl ='',
			$url = ''
		);
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->_render_page('front' . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR . 'about_us', $this->data);
	}
	public function contact_us()
	{
		$e = array(
			'general' => true, //description
			'og' => true,
			'twitter'=> true,
			'robot'=> true
		);
		meta_tags(
			$e, $title = '',
			$desc = '',
			$imgurl ='',
			$url = ''
		);
		if($_POST)
		{
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$mobile_number = $this->input->post('mobile_number');
			$contact_email = $this->input->post('contact_email');
			$contact_message = $this->input->post('contact_message');
		
			// validate form input
			$this->form_validation->set_rules('first_name','First Name','trim|required');
			$this->form_validation->set_rules('last_name','Last Name','trim|required');
			$this->form_validation->set_rules('mobile_number','Mobile Number','trim|required');
			$this->form_validation->set_rules('contact_email','Email','trim|valid_email');
			$this->form_validation->set_rules('contact_message','Message','trim');

			if ($this->form_validation->run() === TRUE)
			{
				$type = $this->uri->segment(1);
				$email_type = $this->configurations_model->get_email_config($type);
				//echo '<pre>'; print_r($email_type); echo '</pre>'; die();
				$sender_email = $email_type->sender_email;
				$sender_name = $email_type->sender_name;
				$sender_subject = $email_type->sender_subject;
				$receiver_subject = $email_type->receiver_subject;
				$message_admin = '<p style="font-size: 16px;">Hi'.
				'<p style="font-size: 16px;">You have received an inquery.</p>'.
				'<p style="font-size: 16px;">Your inquiry details are as following:</p>'.
				'<table style="font-size: 16px;">
					<thead>
						<tr>
							<th style="text-align: left;">Particulars</th>
							<th style="text-align: left;">Entires</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">First Name</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$first_name.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Last Name</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$last_name.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Phone</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$mobile_number.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Email</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$contact_email.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Message</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$contact_message.'</td>
						</tr>
					</tbody>
				</table>';
				$this->send_mail_to_admin($sender_email,$sender_name,$sender_subject,$message_admin);
			}
		}
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->_render_page('front' . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR . 'contact_us', $this->data);
	}
	
    public function send_mail_to_admin($sender_email,$sender_name,$sender_subject,$message_admin){
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->from($sender_email, $sender_name);
        $this->email->to($sender_email);
        $this->email->subject($sender_subject);
        $this->email->message($message_admin);
        $this->email->send();
    }
	public static function pluralize($quantity, $singular, $plural=null)
	{
		if($quantity==1 || !strlen($singular)) return $singular;
		if($plural!==null) return $plural;

		$last_letter = strtolower($singular[strlen($singular)-1]);
		switch($last_letter) {
			case 'y':
				return substr($singular,0,-1).'ies';
			case 's':
				return $singular.'es';
			default:
				return $singular.'s';
		}
	}
	
	public function unique_block($taxonomy, $position) {
		$term_taxonomy = $this->home_model->get_term_taxonomy_by_block($taxonomy, $position);

		$term_taxonomy_id = array();
		for ($i = 0; $i < count($term_taxonomy); $i++) {
			$term_taxonomy_id[] = $term_taxonomy[$i]['term_taxonomy_id'];
		}
		$blockArr = [];
		// Loop term taxonomy te get value by iterating term_taxonomy_id one by one
		foreach ($term_taxonomy_id as $block) {
			$itemArr = [];

			// Get single taxonomy by id ($block) join block with taxonomy. Required for Block title. 
			$taxonomy_block = $this->home_model->get_term_taxonomy_with_block($block, $taxonomy, $position);
			
			// Get all the objects in the specific taxonomy 
			$term_relationships = $this->home_model->get_term_relationships($block);

			// Loop each relation to extract object_id of each flight
			foreach ($term_relationships as $item) {
				$object_id = $item['object_id'];
				$flights_from_in_terms = $this->home_model->get_flights_from_in_terms($object_id, $block);
				$flights_to_in_terms = $this->home_model->get_flights_to_in_terms($object_id, $block);
				$items = array_replace_recursive($flights_from_in_terms, $flights_to_in_terms);
				$itemArr['0'] = $taxonomy_block['title'];
				$itemArr[] = $items;
			}
			if(!empty($itemArr))
			{
				$blockArr[] = $itemArr;
			}
		}
		return $blockArr;
	}
	public function unique_multidim_array($array, $key) {
		$temp_array = array(); 
		$i = 0; 
		$key_array = array(); 
		
		foreach($array as $val) { 
			if (!in_array($val[$key], $key_array)) { 
				$key_array[$i] = $val[$key]; 
				$temp_array[$i] = $val; 
			} 
			$i++; 
		} 
		return $temp_array; 
	}
}

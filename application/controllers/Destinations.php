<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Destinations extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('destinations_model');
		$this->load->model('seo_model');

		$this->lang->load('auth');
	}
	public function index()
	{
		$e = array(
			'general' => true, //description
			'og' => true,
			'twitter'=> true,
			'robot'=> true
		);
		meta_tags(
			$e, $title = '',
			$desc = '',
			$imgurl ='',
			$url = ''
		);
		$destinations = $this->destinations_model->get_destinations_featured();
		if(!empty($destinations)){
			foreach ($destinations as $destination) {
				$des_type = $destination['type'];
				$select_id = $destination['select_id'];
				if($type = $des_type){
					$type = $type .'_name';
					$table = $this->pluralize(2, $des_type);
					$list = $this->destinations_model->get_destinations_location($type, $table, $select_id);
				}
				$list_array[] = $list;
			}
			$destinations_list = array_replace_recursive($destinations, $list_array);
			$this->data['destinations'] = $destinations_list;
		}
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->_render_page('front' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'index', $this->data);
	}
	public function route($slug = NULL)
	{
		$destination = $this->destinations_model->get_destination_by_slug($slug);
		$destination_id = $destination->id;
		$seo = $this->seo_model->get_seo_by_page_id($page = 'destination', $destination_id);
		if(!empty($destination))
		{
    		$type = $destination->type;
    		$select_id = $destination->select_id;
			if(!empty($select_id) && $type == 'city') {
				$terminal = $this->destinations_model->get_flights_to_terminal_city($select_id);
				$this->data['input_flight_to'] = $terminal['0']['terminal_name'];
				//echo '<pre>'; print_r($this->data['input_flight_to']); echo '</pre>'; die();
    		} elseif (!empty($select_id) && $type == 'country') {
				$terminal = $this->destinations_model->get_flights_to_terminal_country($select_id);
			} elseif(!empty($select_id) && $type == 'continent') {
				$terminal = $this->destinations_model->get_flights_to_terminal_continent($select_id);
			}
			if(isset($terminal)) {
				$terminal_loop = array();
				for ($i = 0; $i < count($terminal); $i++) {
					$terminal_loop[] = $terminal[$i]['id'];
				}
				$terminal_array = [];
				if(!empty($terminal_loop)){
					$a = 0; 
					foreach ($terminal_loop as $terminal_value)
					{
						if (!empty($terminal_value))
						{
							$flights = $this->destinations_model->get_flights_by_terminal_id($terminal_value);
							if (!empty($flights))
							{
								$flight_id = array();
								for ($i = 0; $i < count($flights); $i++) {
									$flight_id[] = $flights[$i]['id'];
								}
								$new_array = [];
								if(!empty($flight_id)){
									foreach ($flight_id as $value) {
										$get_flights_to = $this->destinations_model->destination_flights_to($value, $terminal_value);
										$tempArr['flight_to'] = !empty($get_flights_to['flight_to']) ? $get_flights_to['flight_to'] : NULL;
										$tempArr['flight_to_code'] = !empty($get_flights_to['flight_to_code']) ? $get_flights_to['flight_to_code'] : NULL;
										$new_array[] = $tempArr;
									}
								}
								$all_destinations = array_replace_recursive($flights, $new_array);
								if ($all_destinations){
									$destination_id = array();
									for ($i = 0; $i < count($all_destinations); $i++) {
										$destination_id[] = $all_destinations[$i]['id'];
									}
								} 
								$from_array = [];
								if(!empty($destination_id))
								{
									foreach ($destination_id as $from_id) {
										$from = $this->destinations_model->destination_flights_from($from_id);
										if(isset($from['id']))
										{
											$fromArr['flight_from'] = $from['flight_from'];
											$fromArr['flight_from_code'] = $from['flight_from_code'];
											$fromArr['airline_name'] = $from['airline_name'];
											$fromArr['aircraft_class_name'] = $from['aircraft_class_name'];
											$fromArr['protection_name'] = $from['protection_name'];
											$from_array[] = $fromArr;
										}
									}
								}
								$target = array_replace_recursive($all_destinations, $from_array);
								$target_destinations = $this->unique_multidim_array($target,'flight_from');
								$targetArr[$a] = $target_destinations;
								$a++;
							}
						}
					}
					if(!empty($target_destinations)){
							$terminal_array = $targetArr;
					}
				}
			}
			if(!empty($terminal_array)){
				$this->data['target_destinations'] = $terminal_array;
			}
			$destination = $this->destinations_model->get_destination_by_slug($slug);
			$this->data['destination'] = $destination;
			
			if(!empty($destination->thumb)) {
				$seoimage = base_url().$destination->thumb;
			} else {
				$seoimage = base_url().'assets/images/airline-default.jpg';
			}
			$e = array(
				'general' => true, //description
				'og' => true,
				'twitter'=> true,
				'robot'=> true
			);
			meta_tags(
				$e, $title = $seo['title'],
				$desc = $seo['description'],
				$imgurl = $seoimage,
				$url = current_url()
			);
			$this->data['description'] = $title;
			$this->data['title'] = $desc;
    		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
    		$this->_render_page('front' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'single-destination', $this->data);
		}
		else
		{
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('errors' . DIRECTORY_SEPARATOR . 'front' . DIRECTORY_SEPARATOR . '404', $this->data);
		}
	}
	
	public static function pluralize($quantity, $singular, $plural=null)
	{
		if($quantity==1 || !strlen($singular)) return $singular;
		if($plural!==null) return $plural;

		$last_letter = strtolower($singular[strlen($singular)-1]);
		switch($last_letter) {
			case 'y':
				return substr($singular,0,-1).'ies';
			case 's':
				return $singular.'es';
			default:
				return $singular.'s';
		}
	}
	public function unique_multidim_array($array, $key) {
		$temp_array = array(); 
		$i = 0; 
		$key_array = array(); 
		
		foreach($array as $val) { 
			if (!in_array($val[$key], $key_array)) { 
				$key_array[$i] = $val[$key]; 
				$temp_array[$i] = $val; 
			} 
			$i++; 
		} 
		return $temp_array; 
	}
}

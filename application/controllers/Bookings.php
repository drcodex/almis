<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Bookings extends Frontend_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('bookings_model');
		$this->load->model('settings_model');

		$this->lang->load('auth');
	}
	public function index()
	{
		if($_POST)
		{
			$full_name = $this->input->post('full_name');
			$phone = $this->input->post('phone');
			$email = $this->input->post('email');
			$flight_from = $this->input->post('flight_from');
			$flight_to = $this->input->post('flight_to');
			$takeoff_time = $this->input->post('takeoff_time');
			$landing_time = $this->input->post('landing_time');
			$adults = $this->input->post('adults');
			$kids = $this->input->post('kids');
			$infants = $this->input->post('infants');
			$type = $this->input->post('type');
			$class = $this->input->post('class');
		
			// validate form input
			$this->form_validation->set_rules('full_name','Full Name','trim|required');
			$this->form_validation->set_rules('phone','Phone','trim');
			$this->form_validation->set_rules('email','Email','trim|required|valid_email');
			$this->form_validation->set_rules('flight_from','Flight From','trim|required');
			$this->form_validation->set_rules('flight_to','Flight To','trim|required');
			$this->form_validation->set_rules('takeoff_time','Departure','trim|required');
			$this->form_validation->set_rules('landing_time','Arrival','trim|required');
			$this->form_validation->set_rules('adults','Adults','trim|required');
			$this->form_validation->set_rules('kids','Kids','trim');
			$this->form_validation->set_rules('infants','Infants','trim');
			$this->form_validation->set_rules('type','Type','trim|required');
			$this->form_validation->set_rules('class','Class','trim|required');

			if ($this->form_validation->run() === TRUE)
			{
				$additional_data = array(
					'first_name' => $full_name,
					'phone' => $phone,
					'email' => $email,
					'flight_from' => $flight_from,
					'flight_to' => $flight_to,
					'takeoff_time' => $takeoff_time,
					'landing_time' => $landing_time,
					'adults' => $adults,
					'kids' => $kids,
					'infants' => $infants,
					'type' => $type,
					'class' => $class,
				);
				//echo '<pre>'; print_r($additional_data); echo '</pre>'; die();
				$this->bookings_model->add_booking($additional_data);
				$email_type = $this->bookings_model->get_email_config($type == 'thank_you');
				$sender_email = $email_type->sender_email;
				$sender_name = $email_type->sender_name;
				$sender_subject = $email_type->sender_subject;
				$receiver_subject = $email_type->receiver_subject;
				
				$message_admin = '<p style="font-size: 16px;">Hi'.
				'<p style="font-size: 16px;">You have received an inquery.</p>'.
				'<p style="font-size: 16px;">Your inquiry details are as following:</p>'.
				'<table style="font-size: 16px;">
					<thead>
						<tr>
							<th style="text-align: left;">Particulars</th>
							<th style="text-align: left;">Entires</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Full Name</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$full_name.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Phone</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$phone.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Flight From</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$flight_from.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Flight To</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$flight_to.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Departure Date</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$takeoff_time.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Arrival Date</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$landing_time.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Adults</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$adults.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Kids</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$kids.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Infants</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$infants.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Type</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$type.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Class</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$class.'</td>
						</tr>
					</tbody>
				</table>';
				$this->send_mail_to_admin($sender_email,$sender_name,$email,$sender_subject,$message_admin);
				$settings = (object)$this->settings_model->get_settings_options();
				$message_customer = '<p style="font-size: 16px;">Hi '.$full_name.' <'.$email.'></p>'.
				'<p style="font-size: 16px;">Thanks for sending an inquiry to '.$settings->site_title.'. We have received your request, one of our agent will contact you shortly.</p>'.
				'<p style="font-size: 16px;">Your inquiry details are as following:</p>'.
				'<table style="font-size: 16px;">
					<thead>
						<tr>
							<th style="text-align: left;">Particulars</th>
							<th style="text-align: left;">Entires</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Full Name</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$full_name.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Phone</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$phone.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Flight From</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$flight_from.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Flight To</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$flight_to.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Departure Date</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$takeoff_time.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Arrival Date</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$landing_time.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Adults</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$adults.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Kids</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$kids.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Infants</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$infants.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Type</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$type.'</td>
						</tr>
						<tr>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">Class</td>
							<td style="border: 1px solid #757575; padding: 10px 20px; width: 300px;">'.$class.'</td>
						</tr>
					</tbody>
				</table>';
				$this->send_mail_to_customer($sender_email,$sender_name,$email,$receiver_subject,$message_customer);
				//$this->user_model->update_user_data($additional_data);
			}
		}
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->_render_page('front' . DIRECTORY_SEPARATOR . 'bookings' . DIRECTORY_SEPARATOR . 'index', $this->data);
	}
    public function send_mail_to_admin($sender_email,$sender_name,$email,$sender_subject,$message_admin){
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->from($sender_email, $sender_name);
        $this->email->to($sender_email);
        $this->email->subject($sender_subject);
        $this->email->message($message_admin);
        $this->email->send();
    }
	public function send_mail_to_customer($sender_email,$sender_name,$email,$receiver_subject,$message_customer){
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->from($sender_email, $sender_name);
        $this->email->to($email);
        $this->email->subject($receiver_subject);
        $this->email->message($message_customer);
        $this->email->send();
		redirect("bookings", 'refresh');
    }
}

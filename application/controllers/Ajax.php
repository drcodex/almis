<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('flights_model');

		$this->lang->load('auth');
	}
	public function index()
	{
		if($_POST)
		{
			$flight_from = $this->input->post('flight_from');
			$flight_to = $this->input->post('flight_to');
			redirect("flights/routes/".$flight_from."/".$flight_to);
		}
		else
		{
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('front' . DIRECTORY_SEPARATOR . 'home', $this->data);
		}
	}
	public function fetch_from()
	{
		$output = '';
		$query = '';
		if($this->input->post('flight_from'))
		{
			$query = $this->input->post('flight_from');
		}
		$response = $this->flights_model->fetch_data($query);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}
	public function fetch_to()
	{
		$output = '';
		$query = '';
		if($this->input->post('flight_to'))
		{
			$query = $this->input->post('flight_to');
		}
		$response = $this->flights_model->fetch_data($query);
		//echo '<pre>'; print_r($data->result()); echo '</pre>'; die();
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Flights extends Backend_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('flights_model');
		$this->load->model('terminals_model');
		$this->load->model('airlines_model');
		$this->load->model('settings_model');

		$this->lang->load('auth');
	}

	public function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$ticket_price = $this->input->post('ticket_price');
			
				// validate form input
				$this->form_validation->set_rules('ticket_price','Flight From','trim');

				if ($this->form_validation->run() === TRUE)
				{
					if(!empty($ticket_price)){
						foreach ($ticket_price as $flight_id => $price) {
							$flight = $this->flights_model->get_flight_by_id($flight_id);
							$flightArray['id'] = $flight['id'];
							$flightArray['flight_price'] = $price;
							$flight_array[] = $flightArray;
						}
					}
					$this->db->update_batch('flights', $flight_array, 'id');
					//echo '<pre>'; print_r($flight_array); echo '</pre>'; die();
				}
			}
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$flights = $this->flights_model->get_flights();
			$get_flights_from = $this->flights_model->get_flights_from();
			$get_flights_stopover = $this->flights_model->get_flights_stopover();
			$get_flights_to = $this->flights_model->get_flights_to();
			$all_flights = array_replace_recursive($flights, $get_flights_from, $get_flights_stopover, $get_flights_to);
			$this->data['all_flights'] = $all_flights;
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
			$this->data['settings'] = (object)$this->settings_model->get_settings_options();
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'flights' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
	}
	
	/**
	 * Create a new Flight
	 */
	public function new_flight()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$term_id = $this->input->post('term_id');
				//echo '<pre>'; print_r($term_id); echo '</pre>'; die();
				$flight_from = $this->input->post('flight_from');
				$flight_stopover = $this->input->post('flight_stopover');
				$flight_to = $this->input->post('flight_to');
				$flight_airline = $this->input->post('flight_airline');
				$flight_aircraft_class = $this->input->post('flight_aircraft_class');
				$flight_protection = $this->input->post('flight_protection');
				$flight_route = $this->input->post('flight_route');
				$flight_price = $this->input->post('flight_price');
			
				// validate form input
				$this->form_validation->set_rules('term_id','Category','trim');
				$this->form_validation->set_rules('flight_from','Flight From','trim|required');
				$this->form_validation->set_rules('flight_stopover','Flight Stopover','trim');
				$this->form_validation->set_rules('flight_to','Flight To','trim|required');
				$this->form_validation->set_rules('flight_airline','Airline','trim|required');
				$this->form_validation->set_rules('flight_aircraft_class','Aircraft Class','trim|required');
				$this->form_validation->set_rules('flight_protection','Protection','trim|required');
				$this->form_validation->set_rules('flight_route','Route','trim');
				$this->form_validation->set_rules('flight_price','Price','trim|required');

				if ($this->form_validation->run() === TRUE)
				{
					$session_data = $this->session->userdata;
					$userID = $session_data['user_id'];
					$additional_data = array(
						'flight_from' => $flight_from,
						'flight_stopover' => $flight_stopover,
						'flight_to' => $flight_to,
						'flight_airline' => $flight_airline,
						'flight_aircraft_class' => $flight_aircraft_class,
						'flight_protection' => $flight_protection,
						'flight_route' => $flight_route,
						'flight_price' => $flight_price,
					);
					$object_id = $this->flights_model->add_flight($additional_data);
					if(!empty($term_id))
					{
						foreach ($term_id as $key => $value)
						{
							if(isset($key) && isset($value))
							{
								$new[] = array(
									'object_id' => $object_id,
									'term_taxonomy_id' => $value,
								);
							}
						}
						$this->db->insert_batch('term_relationships', $new);
					}
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect("admin/flights/edit_flight/". $object_id, 'refresh');
					//$this->user_model->update_user_data($additional_data);
				}
			}
			$this->data['flight_price'] = array(
				'name' => 'flight_price',
				'id' => 'flight_price',
				'type' => 'text',
				'value' => $this->form_validation->set_value('flight_price'),
				'class' => 'form-control"',
				'placeholder' => 'Enter ticket price',
			);
			$this->data['settings'] = (object)$this->settings_model->get_settings_options();
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->data['title'] = $this->lang->line('create_user_heading');
			$this->data['terms'] = $this->flights_model->get_terms_by_taxonomy($taxonomy = 'flights_category');
			//echo '<pre>'; print_r($this->data['terms']); echo '</pre>'; die();	
			$this->data['all_protections'] = $this->flights_model->get_protections();
			$this->data['all_aircraft_classes'] = $this->flights_model->get_aircraft_classes();				
			$this->data['all_terminals'] = $this->terminals_model->get_terminals();	
			$this->data['all_airlines'] = $this->airlines_model->get_airlines();	
			//echo '<pre>'; print_r($this->data['all_terminals']); echo '</pre>'; die();
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'flights' . DIRECTORY_SEPARATOR . 'new_flight', $this->data);
		}
	}
	
	public function edit_flight($id = NULL)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$flight = $this->flights_model->get_flight($id);
			if(!empty($flight))
			{
				if($_POST)
				{
					$term_id = $this->input->post('term_id');
					$flight_from = $this->input->post('flight_from');
					$flight_stopover = $this->input->post('flight_stopover');
					$flight_to = $this->input->post('flight_to');
					$airline_name = $this->input->post('airline_name');
					$aircraft_class_name = $this->input->post('aircraft_class_name');
					$protection_name = $this->input->post('protection_name');
					$flight_route = $this->input->post('flight_route');
					$flight_price = $this->input->post('flight_price');
				
					// validate form input
					$this->form_validation->set_rules('term_id','Category','trim');
					$this->form_validation->set_rules('flight_from','Flight From','trim|required');
					$this->form_validation->set_rules('flight_stopover','Flight Stopover','trim');
					$this->form_validation->set_rules('flight_to','Flight To','trim|required');
					$this->form_validation->set_rules('airline_name','Airline','trim|required');
					$this->form_validation->set_rules('aircraft_class_name','Aircraft Class','trim|required');
					$this->form_validation->set_rules('protection_name','Protection','trim|required');
					$this->form_validation->set_rules('flight_route','Route','trim');
					$this->form_validation->set_rules('flight_price','Price','trim|required');

					if ($this->form_validation->run() === TRUE)
					{
						$additional_data = array(
							'flight_from' => $flight_from,
							'flight_stopover' => $flight_stopover,
							'flight_to' => $flight_to,
							'flight_airline' => $airline_name,
							'flight_aircraft_class' => $aircraft_class_name,
							'flight_protection' => $protection_name,
							'flight_route' => $flight_route,
							'flight_price' => $flight_price,
						);
						$this->flights_model->update_flight($id, $additional_data);
						if(!empty($term_id))
						{
							foreach ($term_id as $key => $value)
							{
								$if_exist = $this->flights_model->check_if_term_id_exist_by_id($value, $id);
								$if_not_exist = $this->flights_model->check_if_term_id_not_exist_by_id($value, $id);
								if(empty($if_exist))
								{
									if(isset($key) && isset($value))
									{
										$new[] = array(
											'object_id' => $id,
											'term_taxonomy_id' => $value,
											'term_order' => '0',
										);
									}
								}
							}
							if(!empty($new))
							{
								$this->db->insert_batch('term_relationships', $new);
							}
							foreach ($term_id as $key => $value)
							{
								$if_exist = $this->flights_model->check_if_term_id_exist_by_id($value, $id);
								$if_not_exist = $this->flights_model->check_if_term_id_not_exist_by_id($value, $id);
								if(!empty($if_exist))
								{
									$old[] = $if_exist;
								}
							}
							if(!empty($old))
							{
								$difference = $this->relation_array_diff($if_not_exist, $old);
								$deletearray = array();
								foreach ($difference as $key => $value)
								{
									$object_id = $value['object_id'];
									$term_taxonomy_id = $value['term_taxonomy_id'];
									$this->flights_model->delete_term_relationships_id($object_id, $term_taxonomy_id);
								}
							}
						}
						else
						{
							$this->flights_model->delete_term_relationships($id);
						}
						redirect("admin/flights/edit_flight/" . $id, 'refresh');
					}
				}
				$terms = $this->flights_model->get_taxonomy_with_term($taxonomy = 'flights_category', $id);
				if (!empty($terms))
				{
					$terms_id = array();
					for ($i = 0; $i < count($terms); $i++) {
						$terms_id[] = $terms[$i]['term_taxonomy_id'];
					}
					$new_array = [];
					if(!empty($terms_id)){
						foreach ($terms_id as $value) {
							$taxonomyitems = $this->flights_model->get_terms_relations($id, $value);
							$tempArr['object_id'] = !empty($taxonomyitems['object_id']) ? $taxonomyitems['object_id'] : NULL;
							$tempArr['term_order'] = !empty($taxonomyitems['term_order']) ? $taxonomyitems['term_order'] : NULL;
							$new_array[] = $tempArr;
						}
					}
					$this->data['terms'] = (object)array_replace_recursive($terms, $new_array);
				}
				//echo '<pre>'; print_r($this->data['terms']); echo '</pre>'; die();
				$get_flights_from_single = $this->flights_model->get_flights_from_single($id);
				$get_flights_to_single = $this->flights_model->get_flights_to_single($id);
				$single_flight = array_replace_recursive($flight, $get_flights_from_single, $get_flights_to_single);
				$this->data['flight_from'] = array(
					'name' => 'flight_from',
					'id' => 'flight_from',
					'type' => 'text',
					'value' => $this->form_validation->set_value('flight_from', !empty($single_flight['flight_from']) ? $single_flight['flight_from'] : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter any stopover',
				);
				$this->data['flight_to'] = array(
					'name' => 'flight_to',
					'id' => 'flight_to',
					'type' => 'text',
					'value' => $this->form_validation->set_value('flight_to', !empty($single_flight['flight_to']) ? $single_flight['flight_to'] : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter any stopover',
				);
				$this->data['airline_name'] = array(
					'name' => 'airline_name',
					'id' => 'airline_name',
					'type' => 'text',
					'value' => $this->form_validation->set_value('airline_name', !empty($single_flight['airline_name']) ? $single_flight['airline_name'] : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter airline',
				);
				$this->data['aircraft_class_name'] = array(
					'name' => 'aircraft_class_name',
					'id' => 'aircraft_class_name',
					'type' => 'text',
					'value' => $this->form_validation->set_value('aircraft_class_name', !empty($single_flight['aircraft_class_name']) ? $single_flight['aircraft_class_name'] : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter currency unit',
				);
				$this->data['protection_name'] = array(
					'name' => 'protection_name',
					'id' => 'protection_name',
					'type' => 'text',
					'value' => $this->form_validation->set_value('protection_name', !empty($single_flight['protection_name']) ? $single_flight['protection_name'] : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter ticket price',
				);
				$this->data['flight_route'] = array(
					'name' => 'flight_route',
					'id' => 'flight_route',
					'type' => 'text',
					'value' => $this->form_validation->set_value('flight_route', !empty($single_flight['flight_route']) ? $single_flight['flight_route'] : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter currency unit',
				);
				$this->data['flight_price'] = array(
					'name' => 'flight_price',
					'id' => 'flight_price',
					'type' => 'text',
					'value' => $this->form_validation->set_value('flight_price', !empty($single_flight['flight_price']) ? $single_flight['flight_price'] : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter ticket price',
				);
				
				$this->data['settings'] = (object)$this->settings_model->get_settings_options();
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
				$this->data['single_flight'] = $single_flight;
				$this->data['title'] = $this->lang->line('create_user_heading');
				$this->data['all_protections'] = $this->flights_model->get_protections();
				$this->data['all_aircraft_classes'] = $this->flights_model->get_aircraft_classes();				
				$this->data['all_terminals'] = $this->terminals_model->get_terminals();	
				$this->data['all_airlines'] = $this->airlines_model->get_airlines();
				$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'flights' . DIRECTORY_SEPARATOR . 'edit_flight', $this->data);
			}
			else
			{
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->_render_page('errors' . DIRECTORY_SEPARATOR . '404', $this->data);
			}
		}
	}
	
	public function delete_flight($id)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->flights_model->delete_term_relationship_by_flight_id($id);
			$this->flights_model->delete_flight($id);
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
			redirect("admin/flights", 'refresh');
		}
	}
	
	public function aircraft_classes()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$aircraft_class_name = $this->input->post('aircraft_class_name');
				$aircraft_class_description = $this->input->post('aircraft_class_description');
			
				// validate form input
				$this->form_validation->set_rules('aircraft_class_name','Aircraft Class Name','trim|required');
				$this->form_validation->set_rules('aircraft_class_description','Aircraft Class Details','trim');

				if ($this->form_validation->run() === TRUE)
				{
					$additional_data = array(
						'aircraft_class_name' => $aircraft_class_name,
						'aircraft_class_description' => $aircraft_class_description,
					);
					$this->load->model('flights_model');
					$this->flights_model->add_aircraft_class($additional_data);
					//echo '<pre>'; print_r($this->data); echo '</pre>'; die('YES');
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect("admin/flights/aircraft_classes", 'refresh');
					//$this->user_model->update_user_data($additional_data);
				}
			}
			$this->data['aircraft_class_name'] = array(
				'name' => 'aircraft_class_name',
				'id' => 'aircraft_class_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('aircraft_class_name'),
				'class' => 'form-control"',
				'placeholder' => 'Enter aircraft class name',
			);
			$this->data['aircraft_class_description'] = array(
				'name' => 'aircraft_class_description',
				'id' => 'aircraft_class_description',
				'type' => 'text',
				'value' => $this->form_validation->set_value('aircraft_class_description'),
				'class' => 'form-control"',
				'placeholder' => 'Enter aircraft class detail (if any)',
			);
			$this->data['all_aircraft_classes'] = $this->flights_model->get_aircraft_classes();	
			//list the users
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->data['class'] = $this->session->flashdata('class');
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'flights' . DIRECTORY_SEPARATOR . 'aircraft_classes', $this->data);
		}
	}
	
	public function edit_aircraft_class($id = NULL)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$single_class = $this->flights_model->get_single_aircraft_class($id);
			if(!empty($single_class))
			{
				if($_POST)
				{
						$aircraft_class_name = $this->input->post('aircraft_class_name');
						$aircraft_class_description = $this->input->post('aircraft_class_description');
					
						// validate form input
						$this->form_validation->set_rules('aircraft_class_name','Aircraft Class Name','trim|required');
						$this->form_validation->set_rules('aircraft_class_description','Aircraft Class Details','trim');

					if ($this->form_validation->run() === TRUE)
					{
						$additional_data = array(
							'aircraft_class_name' => $aircraft_class_name,
							'aircraft_class_description' => $aircraft_class_description,
						);
						$this->flights_model->update_aircraft_class($id, $additional_data);
						//echo '<pre>'; print_r($this->data); echo '</pre>'; die('YES');
						$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
						redirect('admin/flights/edit_aircraft_class/'.$id);
						//$this->user_model->update_user_data($additional_data);
					}
				}
				$this->data['aircraft_class_name'] = array(
					'name' => 'aircraft_class_name',
					'id' => 'aircraft_class_name',
					'type' => 'text',
					'value' => $this->form_validation->set_value('aircraft_class_name', !empty($single_class->aircraft_class_name) ? $single_class->aircraft_class_name : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter airline class',
				);
				$this->data['aircraft_class_description'] = array(
					'name' => 'aircraft_class_description',
					'id' => 'aircraft_class_description',
					'type' => 'text',
					'value' => $this->form_validation->set_value('aircraft_class_description', !empty($single_class->aircraft_class_description) ? $single_class->aircraft_class_description : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter airline class detail (if any)',
				);
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
				$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'flights' . DIRECTORY_SEPARATOR . 'edit_aircraft_class', $this->data);
			}
			else
			{
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->_render_page('errors' . DIRECTORY_SEPARATOR . '404', $this->data);
			}
		}
	}
	
	public function delete_aircraft_class($id)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$check_class_exist = $this->flights_model->check_class_exist_in_flights($id);
			$class_count = count($check_class_exist);
			//echo '<pre>'; print_r($class_count); echo '</pre>'; die();
			if($class_count == 0)
			{
				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->flights_model->delete_aircraft_class($id);
				//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
				redirect("admin/flights/aircraft_classes/", 'refresh');
			}
			else
			{
				if($class_count > 1)
				{
					$this->session->set_flashdata('message', 'This aircraft class is linked with '.$class_count.' flights, Unlink first to delete this class');
					$this->session->set_flashdata('class', 'btn btn-danger waves-effect waves-light');
					redirect('admin/flights/aircraft_classes');
				}
				else
				{
					$this->session->set_flashdata('message', 'This aircraft class is linked with 1 flight, Unlink first to delete this class');
					$this->session->set_flashdata('class', 'btn btn-danger waves-effect waves-light');
					redirect('admin/flights/aircraft_classes');
				}
			}
		}
	}
	
	public function protections()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
					$protection_name = $this->input->post('protection_name');
					$protection_description = $this->input->post('protection_description');
				
					// validate form input
					$this->form_validation->set_rules('protection_name','Protection Name','trim|required');
					$this->form_validation->set_rules('protection_description','Protections Details','trim');

				if ($this->form_validation->run() === TRUE)
				{
					$additional_data = array(
						'protection_name' => $protection_name,
						'protection_description' => $protection_description,
					);
					$this->flights_model->add_protection($additional_data);
					//echo '<pre>'; print_r($this->data); echo '</pre>'; die('YES');
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect("admin/flights/protections", 'refresh');
					//$this->user_model->update_user_data($additional_data);
				}
			}
			$this->data['protection_name'] = array(
				'name' => 'protection_name',
				'id' => 'protection_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('protection_name'),
				'class' => 'form-control"',
				'placeholder' => 'Enter airline class',
			);
			$this->data['protection_description'] = array(
				'name' => 'protection_description',
				'id' => 'protection_description',
				'type' => 'text',
				'value' => $this->form_validation->set_value('protection_description'),
				'class' => 'form-control"',
				'placeholder' => 'Enter airline class detail (if any)',
			);
			$this->data['all_protections'] = $this->flights_model->get_protections();	
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->data['class'] = $this->session->flashdata('class');
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'flights' . DIRECTORY_SEPARATOR . 'protections', $this->data);
		}
	}
	
	public function edit_protection($id = NULL)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$single_protection = $this->flights_model->get_single_protection($id);
			if(!empty($single_protection))
			{
				if($_POST)
				{
						$protection_name = $this->input->post('protection_name');
						$protection_description = $this->input->post('protection_description');
					
						// validate form input
						$this->form_validation->set_rules('protection_name','Aircraft Class Name','trim|required');
						$this->form_validation->set_rules('protection_description','Aircraft Class Details','trim');

					if ($this->form_validation->run() === TRUE)
					{
						$additional_data = array(
							'protection_name' => $protection_name,
							'protection_description' => $protection_description,
						);
						$this->flights_model->update_protection($id, $additional_data);
						//echo '<pre>'; print_r($this->data); echo '</pre>'; die('YES');
						$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
						redirect("admin/flights/edit_protection", 'refresh');
						//$this->user_model->update_user_data($additional_data);
					}
				}
				$this->data['protection_name'] = array(
					'name' => 'protection_name',
					'id' => 'protection_name',
					'type' => 'text',
					'value' => $this->form_validation->set_value('protection_name', !empty($single_protection->protection_name) ? $single_protection->protection_name : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter airline class',
				);
				$this->data['protection_description'] = array(
					'name' => 'protection_description',
					'id' => 'protection_description',
					'type' => 'text',
					'value' => $this->form_validation->set_value('protection_description', !empty($single_protection->protection_description) ? $single_protection->protection_description : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter airline class detail (if any)',
				);
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
				$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'flights' . DIRECTORY_SEPARATOR . 'edit_protection', $this->data);
			}
			else
			{
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->_render_page('errors' . DIRECTORY_SEPARATOR . '404', $this->data);
			}
		}
	}
	
	public function delete_protection($id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$check_protection_exist = $this->flights_model->check_protection_exist_in_flights($id);
			$class_protection = count($check_protection_exist);
			//echo '<pre>'; print_r($class_count); echo '</pre>'; die();
			if($class_protection == 0)
			{
				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->flights_model->delete_protection($id);
				//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
				redirect('admin/flights/protections');
			}
			else
			{
				if($class_protection > 1)
				{
					$this->session->set_flashdata('message', 'This protections is linked with '.$class_protection.' flights, Unlink first to delete this protections');
					$this->session->set_flashdata('class', 'btn btn-danger waves-effect waves-light');
					redirect('admin/flights/protections');
				}
				else
				{
					$this->session->set_flashdata('message', 'This protections is linked with 1 flight, Unlink first to delete this protection');
					$this->session->set_flashdata('class', 'btn btn-danger waves-effect waves-light');
					redirect('admin/flights/protections');
				}
			}
		}
	}
	public function categories()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$aircraft_class_name = $this->input->post('aircraft_class_name');
				$aircraft_class_description = $this->input->post('aircraft_class_description');
			
				// validate form input
				$this->form_validation->set_rules('aircraft_class_name','Aircraft Class Name','trim|required');
				$this->form_validation->set_rules('aircraft_class_description','Aircraft Class Details','trim');

				if ($this->form_validation->run() === TRUE)
				{
					$additional_data = array(
						'aircraft_class_name' => $aircraft_class_name,
						'aircraft_class_description' => $aircraft_class_description,
					);
					$this->load->model('flights_model');
					$this->flights_model->add_aircraft_class($additional_data);
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect("admin/flights/aircraft_classes", 'refresh');
					//$this->user_model->update_user_data($additional_data);
				}
			}
			$this->data['flight_category_name'] = array(
				'name' => 'flight_category_name',
				'id' => 'flight_category_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('flight_category_name'),
				'class' => 'form-control"',
				'placeholder' => 'Enter flight category name',
			);
			$this->data['flight_category_slug'] = array(
				'name' => 'flight_category_slug',
				'id' => 'flight_category_slug',
				'type' => 'text',
				'value' => $this->form_validation->set_value('flight_category_slug'),
				'class' => 'form-control"',
				'placeholder' => 'Enter flight category slug',
			);
			$this->data['flight_categories'] = $this->flights_model->get_terms();	
			//echo '<pre>'; print_r($this->data['flight_categories']); echo '</pre>'; die();
			//list the users
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->data['class'] = $this->session->flashdata('class');
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'flights' . DIRECTORY_SEPARATOR . 'categories', $this->data);
		}
	}
	public function relation_array_diff($arraya, $arrayb) {
		foreach ($arraya as $keya => $valuea) {
			if (in_array($valuea, $arrayb)) {
				unset($arraya[$keya]);
			}
		}
		return $arraya;
	}
}

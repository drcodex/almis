<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Configurations extends Backend_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('configurations_model');

		$this->lang->load('auth');
	}
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'bookings' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
	}
	public function email($type = NULL)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if(empty($type) && $type == NULL)
			{
				$this->data['email_config'] = $this->configurations_model->get_all_email_config();
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
				$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'configurations' . DIRECTORY_SEPARATOR . 'email' . DIRECTORY_SEPARATOR . 'index', $this->data);
			}
			elseif(!empty($type) && $type == 'thank_you')
			{
				if($_POST)
				{
					$sender_name = $this->input->post('sender_name');
					$sender_email = $this->input->post('sender_email');
					$sender_cc = $this->input->post('sender_cc');
					$sender_bcc = $this->input->post('sender_bcc');
					$sender_subject = $this->input->post('sender_subject');
					$receiver_subject = $this->input->post('receiver_subject');
				
					// validate form input
					$this->form_validation->set_rules('sender_name','Sender Name','trim|required');
					$this->form_validation->set_rules('sender_email','Sender Email','trim|required|valid_email');
					$this->form_validation->set_rules('sender_cc','CC Email Address','trim|valid_email');
					$this->form_validation->set_rules('sender_bcc','BCC Email Address','trim|valid_email');
					$this->form_validation->set_rules('sender_subject','Sender Subject','trim|required');
					$this->form_validation->set_rules('receiver_subject','Receiver Subject','trim|required');

					if ($this->form_validation->run() === TRUE)
					{
						$data = array(
							'sender_name' => $sender_name,
							'sender_email' => $sender_email,
							'sender_cc' => $sender_cc,
							'sender_bcc' => $sender_bcc,
							'sender_subject' => $sender_subject,
							'receiver_subject' => $receiver_subject,
						);
						//echo '<pre>'; print_r($additional_data); echo '</pre>'; die();
						$this->configurations_model->update_email_config($type, $data);
						redirect('admin/configurations/email/'.$type, 'refresh');
					}
				}
				$email_type = $this->configurations_model->get_email_config($type);
				$this->data['sender_name'] = array(
					'name' => 'sender_name',
					'id' => 'sender_name',
					'type' => 'text',
					'value' => $this->form_validation->set_value('sender_name', !empty($email_type->sender_name) ? $email_type->sender_name : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter sender name',
				);
				$this->data['sender_email'] = array(
					'name' => 'sender_email',
					'id' => 'sender_email',
					'type' => 'email',
					'value' => $this->form_validation->set_value('sender_email', !empty($email_type->sender_email) ? $email_type->sender_email : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter sender email',
				);
				$this->data['sender_cc'] = array(
					'name' => 'sender_cc',
					'id' => 'sender_cc',
					'type' => 'email',
					'value' => $this->form_validation->set_value('sender_cc', !empty($email_type->sender_cc) ? $email_type->sender_cc : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter CC email address',
				);
				$this->data['sender_bcc'] = array(
					'name' => 'sender_bcc',
					'id' => 'sender_bcc',
					'type' => 'email',
					'value' => $this->form_validation->set_value('sender_bcc', !empty($email_type->sender_bcc) ? $email_type->sender_bcc : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter BCC email address',
				);
				$this->data['sender_subject'] = array(
					'name' => 'sender_subject',
					'id' => 'sender_subject',
					'type' => 'text',
					'value' => $this->form_validation->set_value('sender_subject', !empty($email_type->sender_subject) ? $email_type->sender_subject : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter sender email subject',
				);
				$this->data['receiver_subject'] = array(
					'name' => 'receiver_subject',
					'id' => 'receiver_subject',
					'type' => 'text',
					'value' => $this->form_validation->set_value('receiver_subject', !empty($email_type->receiver_subject) ? $email_type->receiver_subject : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter customer email subject',
				);
				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
				$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'configurations' . DIRECTORY_SEPARATOR . 'email' . DIRECTORY_SEPARATOR . 'thank_you', $this->data);
			}
			elseif(!empty($type) && $type == 'contact_us')
			{
				//echo '<pre>'; print_r('im here'); echo '</pre>'; die();
				if($_POST)
				{
					$sender_name = $this->input->post('sender_name');
					$sender_email = $this->input->post('sender_email');
					$sender_cc = $this->input->post('sender_cc');
					$sender_bcc = $this->input->post('sender_bcc');
					$sender_subject = $this->input->post('sender_subject');
				
					// validate form input
					$this->form_validation->set_rules('sender_name','Sender Name','trim|required');
					$this->form_validation->set_rules('sender_email','Sender Email','trim|required|valid_email');
					$this->form_validation->set_rules('sender_cc','CC Email Address','trim|valid_email');
					$this->form_validation->set_rules('sender_bcc','BCC Email Address','trim|valid_email');
					$this->form_validation->set_rules('sender_subject','Sender Subject','trim|required');

					if ($this->form_validation->run() === TRUE)
					{
						$data = array(
							'sender_name' => $sender_name,
							'sender_email' => $sender_email,
							'sender_cc' => $sender_cc,
							'sender_bcc' => $sender_bcc,
							'sender_subject' => $sender_subject,
						);
						//echo '<pre>'; print_r($data); echo '</pre>'; die();
						$this->configurations_model->update_email_config($type, $data);
						redirect('admin/configurations/email/'.$type, 'refresh');
					}
				}
				$email_type = $this->configurations_model->get_email_config($type);
				$this->data['sender_name'] = array(
					'name' => 'sender_name',
					'id' => 'sender_name',
					'type' => 'text',
					'value' => $this->form_validation->set_value('sender_name', !empty($email_type->sender_name) ? $email_type->sender_name : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter sender name',
				);
				$this->data['sender_email'] = array(
					'name' => 'sender_email',
					'id' => 'sender_email',
					'type' => 'email',
					'value' => $this->form_validation->set_value('sender_email', !empty($email_type->sender_email) ? $email_type->sender_email : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter sender email',
				);
				$this->data['sender_cc'] = array(
					'name' => 'sender_cc',
					'id' => 'sender_cc',
					'type' => 'email',
					'value' => $this->form_validation->set_value('sender_cc', !empty($email_type->sender_cc) ? $email_type->sender_cc : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter CC email address',
				);
				$this->data['sender_bcc'] = array(
					'name' => 'sender_bcc',
					'id' => 'sender_bcc',
					'type' => 'email',
					'value' => $this->form_validation->set_value('sender_bcc', !empty($email_type->sender_bcc) ? $email_type->sender_bcc : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter BCC email address',
				);
				$this->data['sender_subject'] = array(
					'name' => 'sender_subject',
					'id' => 'sender_subject',
					'type' => 'text',
					'value' => $this->form_validation->set_value('sender_subject', !empty($email_type->sender_subject) ? $email_type->sender_subject : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter sender email subject',
				);
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
				$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'configurations' . DIRECTORY_SEPARATOR . 'email' . DIRECTORY_SEPARATOR . 'contact_us_email', $this->data);
			}
			else
			{
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->_render_page('errors' . DIRECTORY_SEPARATOR . '404', $this->data);
			}
		}
	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Dashboard extends Backend_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('dashboard_model');

		$this->lang->load('auth');
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data['count_inquiries'] = $this->dashboard_model->count_inquiries();
			$this->data['count_flights'] = $this->dashboard_model->count_flights();
			$this->data['count_airlines'] = $this->dashboard_model->count_airlines();
			$this->data['count_destinations'] = $this->dashboard_model->count_destinations();
			$this->data['count_hajj_packages'] = $this->dashboard_model->count_packages($type = 'hajj');
			$this->data['count_umrah_packages'] = $this->dashboard_model->count_packages($type = 'umrah');
			//echo '<pre>'; print_r($count_inquiries); echo '</pre>'; die();
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'dashboard' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
	}
}

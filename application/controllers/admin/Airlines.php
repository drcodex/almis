<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Airlines extends Backend_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));
        $config =  array(
            'encrypt_name'    => TRUE,
			'upload_path'     => "./uploads/images/airlines/",
			'allowed_types'   => "gif|jpg|png|jpeg",
			'overwrite'       => FALSE,
			'max_size'        => "2000",
			'max_height'      => "2024",
			'max_width'       => "2024"
        );
        $this->load->library('upload', $config);
        $this->load->library('image_lib');

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('airlines_model');

		$this->lang->load('auth');
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['class'] = $this->session->flashdata('class');
			$this->data['all_airlines'] = $this->airlines_model->get_all_airlines();
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'airlines' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
	}
	public function add_airline()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$featured_image_id = $this->input->post('featured_image_id');
				$airline_short_code = $this->input->post('airline_short_code');
				$airline_name = $this->input->post('airline_name');
				
				// validate form input
				$this->form_validation->set_rules('airline_short_code','Airline Short Code','trim|required');
				$this->form_validation->set_rules('airline_name','Airline Name','trim|required');

				if ($this->form_validation->run() === TRUE)
				{
					$additional_data = array(
						'airline_short_code' => $airline_short_code,
						'airline_name' => $airline_name,
						'status' => 'Published',
					);
					//echo '<pre>'; print_r($this->session->all_userdata()); echo '</pre>'; die();
					$last_id = $this->airlines_model->add_airline($additional_data);
					if(!empty($featured_image_id))
					{
						$other_data = array(
							'airline_id' => $last_id,
						);
						$this->airlines_model->update_featured_image_airline_id($featured_image_id, $other_data);
					}
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect("admin/airlines/edit_airline/" . $last_id, 'refresh');
				}
			}
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
			$this->data['airline_short_code'] = array(
				'name' => 'airline_short_code',
				'id' => 'airline_short_code',
				'type' => 'text',
				'value' => $this->form_validation->set_value('airline_short_code'),
				'class' => 'form-control"',
				'placeholder' => 'Edit airline short code',
			);
			$this->data['airline_name'] = array(
				'name' => 'airline_name',
				'id' => 'airline_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('airline_name'),
				'class' => 'form-control"',
				'placeholder' => 'Enter airline name',
			);
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'airlines' . DIRECTORY_SEPARATOR . 'new_airline', $this->data);
		}
	}
	
	public function edit_airline($id = NULL)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$single_airline = $this->airlines_model->get_single_airline($id);
			if(!empty($single_airline))
			{
				if($_POST)
				{
					$airline_short_code = $this->input->post('airline_short_code');
					$airline_name = $this->input->post('airline_name');
					
					// validate form input
					$this->form_validation->set_rules('airline_short_code','Airline Short Code','trim|required');
					$this->form_validation->set_rules('airline_name','Airline Name','trim|required');

					if ($this->form_validation->run() === TRUE)
					{
						$additional_data = array(
							'airline_short_code' => $airline_short_code,
							'airline_name' => $airline_name,
						);
						//echo '<pre>'; print_r($this->session->all_userdata()); echo '</pre>'; die();
						$this->airlines_model->update_airline($id, $additional_data);
						$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
						redirect("admin/airlines/edit_airline/" . $id, 'refresh');
					}
				}
				$this->data['airline_short_code'] = array(
					'name' => 'airline_short_code',
					'id' => 'airline_short_code',
					'type' => 'text',
					'value' => $this->form_validation->set_value('airline_short_code', !empty($single_airline->airline_short_code) ? $single_airline->airline_short_code : ""),
					'class' => 'form-control"',
					'placeholder' => 'Edit airline short code',
				);
				$this->data['airline_name'] = array(
					'name' => 'airline_name',
					'id' => 'airline_name',
					'type' => 'text',
					'value' => $this->form_validation->set_value('airline_name', !empty($single_airline->airline_name) ? $single_airline->airline_name : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter airline name',
				);
				$airline_logo = $this->airlines_model->get_airline_logo($id);
				$this->data['airline_logo'] = $airline_logo;
				//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
				$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'airlines' . DIRECTORY_SEPARATOR . 'edit_airline', $this->data);
			}
			else
			{
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->_render_page('errors' . DIRECTORY_SEPARATOR . '404', $this->data);
			}
		}
	}
	
	public function delete_airline($id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$check_flights_exist = $this->airlines_model->check_flight_exist_in_airline($id);
			$exist_flights = count($check_flights_exist);
			//echo '<pre>'; print_r($exist_flights); echo '</pre>'; die();
			if($exist_flights == 0)
			{
				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->airlines_model->delete_airline($id);
				redirect('admin/airlines');
			}
			else
			{
				if($exist_flights > 1)
				{
					$this->session->set_flashdata('message', 'This airline is linked with '.$exist_flights.' flights, Unlink first to delete this airline');
					$this->session->set_flashdata('class', 'btn btn-danger waves-effect waves-light');
					redirect('admin/airlines');
				}
				else
				{
					$this->session->set_flashdata('message', 'This airline is linked with 1 flight, Unlink first to delete this airline');
					$this->session->set_flashdata('class', 'btn btn-danger waves-effect waves-light');
					redirect('admin/airlines');
				}
			}
		}
	}
    public function upload_file()
    {
		$response =  array("response"=>"no","content"=>"");
        $config['upload_path'] = './uploads/images/airlines/';

        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('file'))
		{
			$theHTMLResponse = '';
			$message = 'Upload failed. File size (max limit: 2mb) or file resolution too high.';
			$response = array('response'=>'yes', 'response_type'=>'error', 'content'=>$theHTMLResponse, 'message'=>$message);
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($response));
		}
		else
        {
            $file = $this->upload->data();
            $thumb_config["image_library"] = "gd2";
            $thumb_config["source_image"] = $file["full_path"];
            $thumb_config['new_image'] = 'uploads/images/airlines/thumb/'.$file['file_name'];
            $thumb_config['create_thumb'] = TRUE;
            $thumb_config['thumb_marker'] = '';
            $thumb_config['maintain_ratio'] = TRUE;
            $thumb_config['width'] = 550;
            $thumb_config['height'] = 424;
            $this->load->library('image_lib', $thumb_config);
            $this->image_lib->initialize($thumb_config);
            $this->image_lib->resize();
            $this->image_lib->clear();
			$airline_id = $this->input->post('airline_id');
			if(!empty($airline_id))
			{
				$airline_logo = $this->airlines_model->get_airline_logo_by_airline_id($airline_id);
				if(!empty($airline_logo))
				{
					$path = $airline_logo->path;
					$thumb =$airline_logo->thumb;
					$this->airlines_model->delete_airline_logo($airline_id, $path, $thumb);
				}
			}
            $data = array (
                'title' => $file['orig_name'],
                'path' => 'uploads/images/airlines/'.$file['file_name'],
                'ext' => $file['file_ext'],
                'thumb' => $thumb_config['new_image'],
                'size' => $file['file_size'],
	            'airline_id' => $airline_id,
	            'created_at' => date('Y-m-d h:i:s')
            );
			$last_id = $this->airlines_model->upload_airline_logo($airline_id, $data);
			$airline_logo = $this->airlines_model->get_airline_image_by_last_id($last_id);
			if($airline_logo)
			{
				$this->data['airline_id'] = $last_id;
				$this->data['airline_logo'] = $airline_logo;
				$theHTMLResponse = $this->load->view('admin/ajax_parts/part_airline_id', $this->data, true);
				$message = 'Airline logo image has been uploaded';
				$response = array('response'=>'yes','response_type'=>'custom','content'=>$theHTMLResponse, 'message'=>$message);
			}
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($response));
        }
    }
}

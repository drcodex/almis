<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class News extends Backend_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	/**
	 * Redirect if needed, otherwise display the user list
	 */
	public function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'post' . DIRECTORY_SEPARATOR . 'all_posts', $this->data);
		}
	}
	public function all_news()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->load->model('posts_model');
			$this->data['posts'] = $this->posts_model->get_news();
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die('YES');
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'news' . DIRECTORY_SEPARATOR . 'all_news', $this->data);
		}
	}
	
	public function edit_news($ID)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->load->model('posts_model');
			$this->data['posts'] = $this->posts_model->get_single_news($ID);
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die('YES');
			
			$this->data['news_title'] = array(
				'name' => 'news_title',
				'id' => 'news_title',
				'type' => 'text',
				'value' => $this->data['posts']->news_title,
				'class' => 'form-control"',
				'placeholder' => 'News Title',
			);
			$this->data['news_slug'] = array(
				'name' => 'news_slug',
				'id' => 'news_slug',
				'type' => 'text',
				'value' => $this->data['posts']->news_slug,
				'class' => 'form-control"',
				'placeholder' => 'News Slug',
			);
			$this->data['news_details'] = array(
				'name' => 'news_details',
				'id' => 'news_details',
				'type' => 'text',
				'value' => $this->data['posts']->news_details,
				'class' => 'form-control"',
				'placeholder' => 'News Details',
			);
			
			if($_POST)
			{
				$news_title = $this->input->post('news_title');
				$news_slug = $this->input->post('news_slug');
				$news_details = $this->input->post('news_details');
			
				// validate form input
				$this->form_validation->set_rules('news_title','News Title','trim|required');
				$this->form_validation->set_rules('news_slug','News Slug','trim|required');
				$this->form_validation->set_rules('news_details','News Details','trim|required');

				if (!$this->form_validation->run())
				{
					// check to see if we are creating the user
					// redirect them back to the admin page
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					$this->_render_page('admin'. DIRECTORY_SEPARATOR .'news'. DIRECTORY_SEPARATOR .'edit_news', $this->data);
				}
				else
				{
					$additional_data = array(
						'news_title' => $news_title,
						'news_slug' => $news_slug,
						'news_details' => $news_details
					);
					//echo '<pre>'; print_r($this->session->all_userdata()); echo '</pre>'; die('YES');
					$this->load->model('posts_model');
					$this->posts_model->update_news($ID, $additional_data);
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect("admin/news/edit_news/" . $ID, 'refresh');
					//$this->user_model->update_user_data($additional_data);
				}
			}
			else
			{
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
				$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'news' . DIRECTORY_SEPARATOR . 'edit_news', $this->data);
			}
		}
	}
	
	public function delete_news($ID)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->load->model('posts_model');
			$this->data['posts'] = $this->posts_model->delete_news($ID);
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die('YES');
			redirect("admin/news/all_news/", 'refresh');
		}
	}
	
	/**
	 * Create a new News
	 */
	public function create_news()
	{
		$this->data['title'] = $this->lang->line('create_user_heading');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('admin/auth', 'refresh');
		}
		$this->data['news_title'] = array(
			'name' => 'news_title',
			'id' => 'news_title',
			'type' => 'text',
			'value' => $this->form_validation->set_value('news_title'),
			'class' => 'form-control"',
			'placeholder' => 'News Title',
		);
		$this->data['news_slug'] = array(
			'name' => 'news_slug',
			'id' => 'news_slug',
			'type' => 'text',
			'value' => $this->form_validation->set_value('news_slug'),
			'class' => 'form-control"',
			'placeholder' => 'News Slug',
		);
		$this->data['news_details'] = array(
			'name' => 'news_details',
			'id' => 'news_details',
			'type' => 'text',
			'value' => $this->form_validation->set_value('news_details'),
			'class' => 'form-control"',
			'placeholder' => 'News Details',
		);
		$this->data['news_seo_title'] = array(
			'name' => 'news_seo_title',
			'id' => 'news_seo_title',
			'type' => 'text',
			'value' => $this->form_validation->set_value('news_seo_title'),
			'class' => 'form-control"',
			'placeholder' => 'News SEO Title',
		);
		$this->data['news_seo_slug'] = array(
			'name' => 'news_seo_slug',
			'id' => 'news_seo_slug',
			'type' => 'text',
			'value' => $this->form_validation->set_value('news_seo_slug'),
			'class' => 'form-control"',
			'placeholder' => 'News SEO Slug',
		);
		$this->data['news_seo_description'] = array(
			'name' => 'news_seo_description',
			'id' => 'news_seo_description',
			'type' => 'text',
			'value' => $this->form_validation->set_value('news_seo_description'),
			'class' => 'form-control"',
			'placeholder' => 'News SEO Description',
		);
		if($_POST)
		{
			$news_title = $this->input->post('news_title');
			$news_slug = $this->input->post('news_slug');
			$news_details = $this->input->post('news_details');
			$news_seo_title = $this->input->post('news_seo_title');
			$news_seo_slug = $this->input->post('news_seo_slug');
			$news_seo_description = $this->input->post('news_seo_description');
		
			// validate form input
			$this->form_validation->set_rules('news_title','News Title','trim|required');
			$this->form_validation->set_rules('news_slug','News Slug','trim|required');
			$this->form_validation->set_rules('news_details','News Details','trim|required');
			$this->form_validation->set_rules('news_seo_title','SEO Title','trim');
			$this->form_validation->set_rules('news_seo_slug','SEO Slug','trim');
			$this->form_validation->set_rules('news_seo_description','SEO Description','trim');

			if (!$this->form_validation->run())
			{
				// check to see if we are creating the user
				// redirect them back to the admin page
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
				$this->_render_page('admin'. DIRECTORY_SEPARATOR .'news'. DIRECTORY_SEPARATOR .'create_news', $this->data);
			}
			else
			{
				$session_data = $this->session->userdata;
				$userID = $session_data['user_id'];
				$additional_data = array(
					'news_author' => $userID,
					'news_title' => $news_title,
					'news_slug' => $news_slug,
					'news_details' => $news_details,
					'news_date' => date('Y-m-d H:i:s'),
				);
				//echo '<pre>'; print_r($this->session->all_userdata()); echo '</pre>'; die('YES');
				$this->load->model('posts_model');
				$this->posts_model->add_news($additional_data);
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
				redirect("admin/news/create_news", 'refresh');
				//$this->user_model->update_user_data($additional_data);
			}
		}
		else
		{
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'news' . DIRECTORY_SEPARATOR . 'create_news', $this->data);
		}
	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Testimonials extends Backend_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('testimonials_model');

		$this->lang->load('auth');
	}
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data['testimonials'] = $this->testimonials_model->get_testimonials();
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'testimonials' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
	}
	
	/**
	 * Create Testimonials
	 */
	public function add_testimonial()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$testimonial = $this->input->post('testimonial');
				$name = $this->input->post('name');
				$designation = $this->input->post('designation');
				$company = $this->input->post('company');
			
				// validate form input
				$this->form_validation->set_rules('testimonial','Testimonial','trim|required');
				$this->form_validation->set_rules('name','Name','trim|required');
				$this->form_validation->set_rules('designation','Designation','trim');
				$this->form_validation->set_rules('company','Comapany','trim');

				if ($this->form_validation->run() === TRUE)
				{
					$data = array(
						'comment' => $testimonial,
						'name' => $name,
						'designation' => $designation,
						'company' => $company,
					);
					//echo '<pre>'; print_r($data); echo '</pre>'; die();
					$testimonial_id = $this->testimonials_model->add_testimonial($data);
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect("admin/testimonials/edit_testimonial/". $testimonial_id, 'refresh');
					//$this->user_model->update_user_data($additional_data);
				}
			}
			$this->data['testimonial'] = array(
				'name' => 'testimonial',
				'id' => 'testimonial',
				'type' => 'text',
				'value' => $this->form_validation->set_value('testimonial'),
				'class' => 'summernote',
				'placeholder' => 'Enter client testimonial here',
			);
			$this->data['name'] = array(
				'name' => 'name',
				'id' => 'name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('name'),
				'class' => 'form-control"',
				'placeholder' => 'Enter client name',
			);
			$this->data['designation'] = array(
				'name' => 'designation',
				'id' => 'designation',
				'type' => 'text',
				'value' => $this->form_validation->set_value('designation'),
				'class' => 'form-control"',
				'placeholder' => 'Enter client designation',
			);
			$this->data['company'] = array(
				'name' => 'company',
				'id' => 'company',
				'type' => 'text',
				'value' => $this->form_validation->set_value('company'),
				'class' => 'form-control"',
				'placeholder' => 'Enter company name',
			);
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'testimonials' . DIRECTORY_SEPARATOR . 'add_testimonial', $this->data);
		}
	}
	
	/**
	 * Create Testimonials
	 */
	public function edit_testimonial($id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$testimonial = $this->input->post('testimonial');
				$name = $this->input->post('name');
				$designation = $this->input->post('designation');
				$company = $this->input->post('company');
			
				// validate form input
				$this->form_validation->set_rules('testimonial','Testimonial','trim|required');
				$this->form_validation->set_rules('name','Name','trim|required');
				$this->form_validation->set_rules('designation','Designation','trim');
				$this->form_validation->set_rules('company','Comapany','trim');

				if ($this->form_validation->run() === TRUE)
				{
					$data = array(
						'comment' => $testimonial,
						'name' => $name,
						'designation' => $designation,
						'company' => $company,
					);
					$testimonial_id = $this->testimonials_model->update_testimonial($id, $data);
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect("admin/testimonials/edit_testimonial/". $testimonial_id, 'refresh');
					//$this->user_model->update_user_data($additional_data);
				}
			}
			$testimonial = $this->testimonials_model->get_testimonial_by_id($id);
			$this->data['testimonial'] = array(
				'name' => 'testimonial',
				'id' => 'testimonial',
				'type' => 'text',
				'value' => $this->form_validation->set_value('testimonial', !empty($testimonial->comment) ? $testimonial->comment : ""),
				'class' => 'summernote',
				'placeholder' => 'Enter client testimonial here',
			);
			$this->data['name'] = array(
				'name' => 'name',
				'id' => 'name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('testimonial', !empty($testimonial->name) ? $testimonial->name : ""),
				'class' => 'form-control"',
				'placeholder' => 'Enter client name',
			);
			$this->data['designation'] = array(
				'name' => 'designation',
				'id' => 'designation',
				'type' => 'text',
				'value' => $this->form_validation->set_value('testimonial', !empty($testimonial->designation) ? $testimonial->designation : ""),
				'class' => 'form-control"',
				'placeholder' => 'Enter client designation',
			);
			$this->data['company'] = array(
				'name' => 'company',
				'id' => 'company',
				'type' => 'text',
				'value' => $this->form_validation->set_value('testimonial', !empty($testimonial->company) ? $testimonial->company : ""),
				'class' => 'form-control"',
				'placeholder' => 'Enter company name',
			);
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'testimonials' . DIRECTORY_SEPARATOR . 'edit_testimonial', $this->data);
		}
	}
	
	public function delete_testimonial($id)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->testimonials_model->delete_testimonial($id);
			redirect("admin/testimonials", 'refresh');
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}
	}
}

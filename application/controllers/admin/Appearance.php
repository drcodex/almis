<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Appearance extends Backend_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('settings_model');
		$this->load->model('destinations_model');
		$this->load->model('appearance_model');
		$this->load->model('flights_model');
		$this->load->model('seo_model');

		$this->lang->load('auth');
	}
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'bookings' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
	}
	public function home()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$page = 'home';
			if ($_POST)
			{
				$destination_select = $this->input->post('destination_select');
				$title = $this->input->post('title');
				$description = $this->input->post('description');
				foreach ($destination_select as $key => $value)
				{
					$if_exist = $this->appearance_model->check_if_destination_id_exist($value);
					$if_not_exist = $this->appearance_model->check_if_destination_id_not_exist($value);
					if(!empty($if_exist))
					{
						$old[] = $if_exist;
					}
					if(empty($if_exist))
					{
						if(isset($key) && isset($value))
						{
							$new[] = array(
								'action_type' => 'home',
								'relation_value' => $value,
							);
						}
					}
				}
				if(!empty($old))
				{
					$difference = $this->relation_array_diff($if_not_exist, $old);
					foreach ($difference as $key => $value)
					{
						$valuearray[] = $value['id'];
					}
					if(!empty($valuearray))
					{
						//echo '<pre>'; print_r($valuearray); echo '</pre>'; die();
						$this->appearance_model->delete_relation_id($valuearray);
					}
				}
				if(!empty($new))
				{
					$this->db->insert_batch('relations', $new);
				}
				$data = array(
					'page' => $page,
					'title' => $title,
					'description' => $description,
				);
				$this->seo_model->add_update_seo($page, $data);
			}
			$destinations = $this->destinations_model->get_destinations();
			if(!empty($destinations)){
				foreach ($destinations as $destination) {
					$des_type = $destination['type'];
					$select_id = $destination['select_id'];
					if($type = $des_type){
						$type = $type .'_name';
						$table = $this->pluralize(2, $des_type);
						$list = $this->destinations_model->get_destinations_location($type, $table, $select_id);
					}
					$list_array[] = $list;
				}
				$destinations_list = array_replace_recursive($destinations, $list_array);
				$this->data['destinations'] = $destinations_list;
			}
			$seo = $this->seo_model->get_seo_by_page($page);
			$this->data['title'] = array(
				'name' => 'title',
				'id' => 'title',
				'type' => 'text',
				'value' => $this->form_validation->set_value('title', !empty($seo['title']) ? $seo['title'] : ""),
				'class' => 'form-control"',
				'placeholder' => 'Enter page title number',
			);
			$this->data['block_title'] = array(
				'name' => 'block_title',
				'id' => 'block_title',
				'type' => 'text',
				'value' => $this->form_validation->set_value('block_title', !empty($seo['block_title']) ? $seo['description'] : ""),
				'class' => 'form-control"',
				'placeholder' => 'Enter list title',
			);
			$this->data['description'] = array(
				'name' => 'description',
				'id' => 'description',
				'type' => 'text',
				'value' => $this->form_validation->set_value('description', !empty($seo['description']) ? $seo['description'] : ""),
				'class' => 'form-control"',
				'placeholder' => 'Enter page description number',
			);
			// set the flash data error message if there is one
			$this->data['flight_offers'] = $this->appearance_model->get_block_items($position = 'home_flight_offers');
			$this->data['lowest_fares'] = $this->appearance_model->get_block_items($position = 'home_lowest_fares');
			$this->data['terms'] = $this->appearance_model->get_terms_by_taxonomy($taxonomy = 'flights_category');
			//echo '<pre>'; print_r($this->data['terms']); echo '</pre>'; die();
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'appearance' . DIRECTORY_SEPARATOR . 'home', $this->data);
		}
	}
	
	public function menus()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data['url'] = array(
				'name' => 'url',
				'id' => 'url',
				'type' => 'url',
				'value' => $this->form_validation->set_value('url'),
				'class' => 'form-control"',
				'placeholder' => 'https://',
			);
			$this->data['url_text'] = array(
				'name' => 'url_text',
				'id' => 'url_text',
				'type' => 'text',
				'value' => $this->form_validation->set_value('url_text'),
				'class' => 'form-control"',
				'placeholder' => 'Enter link text here',
			);
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'appearance' . DIRECTORY_SEPARATOR . 'menus', $this->data);
		}
	}
	public static function pluralize($quantity, $singular, $plural=null)
	{
		if($quantity==1 || !strlen($singular)) return $singular;
		if($plural!==null) return $plural;

		$last_letter = strtolower($singular[strlen($singular)-1]);
		switch($last_letter) {
			case 'y':
				return substr($singular,0,-1).'ies';
			case 's':
				return $singular.'es';
			default:
				return $singular.'s';
		}
	}
	public function ajax_load_modal()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$modaluri = $this->input->post('modaluri');
			$this->data['block_title'] = array(
				'name' => 'block_title',
				'id' => 'block_title',
				'type' => 'text',
				'value' => $this->form_validation->set_value('block_title'),
				'class' => 'form-control"',
				'placeholder' => 'Enter list title',
			);
			$this->data['terms'] = $this->appearance_model->get_terms_by_taxonomy($taxonomy = 'flights_category');
			$this->data['flight_offers'] = $this->appearance_model->get_block_items($position = 'home_flight_offers');
			$this->data['lowest_fares'] = $this->appearance_model->get_block_items($position = 'home_lowest_fares');
			$theHTMLResponse = $this->load->view('admin/modals/'. $modaluri, $this->data);
		}
	}
	public function ajax_add_block_item()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$json_data = array('response'=>'no', 'message' =>'Failed to add this amenity');
			if($_POST)
			{
				$block_title = $this->input->post('block_title');
				$term_id = $this->input->post('term_id');
				$position = $this->input->post('position');
				$this->form_validation->set_rules('block_title','Name','trim|required');
				$this->form_validation->set_rules('term_id','Slug','trim|required');
				//echo '<pre>';print_r($block_title);echo '</pre>';
				//echo '<pre>';print_r($term_id);echo '</pre>';
				//echo '<pre>';print_r($position);echo '</pre>'; die();
				if ($this->form_validation->run() === TRUE)
				{
					$data = array(
						'position' => $position,
						'title' => $block_title,
						'term_id' => $term_id,
					);
					$last_id = $this->appearance_model->add_block($data);
					$block = $this->appearance_model->get_block_by_last_id($last_id);
					$flight_categories = $this->flights_model->get_terms();
					if($block)
					{
						$this->data['block'] = $block;
						$this->data['flight_categories'] = $flight_categories;
						$this->data['flight_offers'] = $this->appearance_model->get_block_items($position);
						$theHTMLResponse = $this->load->view('admin/ajax_parts/part_home_flight_offer_block', $this->data, true);
						$response = array('response'=>'yes','message'=>'Hajj Itinerary Added Successfully','content'=>$theHTMLResponse);
					}
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
			}
		}
	}
	public function ajax_delete_block_item()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$json_data = array('response'=>'no', 'message' =>'Failed to delete this file');
			if ($_POST)
			{
				$block_id = $this->input->post('block_id');
				$position = $this->input->post('position');
				//echo '<pre>';print_r($block_id);echo '</pre>';die();
				$this->appearance_model->delete_block_item($block_id, $position);
				$this->data['flight_offers'] = $this->appearance_model->get_block_items($position);
				$theHTMLResponse = $this->load->view('admin/ajax_parts/part_home_flight_offer_block', $this->data, true);
				$response = array('response'=>'yes','message'=>'Hajj Itinerary Added Successfully','content'=>$theHTMLResponse);
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode($response));
			}
		}
	}
	public function relation_array_diff($arraya, $arrayb) {
		foreach ($arraya as $keya => $valuea) {
			if (in_array($valuea, $arrayb)) {
				unset($arraya[$keya]);
			}
		}
		return $arraya;
	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Destinations extends Backend_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));
        $config =  array(
            'encrypt_name'    => TRUE,
			'upload_path'     => "./uploads/images/destinations/",
			'allowed_types'   => "gif|jpg|png|jpeg",
			'overwrite'       => FALSE,
			'max_size'        => "2000",
			'max_height'      => "4000",
			'max_width'       => "4000"
        );
        $this->load->library('upload', $config);
        $this->load->library('image_lib');

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('destinations_model');
		$this->load->model('seo_model');

		$this->lang->load('auth');
	}

	public function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$draft = $this->destinations_model->get_destinations_draft();
			$city_type = 'city';
			$cities = $this->destinations_model->get_cities_destinations($city_type);
			$country_type = 'country';
			$countries = $this->destinations_model->get_countries_destinations($country_type);
			$continent_type = 'continent';
			$continents = $this->destinations_model->get_continents_destinations($continent_type);
			//echo '<pre>'; print_r($continents); echo '</pre>'; die();
			//list the users
			$this->data['all_destinations'] = array_merge($draft, $cities, $countries, $continents);
			//echo '<pre>'; print_r($this->data['all_destinations']); echo '</pre>'; die();
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
	}
	
	/*
	 * Create a Select Country
	 */
	public function select_destination_type()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$select_destination_type = $this->input->post('select_destination_type');
				$this->form_validation->set_rules('select_destination_type','Type','trim|required');
				if ($this->form_validation->run() === TRUE)
				{
					redirect('admin/destinations/create_destination/'.$select_destination_type);
				}
			}
			$this->data['title'] = $this->lang->line('create_user_heading');
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'select_destination_type', $this->data);
		}
	}
	
	/*
	 * Create a new Destination
	 */
	public function create_destination($destinations_type = NULL)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$page = 'destination';
			if($_POST)
			{
				$select_id = $this->input->post('select_id');
				$featured_image_id = $this->input->post('featured_image_id');
				$destination_title = $this->input->post('destination_title');
				$replace = strtolower($this->input->post('destination_slug'));
				$destination_slug = str_replace(' ', '_', $replace);
				$destination_details = $this->input->post('destination_details');
				$title = $this->input->post('title');
				$description = $this->input->post('description');
			
				// validate form input
				$this->form_validation->set_rules('select_id','Selected Destination','trim|required');
				$this->form_validation->set_rules('destination_title','Destination Title','trim|required');
				$this->form_validation->set_rules('destination_slug','Destination Slug','trim|required|callback_convert_lowercase');
				$this->form_validation->set_rules('destination_details','Destination Details','trim');
				$this->form_validation->set_message('convert_lowercase', 'All "slug" letters must me lowercase');

				if ($this->form_validation->run() === TRUE)
				{
					$additional_data = array(
						'type' => $destinations_type,
						'select_id' => $select_id,
						'title' => $destination_title,
						'slug' => $destination_slug,
						'description' => $destination_details,
						'status' => 'Published',
						'date' => date('Y-m-d H:i:s'),
					);
					$last_id = $this->destinations_model->add_destination($additional_data);
					$destination_id = $last_id;
					if(!empty($featured_image_id))
					{
						$other_data = array(
							'destination_id' => $last_id,
						);
						$this->destinations_model->update_featured_image_destination_id($featured_image_id, $other_data);
					}
				}
				$data = array(
					'page_id' => $destination_id,
					'page' => $page,
					'title' => $title,
					'description' => $description,
				);
				$this->seo_model->add_update_seo_by_page_id($page, $destination_id, $data);
				redirect('admin/destinations', 'refresh');
			}
			$this->data['title'] = array(
				'name' => 'title',
				'id' => 'title',
				'type' => 'text',
				'value' => $this->form_validation->set_value('title'),
				'class' => 'form-control"',
				'placeholder' => 'Enter seo title',
			);
			$this->data['description'] = array(
				'name' => 'description',
				'id' => 'description',
				'value' => $this->form_validation->set_value('description'),
				'class' => 'form-control"',
				'placeholder' => 'Enter seo description',
			);
			$this->data['destination_title'] = array(
				'name' => 'destination_title',
				'id' => 'destination_title',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_title'),
				'class' => 'form-control"',
				'placeholder' => 'Add Title',
			);
			$this->data['destination_slug'] = array(
				'name' => 'destination_slug',
				'id' => 'destination_slug',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_slug'),
				'class' => 'form-control"',
				'placeholder' => 'Add Slug',
			);
			$this->data['destination_details'] = array(
				'name' => 'destination_details',
				'id' => 'destination_details',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_details'),
				'class' => 'summernote',
				'placeholder' => 'Start adding text or html here',
			);
			switch ($destinations_type) {
				case "city":
					$this->data['type'] = 'city';
					$this->data['destination_list'] = $this->destinations_model->get_cities_convent();
					break;
				case "country":
					$this->data['type'] = 'country';
					$this->data['destination_list'] = $this->destinations_model->get_countries_convent();
					break;
				case "continent":
					$this->data['type'] = 'continent';
					$this->data['destination_list'] = $this->destinations_model->get_continents_convent();
					break;
				default:
					echo "Your favorite color is neither red, blue, nor green!";
			}
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'create_destination', $this->data);
		}
	}
	public function edit_destination($destination_id = NULL)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$page = 'destination';
			$destinations = $this->destinations_model->get_destination_by_id($destination_id);
			if(!empty($destinations))
			{
				if($_POST)
				{
					$select_id = $this->input->post('select_id');
					$destination_title = $this->input->post('destination_title');
					$destination_slug = $this->input->post('destination_slug');
					$destination_details = $this->input->post('destination_details');
					$status = $this->input->post('status');
					$title = $this->input->post('title');
					$description = $this->input->post('description');
				
					$this->form_validation->set_rules('select_id','Selected Destination','trim|required');
					$this->form_validation->set_rules('destination_title','Destination Title','trim|required');
					$this->form_validation->set_rules('destination_slug','Destination Slug','trim|required');
					$this->form_validation->set_rules('destination_details','Destination Details','trim');

					if ($this->form_validation->run() === TRUE)
					{
						$additional_data = array(
							'select_id' => $select_id,
							'title' => $destination_title,
							'slug' => $destination_slug,
							'description' => $destination_details,
							'status' => $status,
							'date' => date('Y-m-d H:i:s'),
						);
						//echo '<pre>'; print_r($additional_data); echo '</pre>'; die();
						$this->destinations_model->update_destination($destination_id, $additional_data);
						$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					}
					
					$data = array(
						'page_id' => $destination_id,
						'page' => $page,
						'title' => $title,
						'description' => $description,
					);
					$this->seo_model->add_update_seo_by_page_id($page, $destination_id, $data);
					redirect("admin/destinations/edit_destination/" . $destination_id, 'refresh');
				}
				$seo = $this->seo_model->get_seo_by_page_id($page, $destination_id);
				$dest = $this->destinations_model->get_destination_by_id($destination_id);
				$this->data['title'] = array(
					'name' => 'title',
					'id' => 'title',
					'type' => 'text',
					'value' => $this->form_validation->set_value('title', !empty($seo['title']) ? $seo['title'] : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter page title number',
				);
				$this->data['description'] = array(
					'name' => 'description',
					'id' => 'description',
					'type' => 'text',
					'value' => $this->form_validation->set_value('description', !empty($seo['description']) ? $seo['description'] : ""),
					'class' => 'form-control"',
					'placeholder' => 'Enter page description number',
				);
				$this->data['destination_title'] = array(
					'name' => 'destination_title',
					'id' => 'destination_title',
					'type' => 'text',
					'value' => $this->form_validation->set_value('destination_title', !empty($dest->title) ? $dest->title : ""),
					'class' => 'form-control"',
					'placeholder' => 'Add Title',
				);
				$this->data['destination_slug'] = array(
					'name' => 'destination_slug',
					'id' => 'destination_slug',
					'type' => 'text',
					'value' => $this->form_validation->set_value('destination_slug', !empty($dest->slug) ? $dest->slug : ""),
					'class' => 'form-control"',
					'placeholder' => 'Add Slug',
				);
				$this->data['destination_details'] = array(
					'name' => 'destination_details',
					'id' => 'destination_details',
					'type' => 'text',
					'value' => $this->form_validation->set_value('destination_details', !empty($dest->description) ? $dest->description : ""),
					'class' => 'summernote',
					'placeholder' => 'Start adding text or html here',
				);
				$destinations_select = $destinations->select_id;
				$destinations_type = $destinations->type;
				switch ($destinations_type) {
					case "city":
						$this->data['location'] = $this->destinations_model->get_city_by_id($destinations_select);
						$this->data['destination_list'] = $this->destinations_model->get_cities_convent();
						break;
					case "country":
						$this->data['location'] = $this->destinations_model->get_country_by_id($destinations_select);
						$this->data['destination_list'] = $this->destinations_model->get_countries_convent();
						break;
					case "continent":
						$this->data['location'] = $this->destinations_model->get_continent_by_id($destinations_select);
						$this->data['destination_list'] = $this->destinations_model->get_continents_convent();
						break;
				}
				$destination_image = $this->destinations_model->get_destination_image($destination_id);
				$this->data['destination_image'] = $destination_image;
				$this->data['destination'] = $dest;
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
				$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'edit_destination', $this->data);
			}
			else
			{
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->_render_page('errors' . DIRECTORY_SEPARATOR . '404', $this->data);
			}
		}
	}
	
	public function delete_destination($id)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->destinations_model->delete_destination($id);
			$this->destinations_model->delete_relations($id);
			redirect("admin/destinations", 'refresh');
		}
	}
    public function upload_file()
    {
		$response =  array("response"=>"no","content"=>"");
        $config['upload_path'] = './uploads/images/destinations/';

        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('file'))
		{
			$theHTMLResponse = '';
			$message = 'Upload failed. File size (max limit: 2mb) or file resolution too high.';
			$response = array('response'=>'yes', 'response_type'=>'error', 'content'=>$theHTMLResponse, 'message'=>$message);
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($response));
		}
		else
        {
            $file = $this->upload->data();
            $thumb_config["image_library"] = "gd2";
            $thumb_config["source_image"] = $file["full_path"];
            $thumb_config['new_image'] = 'uploads/images/destinations/thumb/'.$file['file_name'];
            $thumb_config['create_thumb'] = TRUE;
            $thumb_config['thumb_marker'] = '';
            $thumb_config['maintain_ratio'] = TRUE;
            $thumb_config['width'] = 1140;
            //$thumb_config['height'] = 350;
            $this->load->library('image_lib', $thumb_config);
            $this->image_lib->initialize($thumb_config);
            $this->image_lib->resize();
            $this->image_lib->clear();
			$destination_id = $this->input->post('destination_id');
            $data = array (
                'title' => $file['orig_name'],
                'path' => 'uploads/images/destinations/'.$file['file_name'],
                'ext' => $file['file_ext'],
                'thumb' => $thumb_config['new_image'],
                'size' => $file['file_size'],
	            'destination_id' => $destination_id,
	            'created_at' => date('Y-m-d h:i:s')
            );
			$last_id = $this->destinations_model->upload_destination_featured_image($destination_id, $data);
			//echo '<pre>'; print_r($last_id); echo '</pre>'; die();
			$destination_image = $this->destinations_model->get_destination_image_by_last_id($last_id);
			$this->data['destination_id'] = $last_id;
			$this->data['package_image'] = $destination_image;
			$theHTMLResponse = $this->load->view('admin/destinations/ajax_parts/part_destination_id', $this->data, true);
			$message = 'Destination featured image has been uploaded';
			$response = array('response'=>'yes','response_type'=>'custom','content'=>$theHTMLResponse, 'message'=>$message);
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($response));
        }
    }
	public function convert_lowercase() {
		return strtolower($this->input->post('destination_slug'));
	}
}

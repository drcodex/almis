<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends Backend_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
        $config =  array(
            'encrypt_name'    => TRUE,
			'upload_path'     => "./uploads/images/packages/galleries/",
			'allowed_types'   => "gif|jpg|png|jpeg",
			'overwrite'       => FALSE,
			'max_size'        => "2000",
			'max_height'      => "2024",
			'max_width'       => "2024"
        );
        $this->load->library('upload', $config);
        $this->load->library('image_lib');
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('packages_model');
		$this->load->model('destinations_model');
		$this->load->model('appearance_model');
		$this->load->model('flights_model');

		$this->lang->load('auth');
	}
	public function update_description()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$json_data = array('response'=>'no', 'message' =>'Failed to add this description');
			if($_POST)
			{
				$description = $this->input->post('description');
				$package_id = $this->input->post('package_id');
				$key = $this->input->post('key');
				
				$this->form_validation->set_rules('description','Description','trim');
				if ($this->form_validation->run() === TRUE)
				{
					$description_exist = $this->packages_model->get_package_meta_by_key_row($package_id, $key);
					if (!empty($description_exist))
					{
						$description_id = $description_exist->pmeta_id;
						$data = array(
							'pmeta_id' => $description_id,
							'package_id' => $package_id,
							'meta_key' => $key,
							'meta_value' => $description,
						);
						$last_id = $this->packages_model->update_description($description_id, $data);
					}
					else
					{
						$data = array(
							'package_id' => $package_id,
							'meta_key' => $key,
							'meta_value' => $description,
						);
						$last_id = $this->packages_model->add_description($data);
					}
					$response = array('response'=>'yes','message'=>'Hajj Package Description Updated Successfully');
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
			}
		}
	}
	public function add_feature()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$json_data = array('response'=>'no', 'message' =>'Failed to add this feature');
			if($_POST)
			{
				$feature = $this->input->post('feature');
				$package_id = $this->input->post('package_id');
				$this->form_validation->set_rules('feature','Feature','trim|required');
				if ($this->form_validation->run() === TRUE)
				{
					$data = array(
						array(
							'package_id' => $package_id,
							'meta_key' => 'feature',
							'meta_value' => $feature,
						),
					);
					$key = 'feature';
					$last_id = $this->packages_model->add_feature($data);
					$features = $this->packages_model->get_package_meta_by_key_result($package_id, $key);
					//echo '<pre>';print_r($features);echo '</pre>';die();
					$hajj_feature = $this->packages_model->get_last_feature($last_id);
					if($hajj_feature)
					{
						$this->data['features'] = $features;
						$this->data['hajj_feature'] = $hajj_feature;
						$theHTMLResponse = $this->load->view('admin/packages/travel/ajax_parts/part_feature', $this->data, true);
						$response = array('response'=>'yes','message'=>'Hajj Package Feature Added Successfully','content'=>$theHTMLResponse);
					}
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
			}
		}
	}
	public function delete_feature()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$json_data = array('response'=>'no', 'message' =>'Failed to delete this file');
			if ($_POST)
			{
				$feature_id = $this->input->post('feature_id');
				$package_id = $this->input->post('package_id');
				$this->packages_model->delete_feature($feature_id);
				$key = 'feature';
				$features = $this->packages_model->get_package_meta_by_key_result($package_id, $key);
				$this->data['features'] = $features;
				$theHTMLResponse = $this->load->view('admin/packages/travel/ajax_parts/part_feature', $this->data, true);
				$json_data = array('response'=>'yes','message'=>'Hajj Package Feature Deleted Successfully','content'=> $theHTMLResponse);
			}
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($json_data));
		}
	}
	public function add_amenity()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$json_data = array('response'=>'no', 'message' =>'Failed to add this amenity');
			if($_POST)
			{
				$amenity = $this->input->post('amenity');
				$package_id = $this->input->post('package_id');
				$this->form_validation->set_rules('amenity','Amenity','trim|required');
				if ($this->form_validation->run() === TRUE)
				{
					$data = array(
						array(
							'package_id' => $package_id,
							'meta_key' => 'amenity',
							'meta_value' => $amenity,
						),
					);
					$key = 'amenity';
					$last_id = $this->packages_model->add_amenity($data);
					$amenities = $this->packages_model->get_amenities($package_id, $key);
					//echo '<pre>';print_r($features);echo '</pre>';die();
					$hajj_amenity = $this->packages_model->get_package_meta_last_id($last_id);
					if($hajj_amenity)
					{
						$this->data['amenities'] = $amenities;
						$this->data['hajj_amenity'] = $hajj_amenity;
						$theHTMLResponse = $this->load->view('admin/packages/travel/ajax_parts/part_amenity', $this->data, true);
						$response = array('response'=>'yes','message'=>'Hajj Package Amenity Added Successfully','content'=>$theHTMLResponse);
					}
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
			}
		}
	}
	public function delete_amenity()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$json_data = array('response'=>'no', 'message' =>'Failed to delete this file');
			if ($_POST)
			{
				$amenity_id = $this->input->post('amenity_id');
				$package_id = $this->input->post('package_id');
				$this->packages_model->delete_amenity($amenity_id);
				$key = 'amenity';
				$amenities = $this->packages_model->get_amenities($package_id, $key);
				$this->data['amenities'] = $amenities;
				$theHTMLResponse = $this->load->view('admin/packages/travel/ajax_parts/part_amenity', $this->data, true);
				$json_data = array('response'=>'yes','message'=>'Hajj Package Feature Deleted Successfully','content'=> $theHTMLResponse);
			}
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($json_data));
		}
	}
	public function add_itinerary()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$json_data = array('response'=>'no', 'message' =>'Failed to add this amenity');
			if($_POST)
			{
				$day = $this->input->post('day');
				$itinerary_title = $this->input->post('itinerary_title');
				$itinerary_detail = $this->input->post('itinerary_detail');
				$type = $this->input->post('type');
				$package_id = $this->input->post('package_id');
				$this->form_validation->set_rules('day','Day','trim|required');
				$this->form_validation->set_rules('itinerary_title','Title','trim|required');
				$this->form_validation->set_rules('itinerary_detail','Detail','trim|required');
				$this->form_validation->set_rules('type','Type','trim|required');
				$this->form_validation->set_rules('package_id','Package','trim|required');
				if ($this->form_validation->run() === TRUE)
				{
					$data = array(
						array(
							'day' => $day,
							'title' => $itinerary_title,
							'detail' => $itinerary_detail,
							'type' => $type,
							'package_id' => $package_id,
						),
					);
					$last_id = $this->packages_model->add_itinerary($data);
					$itinerary = $this->packages_model->get_itinerary($package_id, $type);
					$last_itinerary = $this->packages_model->get_package_meta_last_id($last_id);
					//echo '<pre>';print_r($data);echo '</pre>';die();
					if($last_itinerary)
					{
						$this->data['itinerary'] = $itinerary;
						$this->data['last_itinerary'] = $last_itinerary;
						$theHTMLResponse = $this->load->view('admin/packages/travel/ajax_parts/part_itinerary', $this->data, true);
						$response = array('response'=>'yes','message'=>'Hajj Itinerary Added Successfully','content'=>$theHTMLResponse);
					}
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
			}
		}
	}
	public function delete_itinerary()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$json_data = array('response'=>'no', 'message' =>'Failed to delete this file');
			if ($_POST)
			{
				$itinerary_id = $this->input->post('itinerary_id');
				$type = $this->input->post('type');
				$package_id = $this->input->post('package_id');
				$this->packages_model->delete_itinerary($itinerary_id);
				$itinerary = $this->packages_model->get_itinerary($package_id, $type);
				$this->data['itinerary'] = $itinerary;
				$theHTMLResponse = $this->load->view('admin/packages/travel/ajax_parts/part_itinerary', $this->data, true);
				$json_data = array('response'=>'yes','message'=>'Hajj Package Itinerary Day Deleted Successfully','content'=> $theHTMLResponse);
			}
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($json_data));
		}
	}
	public function update_location()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$json_data = array('response'=>'no', 'message' =>'Failed to add this location');
			if($_POST)
			{
				$location = $this->input->post('location');
				$package_id = $this->input->post('package_id');
				$key = $this->input->post('key');
				
				$this->form_validation->set_rules('location','Location','trim');
				if ($this->form_validation->run() === TRUE)
				{
					$location_exist = $this->packages_model->get_package_meta_by_key_row($package_id, $key);
					if (!empty($location_exist))
					{
						$location_id = $location_exist->pmeta_id;
						$data = array(
							'pmeta_id' => $location_id,
							'package_id' => $package_id,
							'meta_key' => $key,
							'meta_value' => $location,
						);
						$last_id = $this->packages_model->update_location($location_id, $data);
					}
					else
					{
						$data = array(
							'package_id' => $package_id,
							'meta_key' => $key,
							'meta_value' => $location,
						);
						$last_id = $this->packages_model->add_location($data);
					}
					$response = array('response'=>'yes','message'=>'Hajj Package Location Updated Successfully');
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
			}
		}
	}
	public function gallery_image_upload()
    {
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$response =  array("response"=>"no","content"=>"");
			$config['upload_path'] = './uploads/images/packages/galleries/';

			$this->load->library('upload', $config);
			if(!$this->upload->do_upload('file'))
			{
				$theHTMLResponse = '';
				$message = 'Upload failed. File size (max limit: 2mb) or file resolution too high.';
				$response = array('response'=>'yes', 'response_type'=>'error', 'content'=>$theHTMLResponse, 'message'=>$message);
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode($response));
			}
			else
			{
				$file = $this->upload->data();
				$thumb_config["image_library"] = "gd2";
				$thumb_config["source_image"] = $file["full_path"];
				$thumb_config['new_image'] = 'uploads/images/packages/galleries/thumb/'.$file['file_name'];
				$thumb_config['create_thumb'] = TRUE;
				$thumb_config['thumb_marker'] = '';
				$thumb_config['maintain_ratio'] = TRUE;
				$thumb_config['width'] = 550;
				$thumb_config['height'] = 350;
				$this->load->library('image_lib', $thumb_config);
				$this->image_lib->initialize($thumb_config);
				$this->image_lib->resize();
				$this->image_lib->clear();
				$package_id = $this->input->post('package_id'); //if uploaded via package edit
				$type = $this->input->post('type');
				if(!empty($package_id) && !empty($type))
				{
					$package_image = $this->packages_model->get_package_featured_image_by_id($package_id, $type);
					if(!empty($package_image))
					{
						$path = $package_image->path;
						$thumb =$package_image->thumb;
						$this->packages_model->delete_featured_image($package_id, $path, $thumb);
					}
				}
				$data = array (
					'type' => $type,
					'title' => $file['orig_name'],
					'path' => 'uploads/images/packages/galleries/'.$file['file_name'],
					'ext' => $file['file_ext'],
					'thumb' => $thumb_config['new_image'],
					'size' => $file['file_size'],
					'package_id' => $package_id,
					'created_at' => date('Y-m-d h:i:s')
				);
				if($type == 'featured')
				{
					$last_id = $this->packages_model->upload_package_featured_image($package_id, $data);
					$package_images = $this->packages_model->get_package_images_by_package_id($package_id, $type);
					$package_image = $this->packages_model->get_package_image_by_last_id($last_id);
					$this->data['package_id'] = $last_id;
					$this->data['images'] = $package_images;
					$this->data['package_image'] = $package_image;
					$theHTMLResponse = $this->load->view('admin/packages/travel/ajax_parts/part_package_id', $this->data, true);
					$message = 'Package featured image has been uploaded';
					$response = array('response'=>'yes','response_type'=>'custom','content'=>$theHTMLResponse, 'message'=>$message);
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
				elseif($type == 'gallery')
				{
					$last_id = $this->packages_model->upload_package_images($package_id, $data);
					$package_images = $this->packages_model->get_package_images_by_package_id($package_id, $type);
					$package_image = $this->packages_model->get_package_image_by_last_id($last_id);
					$this->data['images'] = $package_images;
					$this->data['package_image'] = $package_image;
					$theHTMLResponse = $this->load->view('admin/packages/travel/ajax_parts/thumbnails', $this->data, true);
					$message = 'Package gallery image has been uploaded';
					$response = array('response'=>'yes','response_type'=>'custom','content'=>$theHTMLResponse, 'message'=>$message);
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
				else
				{
					$theHTMLResponse = '';
					$response = array('response'=>'no','content'=>$theHTMLResponse);
				}
			}
		}
    }
	public function delete_gallery_image()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');	
		}
		else
		{
			$json_data = array('response'=>'no', 'message' =>'Failed to delete this file');
			if ($_POST)
			{
				$image_id = $this->input->post('image_id');
				$type = $this->input->post('type');
				$package_id = $this->input->post('package_id');
				$gallery_image = $this->packages_model->get_package_gallery_image_by_id($image_id, $type);
				$path = $gallery_image->path;
				$thumb =$gallery_image->thumb;
				//echo '<pre>'; print_r($path); echo '</pre>'; die();
				$this->packages_model->delete_gallery_image($image_id, $path, $thumb);
				$package_images = $this->packages_model->get_package_images_by_package_id($package_id, $type);
				$this->data['images'] = $package_images;
				$theHTMLResponse = $this->load->view('admin/packages/travel/ajax_parts/thumbnails', $this->data, true);
				$json_data = array('response'=>'yes','message'=>'Gallery Image Deleted Successfully','content'=> $theHTMLResponse);
			}
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($json_data));
		}
	}
	public function change_featured()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$response =  array("response"=>"no","content"=>"");
				$featured_id = $this->input->post('featured');
				$destination = $this->destinations_model->get_destination_by_id($featured_id);
				//echo '<pre>'; print_r($destination); echo '</pre>'; die();
				$destination_id = $destination->id;
				$title = $destination->title;
				if($destination->featured == '0')
				{
					$additional_data = array(
						'featured' => '1',
					);
					$this->destinations_model->update_destination($destination_id, $additional_data);
					//echo '<pre>'; print_r($change); echo '</pre>'; die();
					$theHTMLResponse = 'enabled';
					$response = array('response'=>'yes', 'message'=> 'Enabled '.$title.' as featured', 'content'=>$theHTMLResponse);
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
				else
				{
					$additional_data = array(
						'featured' => '0',
					);
					$this->destinations_model->update_destination($destination_id, $additional_data);
					$theHTMLResponse = 'desabled';
					$response = array('response'=>'yes', 'message'=> 'Disabled '.$title.' as featured', 'content'=>$theHTMLResponse);
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
			}
		}
	}
	public function add_flight_category()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$json_data = array('response'=>'no', 'message' =>'Failed to add this amenity');
			if($_POST)
			{
				$flight_category_name = $this->input->post('flight_category_name');
				$flight_category_slug = $this->input->post('flight_category_slug');
				$this->form_validation->set_rules('flight_category_name','Name','trim|required');
				$this->form_validation->set_rules('flight_category_slug','Slug','trim|required');
				if ($this->form_validation->run() === TRUE)
				{
					$data = array(
						'name' => $flight_category_name,
						'slug' => $flight_category_slug,
						'term_group' => 0,
					);
					$last_id = $this->flights_model->add_term($data);
					$other_data = array(
						'term_id' => $last_id,
						'taxonomy' => 'flights_category',
						'description' => '',
						'parent' => '',
						'count' => '',
					);
					$this->flights_model->add_term_taxonomy($other_data);
					$last_flight_category = $this->flights_model->get_flight_category_last_id($last_id);
					$flight_categories = $this->flights_model->get_terms();
					//echo '<pre>';print_r($data);echo '</pre>';die();
					if($last_flight_category)
					{
						$this->data['flight_categories'] = $flight_categories;
						$this->data['last_flight_category'] = $last_flight_category;
						$theHTMLResponse = $this->load->view('admin/ajax_parts/part_flight_category', $this->data, true);
						$response = array('response'=>'yes','message'=>'Hajj Itinerary Added Successfully','content'=>$theHTMLResponse);
					}
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
			}
		}
	}
	public function delete_flight_category()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$json_data = array('response'=>'no', 'message' =>'Failed to delete this file');
			if ($_POST)
			{
				$term_id = $this->input->post('flight_category_id');
				$term_by_taxonomy_id = $this->flights_model->get_term_by_taxonomy_id($term_id);
				//echo '<pre>';print_r($term_by_taxonomy_id);echo '</pre>';die();
				$this->flights_model->delete_term($term_by_taxonomy_id->term_id);
				$this->flights_model->delete_term_taxonomy($term_id);
				$this->flights_model->delete_term_relationships_by_term_taxonomy_is($term_id);
				$flight_categories = $this->flights_model->get_terms();
				$this->data['flight_categories'] = $flight_categories;
				$theHTMLResponse = $this->load->view('admin/ajax_parts/part_flight_category', $this->data, true);
				$json_data = array('response'=>'yes','message'=>'Hajj Package Feature Deleted Successfully','content'=> $theHTMLResponse);
			}
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($json_data));
		}
	}
	
	public function add_block()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$json_data = array('response'=>'no', 'message' =>'Failed to add this amenity');
			if($_POST)
			{
				$block_title = $this->input->post('block_title');
				$term_id = $this->input->post('term_id');
				$this->form_validation->set_rules('block_title','Name','trim|required');
				$this->form_validation->set_rules('term_id','Slug','trim|required');
				if ($this->form_validation->run() === TRUE)
				{
					$data = array(
						'position' => 'home_flight_offers',
						'title' => $block_title,
						'term_id' => $term_id,
					);
					$last_id = $this->appearance_model->add_block($data);
					$block = $this->appearance_model->get_block_by_last_id($last_id);
					$flight_categories = $this->flights_model->get_terms();
					//echo '<pre>';print_r($data);echo '</pre>';die();
					if($block)
					{
						$this->data['block'] = $block;
						$this->data['flight_categories'] = $flight_categories;
						$this->data['block_items'] = $this->appearance_model->get_block_items();
						$theHTMLResponse = $this->load->view('admin/ajax_parts/part_home_flight_offer_block', $this->data, true);
						$response = array('response'=>'yes','message'=>'Hajj Itinerary Added Successfully','content'=>$theHTMLResponse);
					}
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
			}
		}
	}
}

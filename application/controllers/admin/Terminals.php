<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Terminals extends Backend_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('terminals_model');

		$this->lang->load('auth');
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['class'] = $this->session->flashdata('class');
			$this->data['all_terminals'] = $this->terminals_model->get_terminals();
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'terminals' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
	}
	
	/**
	 * Create a new News
	 */
	public function new_terminal()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$city_country = $this->input->post('city_country');
				$terminal_name = $this->input->post('terminal_name');
				$terminal_short_code = $this->input->post('terminal_short_code');
				
				// validate form input
				$this->form_validation->set_rules('city_country','City','trim|required');
				$this->form_validation->set_rules('terminal_name','Terminal Name','trim|required');
				$this->form_validation->set_rules('terminal_short_code','Terminal Short Code','trim|required');

				if ($this->form_validation->run() === TRUE)
				{
					$result_explode = explode('|', $city_country);
					$city_id = $result_explode[0];
					$country_id = $result_explode[1];
					$session_data = $this->session->userdata;
					$userID = $session_data['user_id'];
					$additional_data = array(
						'city_id' => $city_id,
						'country_id' => $country_id,
						'terminal_name' => $terminal_name,
						'terminal_short_code' => $terminal_short_code,
					);
					//echo '<pre>'; print_r($additional_data); echo '</pre>'; die('YES');
					$this->terminals_model->add_terminal($additional_data);
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect("admin/terminals/new_terminal", 'refresh');
					//$this->user_model->update_user_data($additional_data);
				}
			}
			$this->data['terminal_name'] = array(
				'name' => 'terminal_name',
				'id' => 'terminal_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('terminal_name'),
				'class' => 'form-control"',
				'placeholder' => 'Enter terminal name',
			);	
			$this->data['terminal_short_code'] = array(
				'name' => 'terminal_short_code',
				'id' => 'terminal_short_code',
				'type' => 'text',
				'value' => $this->form_validation->set_value('terminal_short_code'),
				'class' => 'form-control"',
				'placeholder' => 'Enter short code here',
			);
			$this->data['terminal_short_name'] = array(
				'name' => 'terminal_short_name',
				'id' => 'terminal_short_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('terminal_short_name'),
				'class' => 'form-control"',
				'placeholder' => 'Enter short name for new terminal',
			);
			
			$this->data['title'] = $this->lang->line('create_user_heading');
			$this->data['all_cities'] = $this->terminals_model->get_all_cities();
			//echo '<pre>'; print_r($this->data['all_cities']); echo '</pre>'; die('YES');
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'terminals' . DIRECTORY_SEPARATOR . 'new_terminal', $this->data);
		}
	}
	
	public function edit_terminal($terminal_id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$terminal_name = $this->input->post('terminal_name');
				$terminal_short_code = $this->input->post('terminal_short_code');
				$city_country = $this->input->post('city_country');
				
				// validate form input
				$this->form_validation->set_rules('city_country','City','trim|required');
				$this->form_validation->set_rules('terminal_name','Terminal Name','trim|required');
				$this->form_validation->set_rules('terminal_short_code','Terminal Short Code','trim|required');

				if ($this->form_validation->run() === TRUE)
				{
					$result_explode = explode('|', $city_country);
					$city_id = $result_explode[0];
					$country_id = $result_explode[1];
					$additional_data = array(
						'city_id' => $city_id,
						'country_id' => $country_id,
						'terminal_name' => $terminal_name,
						'terminal_short_code' => $terminal_short_code,
					);
					$this->terminals_model->update_terminal($terminal_id, $additional_data);
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect("admin/terminals/edit_terminal/" . $terminal_id, 'refresh');
				}
			}
			$single_terminal = (array)$this->terminals_model->get_single_terminal($terminal_id);
			$this->data['terminal_name'] = array(
				'name' => 'terminal_name',
				'id' => 'terminal_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('terminal_name', !empty($single_terminal['terminal_name']) ? $single_terminal['terminal_name'] : ""),
				'class' => 'form-control"',
				'placeholder' => 'Enter enter name for new terminal',
			);
			$this->data['terminal_short_code'] = array(
				'name' => 'terminal_short_code',
				'id' => 'terminal_short_code',
				'type' => 'text',
				'value' => $this->form_validation->set_value('terminal_short_code', !empty($single_terminal['terminal_short_code']) ? $single_terminal['terminal_short_code'] : ""),
				'class' => 'form-control"',
				'placeholder' => 'Edit short code for new terminal',
			);
			$this->data['city_country'] = array(
				'name' => 'city_country',
				'id' => 'city_country',
				'type' => 'text',
				'value' => $this->form_validation->set_value('city_country', !empty($single_terminal['city_country']) ? $single_terminal['city_country'] : ""),
				'class' => 'form-control"',
				'placeholder' => 'Enter short name for new terminal',
			);
			$this->data['all_cities'] = $this->terminals_model->get_all_cities();
			//echo '<pre>'; print_r($this->data['all_cities']); echo '</pre>'; die();
			$this->data['single_terminal'] = $single_terminal;
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'terminals' . DIRECTORY_SEPARATOR . 'edit_terminal', $this->data);
		}
	}
	
	public function delete_terminal($terminal_id)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$exist_in_flights_from = $this->terminals_model->check_terminal_exist_in_flights_from($terminal_id);
			$exist_in_flights_to = $this->terminals_model->check_terminal_exist_in_flights_to($terminal_id);
			$check_terminal_exist = array_replace_recursive($exist_in_flights_from, $exist_in_flights_to);
			//echo '<pre>'; print_r($check_terminal_exist); echo '</pre>'; die();
			$count_terminal = count($check_terminal_exist);
			if($count_terminal == 0)
			{
				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->terminals_model->delete_terminal($terminal_id);
				//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
				redirect('admin/terminals');
			}
			else
			{
				if($count_terminal > 1)
				{
					$this->session->set_flashdata('message', 'This terminal is linked with '.$count_terminal.' flights, Unlink first to delete this terminals');
					$this->session->set_flashdata('class', 'btn btn-danger waves-effect waves-light');
					redirect('admin/terminals');
				}
				else
				{
					$this->session->set_flashdata('message', 'This terminal is linked with 1 flight, Unlink first to delete this terminal');
					$this->session->set_flashdata('class', 'btn btn-danger waves-effect waves-light');
					redirect('admin/terminals');
				}
			}
		}
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['posts'] = $this->terminals_model->delete_terminal($terminal_id);
			redirect("admin/terminals/all_terminals/", 'refresh');
		}
	}
}

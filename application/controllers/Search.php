<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends Frontend_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('flights_model');
		$this->load->model('home_model');

		$this->lang->load('auth');
	}
	public function index()
	{
		if($_POST)
		{
			$flight_from = $this->input->post('flight_from');
			$flight_to = $this->input->post('flight_to');
			$flight_by_from_name = $this->home_model->get_flight_by_from_name($flight_from);
			$flight_by_to_name = $this->home_model->get_flight_by_to_name($flight_to);
			if(count($flight_by_from_name) == 0 || count($flight_by_to_name) == 0)
			{
				redirect('/');
			}
			else
			{
				$from_short_code = $flight_by_from_name->terminal_short_code;
				$to_short_code = $flight_by_to_name->terminal_short_code;
				redirect("flights/route/".$from_short_code."/".$to_short_code);
			}
		}
		else
		{
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('front' . DIRECTORY_SEPARATOR . 'home', $this->data);
		}
	}
	
	public function flight_route($flight_from, $flight_to)
	{
		$from = $this->flights_model->get_flight_from_city_by_code($flight_from);
		$to = $this->flights_model->get_flight_to_city_by_code($flight_to);
		$from_id = $from->id;
		$to_id = $to->id;
		$destination_flights = $this->flights_model->destination_flights($from_id, $to_id);
		if(count($destination_flights) == 0)
		{
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('front' . DIRECTORY_SEPARATOR . 'bookings' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
		else
		{
			if ($destination_flights) {
				$flight_id = array();
				for ($i = 0; $i < count($destination_flights); $i++) {
						$flight_id[] = $destination_flights[$i]['id'];
				}
			}
			//echo '<pre>'; print_r($flight_id); echo '</pre>'; die();
			$new_array = [];
			if(!empty($flight_id)){
				foreach ($flight_id as $value) {
					$get_flights_from = $this->flights_model->destination_flights_from($value, $from_id);
					$get_flights_to = $this->flights_model->destination_flights_to($value, $to_id);
					$tempArr['flight_from'] = !empty($get_flights_from['flight_from']) ? $get_flights_from['flight_from'] : NULL;
					$tempArr['flight_from_code'] = !empty($get_flights_from['flight_from_code']) ? $get_flights_from['flight_from_code'] : NULL;
					$tempArr['flight_to'] = !empty($get_flights_to['flight_to']) ? $get_flights_to['flight_to'] : NULL;
					$tempArr['flight_to_code'] = !empty($get_flights_to['flight_to_code']) ? $get_flights_to['flight_to_code'] : NULL;
					$new_array[] = $tempArr;
				}
			}
			//echo '<pre>'; print_r($new_array); echo '</pre>'; die();
			$all_routes= array_replace_recursive($destination_flights, $new_array);
			$this->data['all_routes'] = $all_routes;
			//echo '<pre>'; print_r($all_routes); echo '</pre>'; die();
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('front' . DIRECTORY_SEPARATOR . 'routes', $this->data);
		}
	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Packages extends Frontend_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
        $config =  array(
            'encrypt_name'    => TRUE,
			'upload_path'     => "./uploads/images/packages/galleries/",
			'allowed_types'   => "gif|jpg|png|jpeg",
			'overwrite'       => FALSE,
			'max_size'        => "2000",
			'max_height'      => "2024",
			'max_width'       => "2024"
        );
        $this->load->library('upload', $config);
        $this->load->library('image_lib');
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('packages_model');
		$this->load->model('seo_model');


		$this->lang->load('auth');
	}
	public function index()
	{
		$image_type = 'featured';
		$status = 'Published';
		$type = $this->uri->segment(2);
		$packages = $this->packages_model->get_packages_all($status);
		if(!empty($packages))
		{
			$package_id = array();
			for ($i = 0; $i < count($packages); $i++) {
				$package_id[] = $packages[$i]['id'];
			}
			$new_array = [];
			if(!empty($package_id)){
				foreach ($package_id as $id) {
					$package = (array)$this->packages_model->get_package_by_id_no_type($id);
					$image = (array)$this->packages_model->get_package_featured_image_by_id($id, $image_type);
					$tempArr['id'] = !empty($package['id']) ? $package['id'] : NULL;
					$tempArr['type'] = !empty($package['type']) ? $package['type'] : NULL;
					$tempArr['title'] = !empty($package['title']) ? $package['title'] : NULL;
					$tempArr['sub_title'] = !empty($package['sub_title']) ? $package['sub_title'] : NULL;
					$tempArr['price'] = !empty($package['price']) ? $package['price'] : NULL;
					$tempArr['sale_price'] = !empty($package['sale_price']) ? $package['sale_price'] : NULL;
					$tempArr['no_of_days'] = !empty($package['no_of_days']) ? $package['no_of_days'] : NULL;
					$tempArr['departure_date'] = !empty($package['departure_date']) ? $package['departure_date'] : NULL;
					$tempArr['return_date'] = !empty($package['return_date']) ? $package['return_date'] : NULL;
					$tempArr['status'] = !empty($package['status']) ? $package['status'] : NULL;
					$tempArr['date'] = !empty($package['date']) ? $package['date'] : NULL;
					$tempArr['path'] = !empty($image['path']) ? $image['path'] : NULL;
					$package_array[] = (object)$tempArr;
				}
			}
			$this->data['packages'] = $package_array;
		}
		//echo '<pre>'; print_r($this->data['packages']); echo '</pre>'; die();
		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->_render_page('front' . DIRECTORY_SEPARATOR . 'packages' . DIRECTORY_SEPARATOR . 'index', $this->data);
	}
	public function list($type)
	{
		$image_type = 'featured';
		$status = 'Published';
		$packages = (array)$this->packages_model->get_packages_by_type_status($type, $status);
		if(!empty($packages))
		{
			$package_id = array();
			for ($i = 0; $i < count($packages); $i++) {
				$package_id[] = $packages[$i]['id'];
			}
			$new_array = [];
			if(!empty($package_id)){
				foreach ($package_id as $id) {
					$package = (array)$this->packages_model->get_package_by_id($id, $type);
					$image = (array)$this->packages_model->get_package_featured_image_by_id($id, $image_type);
					$tempArr['id'] = !empty($package['id']) ? $package['id'] : NULL;
					$tempArr['type'] = !empty($package['type']) ? $package['type'] : NULL;
					$tempArr['title'] = !empty($package['title']) ? $package['title'] : NULL;
					$tempArr['sub_title'] = !empty($package['sub_title']) ? $package['sub_title'] : NULL;
					$tempArr['price'] = !empty($package['price']) ? $package['price'] : NULL;
					$tempArr['sale_price'] = !empty($package['sale_price']) ? $package['sale_price'] : NULL;
					$tempArr['no_of_days'] = !empty($package['no_of_days']) ? $package['no_of_days'] : NULL;
					$tempArr['departure_date'] = !empty($package['departure_date']) ? $package['departure_date'] : NULL;
					$tempArr['return_date'] = !empty($package['return_date']) ? $package['return_date'] : NULL;
					$tempArr['status'] = !empty($package['status']) ? $package['status'] : NULL;
					$tempArr['date'] = !empty($package['date']) ? $package['date'] : NULL;
					$tempArr['path'] = !empty($image['path']) ? $image['path'] : NULL;
					$package_array[] = (object)$tempArr;
				}
			}
			//echo '<pre>'; print_r($package_array); echo '</pre>'; die();
			$this->data['packages'] = $package_array;
		}
		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->_render_page('front' . DIRECTORY_SEPARATOR . 'packages' . DIRECTORY_SEPARATOR . 'travel' . DIRECTORY_SEPARATOR . 'post_type' . DIRECTORY_SEPARATOR . 'index', $this->data);
	}
	public function details($type, $id = NULL)
	{
		$image_type = 'featured';
		$status = 'Published';
		$seo = $this->seo_model->get_seo_by_page_id($type, $id);
		$pack = $this->packages_model->get_package_by_id_status($id, $type, $status);
		if(!empty($pack))
		{
			$image = (array)$this->packages_model->get_package_featured_image_by_id($id, $image_type);
			$package_array = (array)$pack;
			$package_meta = $this->packages_model->get_package_meta_by_id($id);
			$features = $this->packages_model->get_package_meta_by_key_result($id, $key = 'feature');
			$amenities = $this->packages_model->get_package_meta_by_key_result($id, $key = 'amenity');
			$itinerary = $this->packages_model->get_itinerary($id, $type);
			$location = $this->packages_model->get_package_meta_by_key_row($id, $key = 'location');
			$image_type = 'gallery';
			$gallery = $this->packages_model->get_package_gallery_images_by_id($id, $image_type);
			if(!empty($package_meta)) {
				$package = array_replace_recursive($package_array, $package_meta);
			} else {
				$package = $package_array;
			}
			$this->data['features'] = $features;
			$this->data['amenities'] = $amenities;
			$this->data['itinerary'] = $itinerary;
			$this->data['location'] = $location;
			$this->data['package'] = (object)$package;
			//echo '<pre>'; print_r($this->data['package']); echo '</pre>'; die();
			$this->data['gallery'] = $gallery;
			$this->data['featured_image'] = (object)$image;
			if(!empty($image['thumb'])) {
				$seoimage = base_url().$image['thumb'];
			} else {
				$seoimage = base_url().'assets/images/airline-default.jpg';
			}
			$e = array(
				'general' => true, //description
				'og' => true,
				'twitter'=> true,
				'robot'=> true
			);
			meta_tags(
				$e, $title = $seo['title'],
				$desc = $seo['description'],
				$imgurl = $seoimage,
				$url = current_url()
			);
			$this->data['title'] = $title;
			$this->data['description'] = $desc;
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('front' . DIRECTORY_SEPARATOR . 'packages' . DIRECTORY_SEPARATOR . 'travel' . DIRECTORY_SEPARATOR . 'post_type' . DIRECTORY_SEPARATOR . 'details', $this->data);
		}
		else
		{
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('errors' . DIRECTORY_SEPARATOR . 'front' . DIRECTORY_SEPARATOR . '404', $this->data);
		}
	}
}

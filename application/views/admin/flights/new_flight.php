<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/flights/all_flights">Flights</a></li>
							<li class="breadcrumb-item active">Add New Flight</li>
						</ol>
						<h4 class="page-title">Add New Flight</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<form id="form" name="editflight" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-lg-9 col-md-12 col-sm-12">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<div class="form-group row">
										<label for="flight_from" class="col-lg-2 col-md-2 col-sm-12 col-form-label">From <span class="text-danger">*</span></label>
										<div class="col-lg-10 col-md-10 col-sm-12">
											<select name="flight_from" class="selectpicker" data-live-search="true"  data-style="btn-white">
												<option selected="true" disabled="disabled">-- Select flying from --</option>
												<?php if(!empty($all_terminals)): foreach ($all_terminals as $terminal): ?>
												<option value="<?php echo $terminal->terminal_id;?>"><?php echo $terminal->terminal_name;?> (<?php echo $terminal->terminal_short_code;?>), <?php echo $terminal->country_name;?></option>
												<?php endforeach; else:?>
												<option>No Records Found</option>
												<?php endif;?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="flight_to" class="col-lg-2 col-md-2 col-sm-12 col-form-label">To <span class="text-danger">*</span></label>
										<div class="col-lg-10 col-md-10 col-sm-12">
											<select name="flight_to" class="selectpicker" data-live-search="true"  data-style="btn-white">
												<option selected="true" disabled="disabled">-- Select flying to --</option>
											<?php if(!empty($all_terminals)): foreach ($all_terminals as $terminal): ?>
												<option value="<?php echo $terminal->terminal_id;?>"><?php echo $terminal->terminal_name;?> (<?php echo $terminal->terminal_short_code;?>), <?php echo $terminal->country_name;?></option>
												<?php endforeach; else:?>
												<option>No Records Found</option>
												<?php endif;?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="flight_airline" class="col-lg-2 col-md-2 col-sm-12 col-form-label">Airline <span class="text-danger">*</span></label>
										<div class="col-lg-10 col-md-10 col-sm-12">
											<select name="flight_airline" class="selectpicker" data-live-search="true"  data-style="btn-white">
												<option selected="true" disabled="disabled">-- Select airline --</option>
											<?php if(!empty($all_airlines)): foreach ($all_airlines as $airline): ?>
												<option value="<?php echo $airline->id;?>"><?php echo $airline->airline_name;?></option>
												<?php endforeach; else:?>
												<option>No Records Found</option>
												<?php endif;?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="flight_aircraft_class" class="col-lg-2 col-md-2 col-sm-12 col-form-label">Aircraft Class <span class="text-danger">*</span></label>
										<div class="col-lg-10 col-md-10 col-sm-12">
											<select name="flight_aircraft_class" class="selectpicker" data-live-search="true"  data-style="btn-white">
												<option selected="true" disabled="disabled">-- Select aircraft class --</option>
											<?php if(!empty($all_aircraft_classes)): foreach ($all_aircraft_classes as $classes): ?>
												<option value="<?php echo $classes->id;?>"><?php echo $classes->aircraft_class_name;?></option>
												<?php endforeach; else:?>
												<option>No Records Found</option>
												<?php endif;?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="flight_protection" class="col-lg-2 col-md-2 col-sm-12 col-form-label">Protection <span class="text-danger">*</span></label>
										<div class="col-lg-10 col-md-10 col-sm-12">
											<select name="flight_protection" class="selectpicker" data-live-search="true"  data-style="btn-white">
												<option selected="true" disabled="disabled">-- Select protection --</option>
											<?php if(!empty($all_protections)): foreach ($all_protections as $protection): ?>
												<option value="<?php echo $protection->id;?>"><?php echo $protection->protection_name;?></option>
												<?php endforeach; else:?>
												<option>No Records Found</option>
												<?php endif;?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="flight_route" class="col-lg-2 col-md-2 col-sm-12 col-form-label">Route <span class="text-danger">*</span></label>
										<div class="col-lg-10 col-md-10 col-sm-12">
											<select name="flight_route" class="selectpicker" data-live-search="true"  data-style="btn-white">
												<option  disabled="disabled">-- Select route --</option>
												<option selected="true" value="Return">Return</option>
												<option value="One way">One way</option>
												<option value="Multi-city">Multi-city</option>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="price" class="col-lg-2 col-md-2 col-sm-12 col-form-label">Price <span class="text-danger">*</span></label>
										<div class="col-lg-10 col-md-10 col-sm-12">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text" id="basic-addon1"><?php echo $settings->currency_unit;?></span>
												</div>
												<?php echo form_input($flight_price);?>
											</div>
										</div>
									</div>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div><!-- end col -->
					<div class="col-lg-3 col-md-12 col-sm-12">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<a class="btn btn-default btn-block waves-effect waves-light" onclick="document.editflight.submit()">Add</a>
								</div>
							</div>
						</div>
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<h4 class="header-title">Categories</h4>
									<a class="label label-success btn-default" href="<?php echo base_url();?>admin/flights/categories">Add new category</a>
									<div class="mt-3">
										<?php if(!empty($terms)): foreach ($terms as $term): ?>
										<div class="custom-control custom-checkbox">					
											<?php echo form_checkbox('term_id[]', $term['term_taxonomy_id'], FALSE, 'id="category_'.$term['name'].'" class="custom-control-input"');?>
											<label for="category_<?php echo $term['name'];?>" class="custom-control-label"><?php echo $term['name'];?></label>
										</div>
										<?php endforeach; else:?>
										<?php endif;?>
									</div>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
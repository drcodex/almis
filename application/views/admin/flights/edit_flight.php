<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/flights">Flights</a></li>
							<li class="breadcrumb-item active">Edit Flight</li>
						</ol>
						<h4 class="page-title">Edit Flight</h4>
						<a href="<?php echo base_url();?>admin/flights/new_flight" class="btn btn-default waves-effect waves-light">Add New Flight</a>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<form id="form" method="post" name="editflight" class="form-horizontal">
				<div class="row">
					<div class="col-9">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<div class="form-group row">
										<label for="flight_from" class="col-2 col-form-label">From <span class="text-danger">*</span></label>
										<div class="col-10">
											<select name="flight_from" class="selectpicker" data-live-search="true"  data-style="btn-white">
												<option class="option-disabled" selected="true" value="<?php echo $single_flight['flight_from_id'];?>"><?php echo $single_flight['flight_from'];?> (<?php echo $single_flight['flight_from_code'];?>)</option>
												<?php if(!empty($all_terminals)): foreach ($all_terminals as $terminal): ?>
												<option value="<?php echo $terminal->terminal_id;?>"><?php echo $terminal->terminal_name;?> (<?php echo $terminal->terminal_short_code;?>), <?php echo $terminal->country_name;?></option>
												<?php endforeach; else:?>
												<option>No Records Found</option>
												<?php endif;?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="flight_to" class="col-2 col-form-label">To <span class="text-danger">*</span></label>
										<div class="col-10">
											<select name="flight_to" class="selectpicker" data-live-search="true"  data-style="btn-white">
												<option class="option-disabled" selected="true" value="<?php echo $single_flight['flight_to_id'];?>"><?php echo $single_flight['flight_to'];?> (<?php echo $single_flight['flight_to_code'];?>)</option>
											<?php if(!empty($all_terminals)): foreach ($all_terminals as $terminal): ?>
												<option value="<?php echo $terminal->terminal_id;?>"><?php echo $terminal->terminal_name;?> (<?php echo $terminal->terminal_short_code;?>), <?php echo $terminal->country_name;?></option>
												<?php endforeach; else:?>
												<option>No Records Found</option>
												<?php endif;?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="airline_name" class="col-2 col-form-label">Airline <span class="text-danger">*</span></label>
										<div class="col-10">
											<select name="airline_name" class="selectpicker" data-live-search="true"  data-style="btn-white">
												<option class="option-disabled" selected="true" value="<?php echo $single_flight['airline_id'];?>"><?php echo $single_flight['airline_name'];?></option>
											<?php if(!empty($all_airlines)): foreach ($all_airlines as $airline): ?>
												<option value="<?php echo $airline->id;?>"><?php echo $airline->airline_name;?></option>
												<?php endforeach; else:?>
												<option>No Records Found</option>
												<?php endif;?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="aircraft_class_name" class="col-2 col-form-label">Aircraft Class <span class="text-danger">*</span></label>
										<div class="col-10">
											<select name="aircraft_class_name" class="selectpicker" data-live-search="true"  data-style="btn-white">
												<option class="option-disabled" selected="true" value="<?php echo $single_flight['class_id'];?>"><?php echo $single_flight['aircraft_class_name'];?></option>
											<?php if(!empty($all_aircraft_classes)): foreach ($all_aircraft_classes as $classes): ?>
												<option value="<?php echo $classes->id;?>"><?php echo $classes->aircraft_class_name;?></option>
												<?php endforeach; else:?>
												<option>No Records Found</option>
												<?php endif;?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="protection_name" class="col-2 col-form-label">Protection <span class="text-danger">*</span></label>
										<div class="col-10">
											<select name="protection_name" class="selectpicker" data-live-search="true"  data-style="btn-white">
												<option class="option-disabled" selected="true" value="<?php echo $single_flight['protection_id'];?>"><?php echo $single_flight['protection_name'];?></option>
											<?php if(!empty($all_protections)): foreach ($all_protections as $protection): ?>
												<option value="<?php echo $protection->id;?>"><?php echo $protection->protection_name;?></option>
												<?php endforeach; else:?>
												<option>No Records Found</option>
												<?php endif;?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="flight_route" class="col-2 col-form-label">Route <span class="text-danger">*</span></label>
										<div class="col-10">
											<select name="flight_route" class="selectpicker" data-style="btn-white">
												<option  disabled="disabled">-- Select route --</option>
												<?php if($single_flight['flight_route'] == 'Return'):?>
												<option selected="true" value="Return">Return</option>
												<option value="One way">One way</option>
												<option value="Multi-city">Multi-city</option>
												<?php elseif($single_flight['flight_route'] == 'One way'):?>
												<option value="Return">Return</option>
												<option selected="true" value="One way">One way</option>
												<option value="Multi-city">Multi-city</option>
												<?php elseif($single_flight['flight_route'] == 'Multi-city'):?>
												<option value="Return">Return</option>
												<option value="One way">One way</option>
												<option selected="true" value="Multi-city">Multi-city</option>
												<?php endif;?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="price" class="col-2 col-form-label">Price <span class="text-danger">*</span></label>
										<div class="col-10">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text" id="basic-addon1"><?php echo $settings->currency_unit;?></span>
												</div>
												<?php echo form_input($flight_price);?>
											</div>
										</div>
									</div>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div><!-- end col -->
					<div class="col-3">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<a class="btn btn-default btn-block waves-effect waves-light" onclick="document.editflight.submit()">Update</a>
								</div>
							</div>
						</div>
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<h4 class="header-title">Categories</h4>
									<a class="label label-success btn-default" href="<?php echo base_url();?>admin/flights/categories">Add new category</a>
									<div class="mt-3">
										<?php if(!empty($terms)): foreach ($terms as $term): ?>
										<?php if(!empty($term['term_taxonomy_id']) && $term['object_id'] == $this->uri->segment(4)):?>
										<div class="custom-control custom-checkbox">
											<?php echo form_checkbox('term_id[]', $term['term_taxonomy_id'], FALSE, 'id="category_'.$term['name'].'" class="custom-control-input" checked');?>
											<label for="category_<?php echo $term['name'];?>" class="custom-control-label"><?php echo $term['name'];?></label>
										</div>
										<?php else:?>
										<div class="custom-control custom-checkbox">
											<?php echo form_checkbox('term_id[]', $term['term_taxonomy_id'], FALSE, 'id="category_'.$term['name'].'" class="custom-control-input"');?>
											<label for="category_<?php echo $term['name'];?>" class="custom-control-label"><?php echo $term['name'];?></label>
											</div>
											<?php endif;?>
										<?php endforeach; else:?>
										<?php endif;?>
									</div>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
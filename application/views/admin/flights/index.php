<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item active">Flights</li>
						</ol>
						<h4 class="page-title">All Flights</h4>
						<a href="<?php echo base_url();?>admin/flights/new_flight" class="btn btn-default waves-effect waves-light">Add New Flight</a>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<form id="form" method="post" class="form-horizontal">
			<div class="row">
				<div class="col-12">
					<div class="card-box table-responsive">
						<button type="submit" id="bulk_update" name="bulk_update" class="btn btn-sm btn-success waves-effect waves-light m-b-20"><span class="btn-label"><i class="ti-harddrive"></i></span>Bulk Update</button>
						<table id="datatable" class="table table-hover mails m-0 table table-actions-bar">
							<thead>
								<tr>
									<th>From</th>
									<th>To</th>
									<th>Airline</th>
									<th>Price (<?php echo $settings->currency_unit;?>)</th>
									<th>Flight Class</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($all_flights)): foreach ($all_flights as $flight): ?>
								<tr>
									<td><?php echo $flight['flight_from'];?> (<?php echo $flight['flight_from_code'];?>)</td>
									<td><?php echo $flight['flight_to'];?> (<?php echo $flight['flight_to_code'];?>)</td>
									<td><?php echo $flight['airline_name'];?></td>
									<td><input id="ticket_price" name="ticket_price[<?php echo $flight['id'];?>]" class="form-control form-control-sm" type="text" value="<?php echo $flight['flight_price'];?>" /></td>
									<td><?php echo $flight['aircraft_class_name'];?></td>
									<td><?php echo anchor("admin/flights/edit_flight/".$flight['id'], '<i class="md md-edit"></i>', 'class="table-action-btn"') ;?><?php echo anchor("admin/flights/delete_flight/".$flight['id'], '<i class="md md-delete"></i>', 'class="table-action-btn"') ;?></td>
								</tr>
								<?php endforeach; else:?>
								<tr>
									<td class="center text-center" colspan="6">No Records Found</td>
								</tr>
								<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div> <!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
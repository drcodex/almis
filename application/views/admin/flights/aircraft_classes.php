<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/flights/flights">Flights</a></li>
							<li class="breadcrumb-item active">Aircraft Classes</li>
						</ol>
						<h4 class="page-title">Aircraft Classes</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<?php if(isset($class)){?>
			<div id="infoMessage">
				<p class="<?php echo $class;?>"><?php echo $message;?></p>
			</div>
			<?php }else{?>
				<div id="infoMessage"><?php echo $message;?></div>
			<?php };?>
			<form id="form" name="editflight" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-lg-5 col-md-5 col-sm-12">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<div class="form-group row">
										<label for="flight_airline" class="col-lg-2 col-md-2 col-sm-12 col-form-label">Class <span class="text-danger">*</span></label>
										<div class="col-lg-10 col-md-10 col-sm-12">
											<?php echo form_input($aircraft_class_name);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="flight_aircraft_class" class="col-lg-2 col-md-2 col-sm-12 col-form-label">Details</label>
										<div class="col-lg-10 col-md-10 col-sm-12">
											<?php echo form_input($aircraft_class_description);?>
										</div>
									</div>
								<a href="#" class="btn btn-default btn-block waves-effect waves-light" onclick="document.editflight.submit()">Publish</a>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div><!-- end col -->
					<div class="col-lg-7 col-md-7 col-sm-12">
						<div class="card-box table-responsive">
							<div class="row">
								<div class="col-12">
									<table id="datatable" class="table table-hover mails m-0 table table-actions-bar">
										<thead>
											<tr>
												<th>Name</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php if(!empty($all_aircraft_classes)): foreach ($all_aircraft_classes as $classes): ?>
											<tr>
												<td><?php echo $classes->aircraft_class_name;?></td>
												<td><?php echo anchor("admin/flights/edit_aircraft_class/".$classes->id, '<i class="md md-edit"></i>', 'class="table-action-btn"') ;?><?php echo anchor("admin/flights/delete_aircraft_class/".$classes->id, '<i class="md md-delete"></i>', 'class="table-action-btn"') ;?></td>
											</tr>
											<?php endforeach; else:?>
											<tr>
												<td class="center text-center" colspan="6">No Records Found</td>
											</tr>
											<?php endif;?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
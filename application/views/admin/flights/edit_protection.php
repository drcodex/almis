<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/flights/protections">Protections</a></li>
							<li class="breadcrumb-item active">Edit Protection</li>
						</ol>
						<h4 class="page-title">Edit Protection</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<form id="form" name="editflight" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-12">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
										<div class="form-group row">
											<label for="protection_name" class="col-12 col-form-label">Class <span class="text-danger">*</span></label>
											<div class="col-12">
												<?php echo form_input($protection_name);?>
											</div>
										</div>
										<div class="form-group row">
											<label for="protection_description" class="col-12 col-form-label">Details</label>
											<div class="col-12">
												<?php echo form_input($protection_description);?>
											</div>
										</div>
									<a class="btn btn-default btn-block waves-effect waves-light" onclick="document.editflight.submit()">Publish</a>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div><!-- end col -->
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
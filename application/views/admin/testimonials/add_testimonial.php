<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/testimonials">Testimonials</a></li>
							<li class="breadcrumb-item active">Add New Testimonial</li>
						</ol>
						<h4 class="page-title">Add New Testimonial</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<form id="form" name="editflight" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-12">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<div class="form-group row">
										<label for="testimonial" class="col-lg-12 col-md-12 col-sm-12 col-form-label">Testimonial <span class="text-danger">*</span></label>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<?php echo form_textarea($testimonial);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="name" class="col-lg-12 col-md-12 col-sm-12 col-form-label">Name <span class="text-danger">*</span></label>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<?php echo form_input($name);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="designation" class="col-lg-12 col-md-12 col-sm-12 col-form-label">Designation</label>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<?php echo form_input($designation);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="company" class="col-lg-12 col-md-12 col-sm-12 col-form-label">Comapany</label>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<?php echo form_input($company);?>
										</div>
									</div>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div><!-- end col -->
					<div class="col-lg-3 col-md-3 col-sm-12">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<a href="#" class="btn btn-default btn-block waves-effect waves-light" onclick="document.editflight.submit()">Publish</a>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
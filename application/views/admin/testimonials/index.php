<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item active">Testimonials</li>
						</ol>
						<h4 class="page-title">Testimonials</h4>
						<a href="<?php echo base_url();?>admin/testimonials/add_testimonial" class="btn btn-default waves-effect waves-light">Add New Testimonial</a>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<?php if(isset($class)){?>
			<div id="infoMessage">
				<p class="<?php echo $class;?>"><?php echo $message;?></p>
			</div>
			<?php }else{?>
				<div id="infoMessage"><?php echo $message;?></div>
			<?php };?>
			<div class="row">
				<div class="col-12">
					<div class="card-box table-responsive">
						<table id="datatable" class="table table-hover mails m-0 table table-actions-bar">
							<thead>
								<tr>
									<th>Name</th>
									<th>Designation</th>
									<th>Comapny</th>
									<th>status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($testimonials)): foreach ($testimonials as $testimonial): ?>
								<tr>
									<td><?php echo $testimonial->name;?></td>
									<td><?php echo $testimonial->designation;?></td>
									<td><?php echo $testimonial->company;?></td>
									<td><?php echo $testimonial->status;?></td>
									<td><?php echo anchor("admin/testimonials/edit_testimonial/".$testimonial->id, '<i class="md md-edit"></i>', 'class="table-action-btn"') ;?><?php echo anchor("admin/testimonials/delete_testimonial/".$testimonial->id, '<i class="md md-delete"></i>', 'class="table-action-btn"') ;?></td>
								</tr>
								<?php endforeach; else:?>
								<tr>
									<td class="center text-center" colspan="6">No Records Found</td>
								</tr>
								<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div> <!-- end row -->
		</div> <!-- container -->
	</div> <!-- content -->
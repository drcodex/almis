<?php if(!empty($features)): foreach ($features as $feature): ?>
<tr>
	<td><?php echo $feature->meta_value;?></td>
	<td><a href="javsscript:void(0);" id="delete_feature" data-featureid="<?php echo $feature->pmeta_id;?>" data-spinner-color="#7e57c2" data-spinner-size="15px" data-spinner-lines="8" class="table-action-btn"><i class="md md-delete"></i></a></td>
</tr>
<?php endforeach; else:?>
<tr>
	<td class="center text-center" colspan="6">No Records Found</td>
</tr>
<?php endif;?>
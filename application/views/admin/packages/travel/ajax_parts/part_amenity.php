<?php if(!empty($amenities)): foreach ($amenities as $amenity): ?>
<tr>
	<td><?php echo $amenity->meta_value;?></td>
	<td><a href="javsscript:void(0);" id="delete_amenity" data-amenityid="<?php echo $amenity->pmeta_id;?>" data-spinner-color="#7e57c2" data-spinner-size="15px" data-spinner-lines="8" class="table-action-btn"><i class="md md-delete"></i></a></td>
</tr>
<?php endforeach; else:?>
<tr>
	<td class="center text-center" colspan="6">No Records Found</td>
</tr>
<?php endif;?>
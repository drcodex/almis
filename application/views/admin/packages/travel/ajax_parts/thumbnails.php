<?php if(!empty($images)): foreach ($images as $image): ?>
<tr>
	<td><img src="<?php echo base_url().$image->thumb;?>" alt="image" class="img-fluid img-thumbnail thumb-md" width="200"></td>
	<td><?php echo $image->size;?>kb</td>
	<td><a href="javsscript:void(0);" id="delete_image" data-imageid="<?php echo $image->id;?>" data-imagetype="gallery" data-spinner-color="#7e57c2" data-spinner-size="15px" data-spinner-lines="8" class="table-action-btn"><i class="md md-delete"></i></a></td>
</tr>
<?php endforeach; else:?>
<tr>
	<td class="center text-center" colspan="6">No Records Found</td>
</tr>
<?php endif;?>
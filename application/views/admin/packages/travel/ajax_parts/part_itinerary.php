<?php if(!empty($itinerary)): foreach ($itinerary as $iti): ?>
<tr>
	<td><?php echo $iti->day;?></td>
	<td><?php echo $iti->title;?></td>
	<td><a href="javsscript:void(0);" id="delete_itinerary" data-itineraryid="<?php echo $iti->id;?>" data-imagetype="gallery" data-spinner-color="#7e57c2" data-spinner-size="15px" data-spinner-lines="8" class="table-action-btn"><i class="md md-delete"></i></a></td>
</tr>
<?php endforeach; else:?>
<tr>
	<td class="center text-center" colspan="6">No Records Found</td>
</tr>
<?php endif;?>
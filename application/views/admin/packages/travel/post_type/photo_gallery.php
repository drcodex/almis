<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages">Packages</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>"><?php echo $head_info['label'];?></a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>"><?php echo $package->title;?></a></li>
							<li class="breadcrumb-item active">Photo Gallery</li>
						</ol>
						<h4 class="page-title">Photo Gallery</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="table-box opport-box m-b-20">
				<a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/description">Description</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/photo_gallery">Gallery</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/features">Features</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/amenities">Amenities</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/itinerary">Itinerary</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/location">Location</a>
			</div>
			<?php if(isset($class)){?>
			<div id="infoMessage">
				<p class="<?php echo $class;?>"><?php echo $message;?></p>
			</div>
			<?php }else{?>
				<div id="infoMessage"><?php echo $message;?></div>
			<?php };?>
			<form id="form" name="upload_image_package" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-5">
						<div class="card-box">
							<div class="row">
								<div class="col-md-12 portlets">
									<div class="m-b-0">
									<?php if(!empty($package_image->thumb)){;?>
										<input type="file" class="dropify" name="file" data-height="120" data-default-file="<?php echo base_url().$package_image->thumb;?>" data-allowed-file-extensions="jpeg jpg png"  data-show-remove="false" data-show-loader="true">
									<?php } else {;?>
										<input type="file" class="dropify" name="file" data-height="120" data-allowed-file-extensions="jpeg jpg png"  data-show-remove="false" data-show-loader="true">
									<?php } ;?>
                                        <section class="progress-demo">
    										<button id="uploadify" class="ladda-button btn btn-default btn-block waves-effect waves-light" data-imagetype="gallery" data-style="expand-left">Upload</button>
                                        </section>
									</div>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div><!-- end col -->
					<div class="col-7">
						<div class="card-box table-responsive">
							<div class="row">
								<div class="col-12">
									<table id="datatable" class="table table-hover mails m-0 table table-actions-bar">
										<thead>
											<tr>
												<th>Name</th>
												<th>Size</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody class="package_gallery">
											<?php if(!empty($images)): foreach ($images as $image): ?>
											<tr>
												<td><img src="<?php echo base_url().$image->thumb;?>" alt="image" class="img-fluid img-thumbnail thumb-md" width="200"></td>
												<td><?php echo $image->size;?>kb</td>
												<td><a href="javsscript:void(0);" id="delete_image" data-imageid="<?php echo $image->id;?>" data-imagetype="gallery" class="table-action-btn"><i class="md md-delete"></i></a></td>
											</tr>
											<?php endforeach; else:?>
											<tr>
												<td class="center text-center" colspan="6">No Records Found</td>
											</tr>
											<?php endif;?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
	<script>
	$(document).on("click", "#uploadify", function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
		e.stopPropagation();
	 	var l = Ladda.create(this);
	 	l.start();
		var package_id = "<?php echo $this->uri->segment(4);?>";
		var uploadfile = $('.dropify')[0].files[0];
		var image_type = $(this).data('imagetype');
		
		type = uploadfile.type;
		var form_data = new FormData(); 
		form_data.append("file", uploadfile)
		form_data.append("package_id", package_id)
		form_data.append("type", image_type)
		var url = "<?php echo base_url();?>admin/ajax/gallery_image_upload";

		$.ajax({
			url: url,
			type: "POST",
			async:"false",
			dataType: "html",
			cache:false,
			contentType: false,
			processData: false,
			data: form_data, // serializes the form's elements.
			success: function(data)
			{
				data = JSON.parse(data);
				if(data.response == "yes")
				{
					l.stop();
					$("#form")[0].reset();
					$('.package_gallery').html(data.content);
					$.Notification.autoHideNotify(data.response_type, 'top right', 'Success', data.message);
				}
			}
		});
	});
	</script>
	<script>
	$(document).on("click", "#delete_image", function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
		e.stopPropagation();
	 	var l = Ladda.create(this);
	 	l.start();
		var package_id = "<?php echo $this->uri->segment(4);?>";
		var image_id = $(this).data('imageid');
		var image_type = $(this).data('imagetype');
		
		var form_data = new FormData();
		form_data.append("package_id", package_id)
		form_data.append("image_id", image_id)
		form_data.append("type", image_type)
		var url = "<?php echo base_url();?>admin/ajax/delete_gallery_image";

		$.ajax({
			url: url,
			type: "POST",
			async:"false",
			dataType: "html",
			cache:false,
			contentType: false,
			processData: false,
			data: form_data, // serializes the form's elements.
			success: function(data)
			{
				data = JSON.parse(data);
				if(data.response == "yes")
				{
					l.stop();
					$('.package_gallery').html(data.content);
					$.Notification.autoHideNotify('error', 'top right', 'Deleted','gallery image deleted successfully.');
				}
			}
		});
	});
	</script>
<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages">Packages</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>"><?php echo $head_info['singular_label'];?></a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>"><?php echo $package->title;?></a></li>
							<li class="breadcrumb-item active">Itinerary</li>
						</ol>
						<h4 class="page-title">Itinerary</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="table-box opport-box m-b-20">
				<a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/description">Description</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/photo_gallery">Gallery</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/features">Features</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/amenities">Amenities</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/itinerary">Itinerary</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/location">Location</a>
			</div>
			<?php if(isset($class)){?>
			<div id="infoMessage">
				<p class="<?php echo $class;?>"><?php echo $message;?></p>
			</div>
			<?php }else{?>
				<div id="infoMessage"><?php echo $message;?></div>
			<?php };?>
			<form id="form" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-5">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<div class="form-group row">
										<label for="day" class="col-12 col-form-label">Itinerary Day<span class="text-danger">*</span></label>
										<div class="col-12">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text" id="basic-addon1">Day</span>
												</div>
												<?php echo form_input($day);?>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label for="itinerary_title" class="col-12 col-form-label">Itinerary Title<span class="text-danger">*</span></label>
										<div class="col-12">
											<?php echo form_input($itinerary_title);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="itinerary_detail" class="col-12 col-form-label">Itinerary Details<span class="text-danger">*</span></label>
										<div class="col-12">
											<?php echo form_input($itinerary_detail);?>
										</div>
									</div>
									<a href="javsscript:void(0);" class="btn btn-default btn-block waves-effect waves-light" id="add_itinerary">Add</a>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div><!-- end col -->
					<div class="col-7">
						<div class="card-box table-responsive">
							<div class="row">
								<div class="col-12">
									<table id="datatable" class="table table-hover mails m-0 table table-actions-bar">
										<thead>
											<tr>
												<th>Day</th>
												<th>Title</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody class="hajj_itinerary">
											<?php if(!empty($itinerary)): foreach ($itinerary as $iti): ?>
											<tr>
												<td><?php echo $iti->day;?></td>
												<td><?php echo $iti->title;?></td>
												<td><a href="javsscript:void(0);" id="delete_itinerary" data-itineraryid="<?php echo $iti->id;?>" data-spinner-color="#7e57c2" data-spinner-size="15px" data-spinner-lines="8" class="table-action-btn"><i class="md md-delete"></i></a></td>
											</tr>
											<?php endforeach; else:?>
											<tr>
												<td class="center text-center" colspan="6">No Records Found</td>
											</tr>
											<?php endif;?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
	<script>
	$(document).on("click", "#add_itinerary", function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
		e.stopPropagation();
	 	var l = Ladda.create(this);
	 	l.start();
		var day = $("#day").val();
		var itinerary_title = $("#itinerary_title").val();
		var itinerary_detail = $("#itinerary_detail").val();
		var type = "<?php echo $this->uri->segment(3);?>";
		var package_id = "<?php echo $this->uri->segment(4);?>";
		
		var form_data = new FormData(); 
		form_data.append("day", day)
		form_data.append("itinerary_title", itinerary_title)
		form_data.append("itinerary_detail", itinerary_detail)
		form_data.append("type", type)
		form_data.append("package_id", package_id)
		var url = "<?php echo base_url();?>admin/ajax/add_itinerary";

		$.ajax({
			url: url,
			type: "POST",
			async:"false",
			dataType: "html",
			cache:false,
			contentType: false,
			processData: false,
			data: form_data, // serializes the form's elements.
			success: function(data)
			{				
				data = JSON.parse(data);
				if(data.response == "yes")
				{
					l.stop();
					$("#form")[0].reset();
					$('.hajj_itinerary').html(data.content);
					$.Notification.autoHideNotify('custom', 'top right', 'Success', data.message);
				}
			}
		});
	});
	$(document).on("click", "#delete_itinerary", function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
		e.stopPropagation();
	 	var l = Ladda.create(this);
	 	l.start();
		var type = "<?php echo $this->uri->segment(3);?>";
		var package_id = "<?php echo $this->uri->segment(4);?>";
		var itinerary_id = $(this).data('itineraryid');
		
		var form_data = new FormData();
		form_data.append("type", type)
		form_data.append("package_id", package_id)
		form_data.append("itinerary_id", itinerary_id)
		var url = "<?php echo base_url();?>admin/ajax/delete_itinerary";

		$.ajax({
			url: url,
			type: "POST",
			async:"false",
			dataType: "html",
			cache:false,
			contentType: false,
			processData: false,
			data: form_data, // serializes the form's elements.
			success: function(data)
			{				
				data = JSON.parse(data);
				if(data.response == "yes")
				{
					l.stop();
					$('.hajj_itinerary').html(data.content);
					$.Notification.autoHideNotify('error', 'top right', 'Success', data.message);
				}
			}
		});
	});
	</script>
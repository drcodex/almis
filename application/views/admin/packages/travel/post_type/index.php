<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages">Packages</a></li>
							<li class="breadcrumb-item"><?php echo $head_info['label'];?></li>
						</ol>
						<h4 class="page-title"><?php echo $head_info['singular_label'];?> Packages</h4>
						<a href="<?php echo base_url();?>admin/packages/<?php echo $head_info['post_type_name'];?>/create" class="btn btn-default waves-effect waves-light">Add <?php echo $head_info['singular_label'];?> Package</a>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<div class="row">
				<div class="col-lg-12">
						<?php if($packages):?>
								<?php foreach($packages as $package):?>
								<div class="card-box m-b-10">
									<div class="table-box opport-box">
										<div class="table-detail title-detail">
                                            <div class="member-info">
                                                <h4 class="m-t-0">
												<?php if($package->type == 'umrah'){?>
													<a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>">
														<span class="label label-warning pull-left m-r-10"><?php echo $package->type;?></span>
													</a>
												<?php };?>
												<b><?php echo $package->title;?></b></h4>
                                                <p class="text-dark m-b-5"><span class="text-muted"><?php echo $package->sub_title;?></span></p>
                                                <p class="text-dark m-b-0"><b>No of days: </b> <span class="text-muted"><?php echo $package->no_of_days;?></span></p>
                                            </div>
										</div>

										<div class="table-detail" style="width:300px;">
											<p class="text-dark m-b-5"><b>Price:</b> <span class="text-muted"><?php echo $package->price;?></span></p>
											<p class="text-dark m-b-0"><b>Sale Price:</b> <span class="text-muted"><?php echo $package->sale_price;?></span></p>
										</div>
                                        <div class="table-detail lable-detail">
										<?php if($package->status == 'Draft'){?>
                                            <span class="label label-danger"><?php echo $package->status;?></span>
										<?php } else { ?>
                                            <span class="label label-success"><?php echo $package->status;?></span>
										<?php };?>
                                        </div>
										<div class="table-detail lable-detail">
											<a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>" class="btn btn-sm btn-success waves-effect waves-light"><span class="btn-label"><i class="fa fa-check"></i></span>Add Details</a>
										</div>
                                        <div class="table-detail table-actions-bar" style="width:80px;">
											<a href="<?php echo base_url();?>packages/<?php echo $package->type;?>/details/<?php echo $package->id;?>" class="table-action-btn pull-right" target="_blank"><i class="md md-remove-red-eye"></i></a>
                                            <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/edit" class="table-action-btn pull-right"><i class="md md-edit"></i></a>
                                        </div>
									</div>
									<div class="table-box opport-box p-t-10">
										<a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/description">Description</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/photo_gallery">Gallery</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/features">Features</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/amenities">Amenities</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/itinerary">Itinerary</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/location">Location</a>
									</div>
								</div>
								<?php endforeach;?>
						<?php else:?>
						<div class="card-box">
								There is no package yet.
						</div>
						<?php endif;?>
				</div> <!-- end col -->
			</div>
		</div> <!-- container -->
	</div> <!-- content -->
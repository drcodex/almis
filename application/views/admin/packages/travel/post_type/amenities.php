<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages">Packages</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>"><?php echo $head_info['label'];?></a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>"><?php echo $package->title;?></a></li>
							<li class="breadcrumb-item active">Amenities</li>
						</ol>
						<h4 class="page-title">Amenities</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="table-box opport-box m-b-20">
				<a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/description">Description</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/photo_gallery">Gallery</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/features">Features</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/amenities">Amenities</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/itinerary">Itinerary</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/location">Location</a>
			</div>
			<?php if(isset($class)){?>
			<div id="infoMessage">
				<p class="<?php echo $class;?>"><?php echo $message;?></p>
			</div>
			<?php }else{?>
				<div id="infoMessage"><?php echo $message;?></div>
			<?php };?>
			<form id="form" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-5">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<div class="form-group row">
										<label for="amenity" class="col-12 col-form-label">Amenity <span class="text-danger">*</span></label>
										<div class="col-12">
											<?php echo form_input($amenity);?>
										</div>
									</div>
									<a href="javsscript:void(0);" class="btn btn-default btn-block waves-effect waves-light" id="add_amenity">Add</a>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div><!-- end col -->
					<div class="col-7">
						<div class="card-box table-responsive">
							<div class="row">
								<div class="col-12">
									<table id="datatable" class="table table-hover mails m-0 table table-actions-bar">
										<thead>
											<tr>
												<th>Name</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody class="hajj_amenity">
											<?php if(!empty($amenities)): foreach ($amenities as $amenity): ?>
											<tr>
												<td><?php echo $amenity->meta_value;?></td>
												<td><a href="javsscript:void(0);" id="delete_amenity" data-amenityid="<?php echo $amenity->pmeta_id;?>" data-spinner-color="#7e57c2" data-spinner-size="15px" data-spinner-lines="8" class="table-action-btn"><i class="md md-delete"></i></a></td>
											</tr>
											<?php endforeach; else:?>
											<tr>
												<td class="center text-center" colspan="6">No Records Found</td>
											</tr>
											<?php endif;?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
	<script>
	$(document).on("click", "#add_amenity", function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
		e.stopPropagation();
	 	var l = Ladda.create(this);
	 	l.start();
		var amenity = $("#amenity").val();
		var package_id = "<?php echo $this->uri->segment(4);?>";
		
		var form_data = new FormData(); 
		form_data.append("package_id", package_id)
		form_data.append("amenity", amenity)
		var url = "<?php echo base_url();?>admin/ajax/add_amenity";

		$.ajax({
			url: url,
			type: "POST",
			async:"false",
			dataType: "html",
			cache:false,
			contentType: false,
			processData: false,
			data: form_data, // serializes the form's elements.
			success: function(data)
			{				
				data = JSON.parse(data);
				if(data.response == "yes")
				{
					l.stop();
					$("#form")[0].reset();
					$('.hajj_amenity').html(data.content);
					$.Notification.autoHideNotify('custom', 'top right', 'Success', data.message);
				}
			}
		});
	});
	$(document).on("click", "#delete_amenity", function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
		e.stopPropagation();
	 	var l = Ladda.create(this);
	 	l.start();
		var package_id = "<?php echo $this->uri->segment(4);?>";
		var amenity_id = $(this).data('amenityid');
		
		var form_data = new FormData();
		form_data.append("package_id", package_id)
		form_data.append("amenity_id", amenity_id)
		var url = "<?php echo base_url();?>admin/ajax/delete_amenity";

		$.ajax({
			url: url,
			type: "POST",
			async:"false",
			dataType: "html",
			cache:false,
			contentType: false,
			processData: false,
			data: form_data, // serializes the form's elements.
			success: function(data)
			{				
				data = JSON.parse(data);
				if(data.response == "yes")
				{
					l.stop();
					$('.hajj_amenity').html(data.content);
					$.Notification.autoHideNotify('error', 'top right', 'Success', data.message);
				}
			}
		});
	});
	</script>
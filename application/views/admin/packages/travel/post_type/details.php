<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages">Packages</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>"><?php echo $head_info['label'];?></a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>"><?php echo $package->title;?></a></li>
							<li class="breadcrumb-item active">Details</li>
						</ol>
						<h4 class="page-title"><?php echo $package->title;?></h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div class="card m-b-20 card-body">
						<h4 class="card-title">Add Descriptions</h4>
						<p class="card-text">Add package description in detail it help in improving SEO for package in google results.</p>
						<a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/description" class="btn btn-primary">Add</a>
					</div>
				</div>

				<div class="col-sm-4 col-xs-12">
					<div class="card m-b-20 card-body text-xs-center">
						<h4 class="card-title">Add Photo Gallery</h4>
						<p class="card-text">Add as many hotel images, city attractions and sight seeing pictures to attract customers..</p>
						<a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/photo_gallery" class="btn btn-primary">Add</a>
					</div>
				</div>

				<div class="col-sm-4 col-xs-12">
					<div class="card m-b-20 card-body text-xs-right">
						<h4 class="card-title">Add Travel Features</h4>
						<p class="card-text">Add maximum number of package features, do not miss even a minor one. It boost sales.</p>
						<a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/features" class="btn btn-primary">Add</a>
					</div>
				</div>
			</div>
			<!-- end row -->
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div class="card m-b-20 card-body text-xs-right">
						<h4 class="card-title">Add Travel Amenities</h4>
						<p class="card-text">Add maximum number of package amenities, do not miss even a minor one. It boost sales.</p>
						<a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/amenities" class="btn btn-primary">Add</a>
					</div>
				</div>
				
				<div class="col-sm-4 col-xs-12">
					<div class="card m-b-20 card-body">
						<h4 class="card-title">Add Travel Itinerary</h4>
						<p class="card-text">Guide traveler properly by providing detail itinerary plan for locations & activities.</p>
						<a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/itinerary" class="btn btn-primary">Add</a>
					</div>
				</div>

				<div class="col-sm-4 col-xs-12">
					<div class="card m-b-20 card-body text-xs-center">
						<h4 class="card-title">Add Location Map</h4>
						<p class="card-text">Add location map for users to check route and to calculate total duration for journey.</p>
						<a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/location" class="btn btn-primary">Add</a>
					</div>
				</div>
			</div>
			<!-- end row -->
		</div> <!-- container -->
	</div> <!-- content -->
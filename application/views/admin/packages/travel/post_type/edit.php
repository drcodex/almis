<script>
	$( function() {
		$("#datepicker_departure, #datepicker_return").flatpickr({
			//minDate: new Date(),
			enableTime: false,
			altInput: true,
			weekNumbers: true,
			utc: true,
			altFormat: "F d, Y",
			dateFormat: "U",
		});
	});
</script>
<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages">Packages</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages/<?php echo $head_info['post_type_name'];?>"><?php echo $head_info['label'];?></a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages/<?php echo $head_info['post_type_name'];?>/<?php echo $package->id;?>"><?php echo $package->title;?></a></li>
							<li class="breadcrumb-item active">Edit</li>
						</ol>
						<h4 class="page-title"><?php echo $package->title;?></h4>
						<a href="<?php echo base_url();?>admin/packages/umrah/create" class="btn btn-default waves-effect waves-light">Add <?php echo $head_info['singular_label'];?> Package</a>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="table-box opport-box m-b-20">
				<a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/description">Description</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/photo_gallery">Gallery</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/features">Features</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/amenities">Amenities</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/itinerary">Itinerary</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/location">Location</a>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<form id="form" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-12">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<div class="form-group row">
										<label for="title" class="col-12 col-form-label">Title <span class="text-danger">*</span></label>
										<div class="col-12">
											<?php echo form_input($package_title);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="sub_title" class="col-12 col-form-label">Sub Title <span class="text-danger">*</span></label>
										<div class="col-12">
											<?php echo form_input($sub_title);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="no_of_days" class="col-12 col-form-label">No. of days <span class="text-danger">*</span></label>
										<div class="col-12">
											<?php echo form_input($no_of_days);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="departure_date" class="col-12 col-form-label">Departure Date <span class="text-danger">*</span></label>
										<div class="col-12">
											<div class="input-group">
												<?php echo form_input($departure_date);?>
												<div class="input-group-append">
													<span class="input-group-text"><i class="md md-event-note"></i></span>
												</div>
											</div><span class="help-block"><small>registrations closing date & time</small></span>
										</div>
									</div>
									<div class="form-group row">
										<label for="return_date" class="col-12 col-form-label">Return Date <span class="text-danger">*</span></label>
										<div class="col-12">
											<div class="input-group">
												<?php echo form_input($return_date);?>
												<div class="input-group-append">
													<span class="input-group-text"><i class="md md-event-note"></i></span>
												</div>
											</div><span class="help-block"><small>registrations closing date & time</small></span>
										</div>
									</div>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
						<div class="card-box pricing">
							<div class="row">
								<div class="col-12">
									<h4 class="m-t-0 m-b-30 header-title">Pricing</h4>
									<div class="table-responsive">
										<table id="test-table" class="table table-condensed">
										<thead>
											<tr>
												<th>Departure From</th>
												<th>Price</th>
												<th>Sale Price</th>
												<th></th>
											</tr>
										</thead>
										<tbody id="test-body">
											<?php if(!empty($package_prices)): foreach ($package_prices as $price): ?>
											<tr id="row<?php echo $price['id'];?>">
        										<input type="hidden" name="price_id[]" value="<?php echo $price['id'];?>" />
												<td>
													<select name="departure[]" class="selectpicker" data-live-search="true"  data-style="btn-white">
														<option class="option-disabled" selected="true" value="<?php echo $price['city_id'];?>"><?php echo $price['city_name'];?></option>
														<?php if(!empty($cities)): foreach ($cities as $city): ?>
														<option value="<?php echo $city->id;?>"><?php echo $city->city_name;?></option>
														<?php endforeach; else:?>
														<option>No Records Found</option>
														<?php endif;?>
													</select>
												</td>
												<td>
													<div class="input-group">
														<div class="input-group-prepend">
															<span class="input-group-text" id="basic-addon1"><?php echo $settings->currency_unit;?></span>
														</div>
															<input name="price[]" type="text" value="<?php echo $price['price'];?>" class="form-control" placeholder="Enter package price" />
													</div>
												</td>
												<td>
													<div class="input-group">
														<div class="input-group-prepend">
															<span class="input-group-text" id="basic-addon1"><?php echo $settings->currency_unit;?></span>
														</div>
															<input name="sale_price[]" type="text" value="<?php echo $price['sale_price'];?>" class="form-control" placeholder="Enter package price" />
													</div>
												</td>
												<td width="20px">
													<a class="delete-row form-control text-danger icon-btn"><i class="icon-close"></i></a>
												</td>
											</tr>
											<?php endforeach; else:?>
											<option>No Records Found</option>
											<?php endif;?>
										</tbody>
										</table>
									</div>
									<hr class="m-t-0">
									<a id="add-row" class="icon-btn text-primary">Add another departure <i class="icon-plus"></i></a>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<h4 class="m-t-0 m-b-30 header-title">SEO</h4>
									<hr/>
									<div class="form-group row">
										<label for="destination_title" class="col-12 col-form-label">Title</label>
										<div class="col-12">
											<?php echo form_input($title);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="destination_details" class="col-12 col-form-label">Description</label>
										<div class="col-12">
											<?php echo form_textarea($description);?>
										</div>
									</div>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div><!-- end col -->
					<div class="col-lg-3 col-md-3 col-sm-12">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<a href="<?php echo base_url();?>packages/<?php echo $package->type;?>/details/<?php echo $package->id;?>" class="btn btn-inverse waves-effect waves-light pull-right" target="_blank">View</a>
								</div>
							</div>
							<div class="form-group row">
								<label for="status" class="col-12 col-form-label">Status</label>
								<div class="col-12">
									<select name="status" class="selectpicker" data-style="btn-white">
										<?php if(!empty($package) && $package->status == 'Published'): ?>
										<option selected="true" value="<?php echo $package->status;?>"><?php echo $package->status;?></option>
										<option value="Draft">Draft</option>
										<?php elseif(!empty($package) && $package->status == 'Draft'): ?>
										<option selected="true" value="<?php echo $package->status;?>"><?php echo $package->status;?></option>
										<option value="Published">Published</option>
										<?php else:?>
										<option>No Records Found</option>
										<?php endif;?>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<button class="btn btn-default btn-block waves-effect waves-light">Update</button>
								</div>
							</div>
						</div>
						<div class="card-box">
							<div class="row">
								<div class="col-md-12 portlets">
									<div class="m-b-0">
									<?php if(!empty($featured_image->thumb)){;?>
										<input type="file" class="dropify" name="file" data-height="120" data-default-file="<?php echo base_url().$featured_image->thumb;?>" data-allowed-file-extensions="jpeg jpg png"  data-show-remove="false" data-show-loader="true">
									<?php } else {;?>
										<input type="file" class="dropify" name="file" data-height="120" data-allowed-file-extensions="jpeg jpg png"  data-show-remove="false" data-show-loader="true">
									<?php } ;?>
                                        <section class="progress-demo">
    										<button id="uploadify" class="ladda-button btn btn-default btn-block waves-effect waves-light" data-imagetype="featured" data-style="expand-left">Upload</button>
                                        </section>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
	<script>
	$(document).on("click", "#uploadify", function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
		e.stopPropagation();
	 	var l = Ladda.create(this);
	 	l.start();
		var package_id = "<?php echo $this->uri->segment(4);?>";
		var uploadfile = $('.dropify')[0].files[0];
		var image_type = $(this).data('imagetype');
		
		type = uploadfile.type;
		var form_data = new FormData(); 
		form_data.append("file", uploadfile)
		form_data.append("package_id", package_id)
		form_data.append("type", image_type)
		var url = "<?php echo base_url();?>admin/ajax/gallery_image_upload";

		$.ajax({
			url: url,
			type: "POST",
			async:"false",
			dataType: "html",
			cache:false,
			contentType: false,
			processData: false,
			data: form_data, // serializes the form's elements.
			success: function(data)
			{
				data = JSON.parse(data);
				if(data.response == "yes")
				{
					l.stop();
					$.Notification.autoHideNotify(data.response_type, 'top right', 'Success', data.message);
				}
			}
		});
	});

// Add row
var row = <?php echo $prices_count;?>;
$(document).on("click", "#add-row", function () {
	var new_row = '<tr id="row' + row + '"><input type="hidden" name="price_id[]" /><td><select name="departure[]" class="selectpicker" data-live-search="true"  data-style="btn-white"><option selected="true" disabled="disabled">-- Select departure city --</option><?php if(!empty($cities)): foreach ($cities as $city): ?><option value="<?php echo $city->id;?>"><?php echo $city->city_name;?></option><?php endforeach; else:?><option>No Records Found</option><?php endif;?></select></td><td><div class="input-group"><div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><?php echo $settings->currency_unit;?></span></div><input name="price[]" type="text" class="form-control" placeholder="Enter package price"/></div></td><td><div class="input-group"><div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><?php echo $settings->currency_unit;?></span></div><input name="sale_price[]" type="text" class="form-control" placeholder="Enter package price"/></div></td><td><a class="delete-row form-control text-danger icon-btn"><i class="icon-close"></i></a></td></tr>';
	$('#test-body').append(new_row);
	$('.selectpicker').selectpicker('refresh');
	row++;
	return false;
});
	
// Remove criterion
$(document).on("click", ".delete-row", function () {
	if(row > 1) {
	$(this).closest('tr').remove();
	row--;
}
return false;
});
	</script>
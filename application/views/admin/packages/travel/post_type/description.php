<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages">Packages</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>"><?php echo $head_info['label'];?></a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>"><?php echo $package->title;?></a></li>
							<li class="breadcrumb-item active">Description</li>
						</ol>
						<h4 class="page-title">Add Description</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="table-box opport-box m-b-20">
				<a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/description">Description</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/photo_gallery">Gallery</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/features">Features</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/amenities">Amenities</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/itinerary">Itinerary</a> | <a href="<?php echo base_url();?>admin/packages/<?php echo $package->type;?>/<?php echo $package->id;?>/location">Location</a>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<form id="form" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-12">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<div class="form-group row">
										<label for="description" class="col-12 col-form-label">Description</label>
										<div class="col-12">
											<?php echo form_textarea($description);?>
										</div>
									</div>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div><!-- end col -->
					<div class="col-lg-3 col-md-3 col-sm-12">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<a href="javsscript:void(0);" class="btn btn-default btn-block waves-effect waves-light" id="add_description">Update</a>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
	<script>
	$(document).on("click", "#add_description", function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
		e.stopPropagation();
		var l = Ladda.create(this);
		l.start();
		var description = $("#description").val();
		var package_id = "<?php echo $this->uri->segment(4);?>";
		var key = "<?php echo $this->uri->segment(5);?>";
		
		var form_data = new FormData(); 
		form_data.append("description", description)
		form_data.append("package_id", package_id)
		form_data.append("key", key)
		var url = "<?php echo base_url();?>admin/ajax/update_description";

		$.ajax({
			url: url,
			type: "POST",
			async:"false",
			dataType: "html",
			cache:false,
			contentType: false,
			processData: false,
			data: form_data, // serializes the form's elements.
			success: function(data)
			{				
				data = JSON.parse(data);
				if(data.response == "yes")
				{
					l.stop();
					$.Notification.autoHideNotify('custom', 'top right', 'Success', data.message);
				}
			}
		});
	});
	</script>
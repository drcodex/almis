	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/airlines">Airlines</a></li>
							<li class="breadcrumb-item active">Edit Airline</li>
						</ol>
						<h4 class="page-title">Edit Airline</h4>
						<a href="<?php echo base_url();?>admin/airlines/add_airline" class="btn btn-default waves-effect waves-light">Add New Airline</a>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<form id="form" method="post" name="editflight" class="form-horizontal">
				<div class="row">
					<div class="col-9">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<div class="form-group row">
										<label for="airline_short_code" class="col-lg-12 col-md-12 col-sm-12 col-form-label">Short Code <span class="text-danger">*</span></label>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<?php echo form_input($airline_short_code);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="airline_name" class="col-lg-12 col-md-12 col-sm-12 col-form-label">Aircraft Name <span class="text-danger">*</span></label>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<?php echo form_input($airline_name);?>
										</div>
									</div>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div><!-- end col -->
					<div class="col-3">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<a class="btn btn-default btn-block waves-effect waves-light" onclick="document.editflight.submit()">Update</a>
								</div>
							</div>
						</div>
						<div class="card-box">
							<div class="row">
								<div class="col-md-12 portlets">
									<div class="m-b-0">
									<?php if(!empty($airline_logo->thumb)){;?>
										<input type="file" class="dropify" name="file" data-height="120" data-default-file="<?php echo base_url().$airline_logo->thumb;?>" data-allowed-file-extensions="jpeg jpg png"  data-show-remove="false">
									<?php } else {;?>
										<input type="file" class="dropify" name="file" data-height="120" data-allowed-file-extensions="jpeg jpg png"  data-show-remove="false">
									<?php } ;?>
										<span class="help-block"><small>Image size minimum 550px x 424px</small></span>
										<button id="uploadify" class="btn btn-default btn-block waves-effect waves-light">Upload</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
<script>
	$(document).on("click", "#uploadify", function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
		e.stopPropagation();
	 	var l = Ladda.create(this);
	 	l.start();
		var airline_id = "<?php echo $this->uri->segment(4);?>";
		var uploadfile = $('.dropify')[0].files[0];
		
		type = uploadfile.type;
		var form_data = new FormData(); 
		form_data.append("file", uploadfile)
		form_data.append("airline_id", airline_id)
		var url = "<?php echo base_url();?>admin/airlines/upload_file";

		$.ajax({
			url: url,
			type: "POST",
			async:"false",
			dataType: "html",
			cache:false,
			contentType: false,
			processData: false,
			data: form_data, // serializes the form's elements.
			success: function(data)
			{
				data = JSON.parse(data);
				if(data.response == "yes")
				{
					l.stop();
					$.Notification.autoHideNotify(data.response_type, 'top right', 'Success', data.message);
				}
			}
		});
	});
</script>
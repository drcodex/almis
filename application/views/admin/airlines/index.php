	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item active">Airlines</li>
						</ol>
						<h4 class="page-title">Airlines</h4>
						<a href="<?php echo base_url();?>admin/airlines/add_airline" class="btn btn-default waves-effect waves-light">Add New Airline</a>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<?php if(isset($class)){?>
			<div id="infoMessage">
				<p class="<?php echo $class;?>"><?php echo $message;?></p>
			</div>
			<?php } else {?>
				<div id="infoMessage"><?php echo $message;?></div>
			<?php };?>
			<div class="row">
				<div class="col-12">
					<div class="card-box table-responsive">
						<table id="datatable" class="table table-hover mails m-0 table table-actions-bar">
							<thead>
								<tr>
									<th>Logo</th>
									<th>Name</th>
									<th>Short Code</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($all_airlines)): foreach ($all_airlines as $airline): ?>
								<tr>
									<td>
										<?php if(!empty($airline->thumb)) {;?>
										<img src="<?php echo base_url().$airline->thumb;?>" alt="contact-img" title="contact-img" class="rounded-circle thumb-sm"></td>
										<?php } else {;?>
										<img src="<?php echo base_url();?>/assets/images/airline-default.jpg" alt="contact-img" title="contact-img" class="rounded-circle thumb-sm"></td>
										<?php };?>
									<td><?php echo $airline->airline_name;?></td>
									<td><?php echo $airline->airline_short_code;?></td>
									<td>
										<?php if($airline->status == 'Draft') {;?>
										<span class="label label-danger"><?php echo $airline->status;?></span>
										<?php } else {;?>
										<span class="label label-success"><?php echo $airline->status;?></span>
										<?php };?>
									</td>
									<td>
										<?php if($airline->status == 'Draft') {;?>
											<?php echo anchor("admin/airlines/submit_airline/".$airline->airline_id, '<i class="md md-edit"></i>', 'class="table-action-btn"') ;?>
										<?php } else {;?>
											<?php echo anchor("admin/airlines/edit_airline/".$airline->airline_id, '<i class="md md-edit"></i>', 'class="table-action-btn"') ;?>
										<?php };?>
										<?php echo anchor("admin/airlines/delete_airline/".$airline->airline_id, '<i class="md md-delete"></i>', 'class="table-action-btn"') ;?>
									</td>
								</tr>
								<?php endforeach; else:?>
								<tr>
									<td class="center text-center" colspan="6">No Records Found</td>
								</tr>
								<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div> <!-- end row -->
		</div> <!-- container -->
	</div> <!-- content -->
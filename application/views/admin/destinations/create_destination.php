<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/destinations">Destinations</a></li>
							<li class="breadcrumb-item active">Create Destination</li>
						</ol>
						<h4 class="page-title">Create Destinations</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<form id="form" name="create_destination" method="post" class="form-horizontal">
				<div class="package_image_id">
					<input type="hidden" name="featured_image_id" value="" />
				</div>
				<div class="row">
					<div class="col-9">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<div class="form-group row">
										<label for="select_id" class="col-12 col-form-label">Select <?php echo $type;?> <span class="text-danger">*</span></label>
										<div class="col-12">
											<select name="select_id" class="selectpicker" data-live-search="true"  data-style="btn-white">
												<option selected="true" disabled="disabled">-- Select <?php echo $type;?> --</option>
											<?php if(!empty($destination_list)): foreach ($destination_list as $list): ?>
												<option value="<?php echo $list->id;?>"><?php echo $list->name;?></option>
												<?php endforeach; else:?>
												<option>No Records Found</option>
												<?php endif;?>
											</select>
											<span class="help-block"><small>Select <?php echo $type;?> destination you want to add</small></span>
										</div>
									</div>
									<div class="form-group row">
										<label for="destination_title" class="col-12 col-form-label">Title <span class="text-danger">*</span></label>
										<div class="col-12">
											<?php echo form_input($destination_title);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="destination_slug" class="col-12 col-form-label">Slug <span class="text-danger">*</span></label>
										<div class="col-12">
											<?php echo form_input($destination_slug);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="destination_details" class="col-12 col-form-label">Description</label>
										<div class="col-12">
											<?php echo form_textarea($destination_details);?>
										</div>
									</div>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<h4 class="m-t-0 m-b-30 header-title">SEO</h4>
									<hr/>
									<div class="form-group row">
										<label for="destination_title" class="col-12 col-form-label">Title</label>
										<div class="col-12">
											<?php echo form_input($title);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="destination_details" class="col-12 col-form-label">Description</label>
										<div class="col-12">
											<?php echo form_textarea($description);?>
										</div>
									</div>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div><!-- end col -->
					<div class="col-3">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<h6 class="m-t-0 m-b-30">Status and Visibility</h6>
                                        <div class="table-detail">
                                            <p class="text-dark m-b-10">Visibility <span class="text-muted pull-right">Public</span></p>
                                            <p class="text-dark m-b-10">Status <span class="text-muted pull-right">Draft</span></p>
                                        </div>
									<a class="btn btn-default btn-block waves-effect waves-light" onclick="document.create_destination.submit()">Create</a>
								</div>
							</div>
						</div>
						<div class="card-box">
							<div class="row">
								<div class="col-md-12 portlets">
									<div class="m-b-0">
									<?php if(!empty($destination_image->thumb)){;?>
										<input type="file" class="dropify" name="file" data-height="120" data-default-file="<?php echo base_url().$destination_image->thumb;?>" data-allowed-file-extensions="jpeg jpg png"  data-show-remove="false">
									<?php } else {;?>
										<input type="file" class="dropify" name="file" data-height="120" data-allowed-file-extensions="jpeg jpg png"  data-show-remove="false">
									<?php } ;?>
										<span class="help-block"><small>Banner size minimum 1140px x 350px</small></span>
                                        <section class="progress-demo">
    										<button id="uploadify" class="ladda-button btn btn-default btn-block waves-effect waves-light" data-style="expand-left">Upload</button>
                                        </section>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
	<script>
	$(document).on("click", "#uploadify", function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
		e.stopPropagation
	 	var l = Ladda.create(this);
	 	l.start();
		var uploadfile = $('.dropify')[0].files[0];
		
		type = uploadfile.type;
		var form_data = new FormData(); 
		form_data.append("file", uploadfile)
		var url = "<?php echo base_url();?>admin/destinations/upload_file";

		$.ajax({
			url: url,
			type: "POST",
			async:"false",
			dataType: "html",
			cache:false,
			contentType: false,
			processData: false,
			data: form_data, // serializes the form's elements.
			success: function(data)
			{
				data = JSON.parse(data);
				if(data.response == "yes")
				{
					l.stop();
					$('.package_image_id').html(data.content);
					$.Notification.autoHideNotify(data.response_type, 'top right', 'Success', data.message);
				}
			}
		});
	});
	</script>
<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Destinations extends Backend_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));
        $config =  array(
            'encrypt_name'    => TRUE,
			'upload_path'     => "./uploads/images/destinations/",
			'allowed_types'   => "gif|jpg|png|jpeg",
			'overwrite'       => FALSE,
			'max_size'        => "2000",
			'max_height'      => "2024",
			'max_width'       => "2024"
        );
        $this->load->library('upload', $config);
        $this->load->library('image_lib');

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
			$this->load->model('destinations_model');

		$this->lang->load('auth');
	}

	public function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$get_all_city_destinations = $this->destinations_model->get_all_city_destinations();
			$get_all_country_destinations = $this->destinations_model->get_all_country_destinations();
			//list the users
			$this->data['all_destinations'] = array_merge($get_all_city_destinations, $get_all_country_destinations);
			//echo '<pre>'; print_r($this->data['all_destinations']); echo '</pre>'; die();
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
	}
	
	/**
	 * Create a Select Country
	 */
	public function select_destination_type()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$select_destination_type = $this->input->post('select_destination_type');
				
				$this->form_validation->set_rules('select_destination_type','Type','trim|required');
				
				if ($this->form_validation->run() === TRUE)
				{
					$session_data = $this->session->userdata;
					$user_id = $session_data['user_id'];
					$additional_data = array(
						'type' => $select_destination_type,
						'select_id' => NULL,
						'title' => NULL,
						'slug' => NULL,
						'description' => NULL,
						'author' => $user_id,
						'status' => 'Draft',
						'date' => date('Y-m-d H:i:s'),
					);
					//echo '<pre>'; print_r($last_id); echo '</pre>'; die();
					$last_id = $this->destinations_model->add_destination($additional_data);
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect('admin/destinations/edit_destination/'.$last_id);
				}
			}
			$this->data['get_cities'] = $this->destinations_model->get_cities();
			//echo '<pre>'; print_r($this->data['get_cities']); echo '</pre>'; die();
			$this->data['title'] = $this->lang->line('create_user_heading');
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'select_destination_type', $this->data);
		}
	}
	
	public function edit_destination($destination_id)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$select_id = $this->input->post('select_id');
				$destination_title = $this->input->post('destination_title');
				$destination_slug = $this->input->post('destination_slug');
				$destination_details = $this->input->post('destination_details');
				$status = $this->input->post('status');
			
				$this->form_validation->set_rules('destination_title','Destination Title','trim');
				$this->form_validation->set_rules('destination_slug','Destination Slug','trim');
				$this->form_validation->set_rules('destination_details','Destination Details','trim');

				if ($this->form_validation->run() === TRUE)
				{
					$additional_data = array(
						'select_id' => $select_id,
						'title' => $destination_title,
						'slug' => $destination_slug,
						'description' => $destination_details,
						'status' => $status,
						'date' => date('Y-m-d H:i:s'),
					);
					//echo '<pre>'; print_r($additional_data); echo '</pre>'; die();
					$this->destinations_model->update_destination($destination_id, $additional_data);
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect("admin/destinations/edit_destination/" . $destination_id, 'refresh');
				}
			}
			$dest = $this->destinations_model->get_destination_by_id($destination_id);
			//echo '<pre>'; print_r($dest); echo '</pre>'; die();
			$this->data['destination_title'] = array(
				'name' => 'destination_title',
				'id' => 'destination_title',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_title', !empty($dest->title) ? $dest->title : ""),
				'class' => 'form-control"',
				'placeholder' => 'Add Title',
			);
			$this->data['destination_slug'] = array(
				'name' => 'destination_slug',
				'id' => 'destination_slug',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_slug', !empty($dest->slug) ? $dest->slug : ""),
				'class' => 'form-control"',
				'placeholder' => 'Add Slug',
			);
			$this->data['destination_details'] = array(
				'name' => 'destination_details',
				'id' => 'destination_details',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_details', !empty($dest->description) ? $dest->description : ""),
				'class' => 'summernote"',
				'placeholder' => 'Start adding text or html here',
			);
			$destinations = $this->destinations_model->get_destinations_by_id($destination_id);
			$destinations_select = $destinations->select_id;
			$destinations_type = $destinations->type;
			switch ($destinations_type) {
				case "city":
					$this->data['location'] = $this->destinations_model->get_city_by_id($destinations_select);
					$this->data['destination_list'] = $this->destinations_model->get_cities_convent();
					//echo '<pre>'; print_r($this->data['destination_list']); echo '</pre>'; die();
					break;
				case "country":
					$this->data['location'] = $this->destinations_model->get_country_by_id($destinations_select);
					$this->data['destination_list'] = $this->destinations_model->get_countries_convent();
					//echo '<pre>'; print_r($this->data['destination_list']); echo '</pre>'; die();
					break;
				case "continent":
					$this->data['location'] = $this->destinations_model->get_continent_by_id($destinations_select);
					$this->data['destination_list'] = $this->destinations_model->get_continents_convent();
					//echo '<pre>'; print_r($this->data['destination_list']); echo '</pre>'; die();
					break;
			}
			$destination_image = $this->destinations_model->get_destination_image($destination_id);
			$this->data['destination_image'] = $destination_image;
			$this->data['destination'] = $dest;
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'edit_destination', $this->data);
		}
	}
	/**
	 * Create a Select Country
	 */
	public function select_city()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$city_id = $this->input->post('city_id');
				$destination = $this->destinations_model->get_destination_by_city_id($city_id);
				if(!empty($destination))
				{
					redirect('admin/destinations/edit_city_destination/'.$destination->id);
				}
				else
				{
					$this->form_validation->set_rules('city_id','City','trim|required');
					if ($this->form_validation->run() === TRUE)
					{
						$session_data = $this->session->userdata;
						$user_id = $session_data['user_id'];
						$additional_data = array(
							'city_id' => $city_id,
							'title' => NULL,
							'slug' => NULL,
							'description' => NULL,
							'author' => $user_id,
							'status' => 'Draft',
							'date' => date('Y-m-d H:i:s'),
						);
						//echo '<pre>'; print_r($last_id); echo '</pre>'; die();
						$last_id = $this->destinations_model->add_destination($additional_data);
						$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
						redirect('admin/destinations/create_city_destination/'.$last_id);
					}
				}
			}
			$this->data['destination_title'] = array(
				'name' => 'destination_title',
				'id' => 'destination_title',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_title'),
				'class' => 'form-control"',
				'placeholder' => 'Add Title',
			);
			$this->data['destination_slug'] = array(
				'name' => 'destination_slug',
				'id' => 'destination_slug',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_slug'),
				'class' => 'form-control"',
				'placeholder' => 'Add Slug',
			);
			$this->data['destination_details'] = array(
				'name' => 'destination_details',
				'id' => 'destination_details',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_details'),
				'class' => 'summernote"',
				'placeholder' => 'Start adding text or html here',
			);
			$this->data['get_cities'] = $this->destinations_model->get_cities();
			//echo '<pre>'; print_r($this->data['get_cities']); echo '</pre>'; die();
			$this->data['title'] = $this->lang->line('create_user_heading');
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'select_city', $this->data);
		}
	}
	/**
	 * Create a new News
	 */
	public function create_city_destination($destination_id = NULL)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$destination = $this->destinations_model->get_destination_by_id_join_city($destination_id);
			//echo '<pre>'; print_r($destination); echo '</pre>'; die();
			if(empty($destination) || empty($destination_id))
			{
				redirect('admin/destinations/select_city');
			}
			else
			{
				if($_POST)
				{
					$destination_title = $this->input->post('destination_title');
					$destination_slug = $this->input->post('destination_slug');
					$destination_details = $this->input->post('destination_details');
				
					// validate form input
					$this->form_validation->set_rules('destination_title','Destination Title','trim|required');
					$this->form_validation->set_rules('destination_slug','Destination Slug','trim|required');
					$this->form_validation->set_rules('destination_details','Destination Details','trim');

					if ($this->form_validation->run() === TRUE)
					{
						$additional_data = array(
							'title' => $destination_title,
							'slug' => $destination_slug,
							'description' => $destination_details,
							'status' => 'Published',
							'date' => date('Y-m-d H:i:s'),
						);
						$this->destinations_model->update_destination($destination_id, $additional_data);
						$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
						redirect("admin/news/create_news", 'refresh');
					}
				}
				$this->data['destination_title'] = array(
					'name' => 'destination_title',
					'id' => 'destination_title',
					'type' => 'text',
					'value' => $this->form_validation->set_value('destination_title'),
					'class' => 'form-control"',
					'placeholder' => 'Add Title',
				);
				$this->data['destination_slug'] = array(
					'name' => 'destination_slug',
					'id' => 'destination_slug',
					'type' => 'text',
					'value' => $this->form_validation->set_value('destination_slug'),
					'class' => 'form-control"',
					'placeholder' => 'Add Slug',
				);
				$this->data['destination_details'] = array(
					'name' => 'destination_details',
					'id' => 'destination_details',
					'type' => 'text',
					'value' => $this->form_validation->set_value('destination_details'),
					'class' => 'summernote"',
					'placeholder' => 'Start adding text or html here',
				);
				$this->data['title'] = $this->lang->line('create_user_heading');
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
				$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'create_city_destination', $this->data);
			}
		}
	}
	public function edit_city_destination($destination_id)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$destination_title = $this->input->post('destination_title');
				$destination_slug = $this->input->post('destination_slug');
				$destination_details = $this->input->post('destination_details');
				$status = $this->input->post('status');
			
				$this->form_validation->set_rules('destination_title','Destination Title','trim');
				$this->form_validation->set_rules('destination_slug','Destination Slug','trim');
				$this->form_validation->set_rules('destination_details','Destination Details','trim');

				if ($this->form_validation->run() === TRUE)
				{
					$additional_data = array(
						'title' => $destination_title,
						'slug' => $destination_slug,
						'description' => $destination_details,
						'status' => $status,
						'date' => date('Y-m-d H:i:s'),
					);
					//echo '<pre>'; print_r($additional_data); echo '</pre>'; die();
					$this->destinations_model->update_destination($destination_id, $additional_data);
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect("admin/destinations/edit_city_destination/" . $destination_id, 'refresh');
				}
			}
			$dest = $this->destinations_model->get_destination_by_id_join_city($destination_id);
			//echo '<pre>'; print_r($dest); echo '</pre>'; die();
			$this->data['destination_title'] = array(
				'name' => 'destination_title',
				'id' => 'destination_title',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_title', !empty($dest->title) ? $dest->title : ""),
				'class' => 'form-control"',
				'placeholder' => 'Add Title',
			);
			$this->data['destination_slug'] = array(
				'name' => 'destination_slug',
				'id' => 'destination_slug',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_slug', !empty($dest->slug) ? $dest->slug : ""),
				'class' => 'form-control"',
				'placeholder' => 'Add Slug',
			);
			$this->data['destination_details'] = array(
				'name' => 'destination_details',
				'id' => 'destination_details',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_details', !empty($dest->description) ? $dest->description : ""),
				'class' => 'summernote"',
				'placeholder' => 'Start adding text or html here',
			);
			$destination_image = $this->destinations_model->get_destination_image($destination_id);
			$this->data['destination_image'] = $destination_image;
			$this->data['destination'] = $dest;
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'edit_city_destination', $this->data);
		}
	}
	/**
	 * Create a Select Country
	 */
	public function select_country()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$country_id = $this->input->post('country_id');
				$destination = $this->destinations_model->get_destination_by_country_id($country_id);
				//echo '<pre>'; print_r($destination); echo '</pre>'; die();
				if(!empty($destination))
				{
					redirect('admin/destinations/edit_country_destination/'.$destination->id);
				}
				else
				{
					$this->form_validation->set_rules('country_id','Country','trim|required');
					if ($this->form_validation->run() === TRUE)
					{
						$session_data = $this->session->userdata;
						$user_id = $session_data['user_id'];
						$additional_data = array(
							'country_id' => $country_id,
							'title' => NULL,
							'slug' => NULL,
							'description' => NULL,
							'author' => $user_id,
							'status' => 'Draft',
							'date' => date('Y-m-d H:i:s'),
						);
						$last_id = $this->destinations_model->add_destination($additional_data);
						$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
						redirect('admin/destinations/create_destination/'.$last_id);
					}
				}
			}
			$this->data['destination_title'] = array(
				'name' => 'destination_title',
				'id' => 'destination_title',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_title'),
				'class' => 'form-control"',
				'placeholder' => 'Add Title',
			);
			$this->data['destination_slug'] = array(
				'name' => 'destination_slug',
				'id' => 'destination_slug',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_slug'),
				'class' => 'form-control"',
				'placeholder' => 'Add Slug',
			);
			$this->data['destination_details'] = array(
				'name' => 'destination_details',
				'id' => 'destination_details',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_details'),
				'class' => 'summernote"',
				'placeholder' => 'Start adding text or html here',
			);
			$this->data['get_countries'] = $this->destinations_model->get_countries();
			//echo '<pre>'; print_r($this->data['get_countries']); echo '</pre>'; die();
			$this->data['title'] = $this->lang->line('create_user_heading');
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'select_country', $this->data);
		}
	}
	/**
	 * Create a new News
	 */
	public function create_country_destination($destination_id = NULL)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$destination = $this->destinations_model->get_destination_by_id($destination_id);
			echo '<pre>'; print_r($destination); echo '</pre>'; die();
			if(empty($destination) || empty($destination_id))
			{
				redirect('admin/destinations/select_country');
			}
			else
			{
				if($_POST)
				{
					$destination_title = $this->input->post('destination_title');
					$destination_slug = $this->input->post('destination_slug');
					$destination_details = $this->input->post('destination_details');
				
					// validate form input
					$this->form_validation->set_rules('destination_title','Destination Title','trim|required');
					$this->form_validation->set_rules('destination_slug','Destination Slug','trim|required');
					$this->form_validation->set_rules('destination_details','Destination Details','trim');

					if ($this->form_validation->run() === TRUE)
					{
						$additional_data = array(
							'title' => $destination_title,
							'slug' => $destination_slug,
							'description' => $destination_details,
							'status' => 'Published',
							'date' => date('Y-m-d H:i:s'),
						);
						$this->destinations_model->update_destination($additional_data);
						$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
						redirect("admin/news/create_news", 'refresh');
					}
				}
				$this->data['destination_title'] = array(
					'name' => 'destination_title',
					'id' => 'destination_title',
					'type' => 'text',
					'value' => $this->form_validation->set_value('destination_title'),
					'class' => 'form-control"',
					'placeholder' => 'Add Title',
				);
				$this->data['destination_slug'] = array(
					'name' => 'destination_slug',
					'id' => 'destination_slug',
					'type' => 'text',
					'value' => $this->form_validation->set_value('destination_slug'),
					'class' => 'form-control"',
					'placeholder' => 'Add Slug',
				);
				$this->data['destination_details'] = array(
					'name' => 'destination_details',
					'id' => 'destination_details',
					'type' => 'text',
					'value' => $this->form_validation->set_value('destination_details'),
					'class' => 'summernote"',
					'placeholder' => 'Start adding text or html here',
				);
				$this->data['title'] = $this->lang->line('create_user_heading');
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
				$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'create_country_destination', $this->data);
			}
		}
	}
	public function edit_country_destination($destination_id)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			if($_POST)
			{
				$destination_title = $this->input->post('destination_title');
				$destination_slug = $this->input->post('destination_slug');
				$destination_details = $this->input->post('destination_details');
				$status = $this->input->post('status');
			
				$this->form_validation->set_rules('destination_title','Destination Title','trim');
				$this->form_validation->set_rules('destination_slug','Destination Slug','trim');
				$this->form_validation->set_rules('destination_details','Destination Details','trim');

				if ($this->form_validation->run() === TRUE)
				{
					$additional_data = array(
						'title' => $destination_title,
						'slug' => $destination_slug,
						'description' => $destination_details,
						'status' => $status,
						'date' => date('Y-m-d H:i:s'),
					);
					//echo '<pre>'; print_r($additional_data); echo '</pre>'; die();
					$this->destinations_model->update_destination($destination_id, $additional_data);
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect("admin/destinations/edit_country_destination/" . $destination_id, 'refresh');
				}
			}
			$dest = $this->destinations_model->get_destination_by_id_join_country($destination_id);
			//echo '<pre>'; print_r($dest); echo '</pre>'; die();
			$this->data['destination_title'] = array(
				'name' => 'destination_title',
				'id' => 'destination_title',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_title', !empty($dest->title) ? $dest->title : ""),
				'class' => 'form-control"',
				'placeholder' => 'Add Title',
			);
			$this->data['destination_slug'] = array(
				'name' => 'destination_slug',
				'id' => 'destination_slug',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_slug', !empty($dest->slug) ? $dest->slug : ""),
				'class' => 'form-control"',
				'placeholder' => 'Add Slug',
			);
			$this->data['destination_details'] = array(
				'name' => 'destination_details',
				'id' => 'destination_details',
				'type' => 'text',
				'value' => $this->form_validation->set_value('destination_details', !empty($dest->description) ? $dest->description : ""),
				'class' => 'summernote"',
				'placeholder' => 'Start adding text or html here',
			);
			$destination_image = $this->destinations_model->get_destination_image($destination_id);
			$this->data['destination_image'] = $destination_image;
			$this->data['destination'] = $dest;
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render_page('admin' . DIRECTORY_SEPARATOR . 'destinations' . DIRECTORY_SEPARATOR . 'edit_country_destination', $this->data);
		}
	}
	
	public function delete_destination($id)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['posts'] = $this->destinations_model->delete_news($id);
			redirect("admin/news/all_news/", 'refresh');
		}
	}
    public function upload_file()
    {
        $config['upload_path'] = './uploads/images/destinations/';

        $this->load->library('upload', $config);
        if($this->upload->do_upload('file'))
        {
            $file = $this->upload->data();
            $thumb_config["image_library"] = "gd2";
            $thumb_config["source_image"] = $file["full_path"];
            $thumb_config['new_image'] = 'uploads/images/destinations/thumb/'.$file['file_name'];
            $thumb_config['create_thumb'] = TRUE;
            $thumb_config['thumb_marker'] = '';
            $thumb_config['maintain_ratio'] = TRUE;
            $thumb_config['width'] = 550;
            $thumb_config['height'] = 350;
            $this->load->library('image_lib', $thumb_config);
            $this->image_lib->initialize($thumb_config);
            $this->image_lib->resize();
            $this->image_lib->clear();
			$destination_id = $this->input->post('destination_id');
            $data = array (
                'title' => $file['orig_name'],
                'path' => 'uploads/images/destinations/'.$file['file_name'],
                'ext' => $file['file_ext'],
                'thumb' => $thumb_config['new_image'],
                'size' => $file['file_size'],
	            'destination_id' => $destination_id,
	            'created_at' => date('Y-m-d h:i:s')
            );
			$this->destinations_model->upload_destination_image($destination_id, $data);
        }
    }
}

<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/destinations">Destinations</a></li>
							<li class="breadcrumb-item active">Select Destination Type</li>
						</ol>
						<h4 class="page-title">Select Destination Type</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<form id="form" name="select_type" method="post" class="form-horizontal">
				<div class="row justify-content-md-center">
					<div class="col-6">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<div class="form-group row">
										<label for="select_destination_type" class="col-12 col-form-label">Select Destination Type</label>
										<div class="col-12">
											<select name="select_destination_type" class="selectpicker" data-live-search="true"  data-style="btn-white">
												<option selected="true" disabled="disabled">-- Select Destination Type --</option>
												<option value="city">Add City</option>
												<option value="country">Add Country</option>
												<option value="continent">Add Continent</option>
											</select>
											<span class="help-block"><small>Select destination city you want to add</small></span>
										</div>
									</div>
									<a class="btn btn-default btn-block waves-effect waves-light" onclick="document.select_type.submit()">Next</a>
								</div>
							</div>
						</div>
					</div><!-- end col -->
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item active">Destinations</li>
						</ol>
						<h4 class="page-title">All Destinations</h4>
						<a href="<?php echo base_url();?>admin/destinations/select_destination_type" class="btn btn-default waves-effect waves-light">Add New Destination</a>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<div class="row">
				<div class="col-lg-12">
					<div class="card-box">
						<?php if($all_destinations):?>
						<div class="table-responsive">
							<table id="datatable" class="table table-hover mails m-0 table table-actions-bar">
								<thead>
									<tr>
										<th>Destination</th>
										<th>type</th>
										<th>Title</th>
										<th>Slug</th>
										<th>Status</th>
										<th>Featured</th>
										<th width="130px">Action</th>
									</tr>
								</thead>
								<tbody>
								<?php foreach($all_destinations as $destination):?>
									<tr>
										<td>
											<?php if(!empty($destination['country_name'])) {;?>
											<?php echo $destination['country_name'];?>
											<?php } elseif(!empty($destination['city_name'])) { ;?>
											<?php echo $destination['city_name'];?>
											<?php } elseif(!empty($destination['continent_name'])) { ;?>
											<?php echo $destination['continent_name'];?>
											<?php } else { ;?>
												(no title)
											<?php } ;?>
										</td>
										<td>
											<?php if(!empty($destination['type'])) {;?>
											<?php echo $destination['type'];?>
											<?php } else { ;?>
											<?php } ;?>
										</td>
										<td>
											<?php if(!empty($destination['title'])) {;?>
											<?php echo $destination['title'];?>
											<?php } else { ;?>
											<?php } ;?>
										</td>
										<td><?php echo $destination['slug'];?></td>
										<?php if($destination['status'] == 'Published') {;?>
										<td><span class="label label-success"><?php echo $destination['status'];?></span></td>
										<?php } else { ;?>
										<td><span class="label label-danger"><?php echo $destination['status'];?></span></td>
										<?php } ;?>
										<td>
											<input class="check" type="checkbox" data-plugin="switchery" data-color="#81c868" data-secondary-color="#ED5565" data-size="small" data-id-target="<?php echo $destination['destination_id'];?>" value="<?php echo $destination['destination_id'];?>" <?php if($destination['featured'] == '1') echo 'checked'; ?>/>
										</td>
										<td>
										
										<?php if(!empty($destination['slug'])) {;?>
											<?php echo anchor("destinations/route/".$destination['slug'], '<i class="md md-remove-red-eye"></i>', array('class' => 'table-action-btn', 'target' => '_blank')) ;?>
										<?php } else { ;?>
										<?php } ;?>
										<?php if(!empty($destination['destination_id'])) {;?>
										<?php echo anchor("admin/destinations/edit_destination/".$destination['destination_id'], '<i class="md md-edit"></i>', 'class="table-action-btn"') ;?>
										<?php } else { ;?>
										<?php } ;?>
										<?php echo anchor("admin/destinations/delete_destination/".$destination['destination_id'], '<i class="md md-delete"></i>', 'class="table-action-btn"') ;?></td>
									</tr>
								<?php endforeach;?>
								
								<tbody>
								</tbody>
							</table>
						</div>
						<? else:?>
							There is no Destination yet.
						<?php endif;?>
					</div>
				</div> <!-- end col -->
			</div>
		</div> <!-- container -->
	</div> <!-- content -->
	<script type="text/javascript">
	$(document).on("change", ".check", function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
			//var featured = $(".check").val();
			var featured = $(this).data("id-target");
			var checkStatus = this.checked ? 'true' : 'false';
			var url = "<?php echo base_url();?>admin/ajax/change_featured";

		$.ajax({
			url: url,
			type: "POST",
			data:{
                featured:featured,
                checked_in:checkStatus,
            },
			success: function(data) {
				if(data.content == "enabled")
				{
					$.Notification.autoHideNotify('success', 'top right', 'Action', data.message);
				}
				else
				{
					$.Notification.autoHideNotify('error', 'top right', 'Action', data.message);
				}
			},
		});
	});
	</script>
	<div id="LowestFare" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			<form id="form" method="post" class="form-horizontal">
				<div class="modal-header">
					<h4 class="page-title" id="myModalLabel">Lowest Fares</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<input type="hidden" id="position" name="position" value="home_lowest_fares">
					<div class="form-group row">
						<label for="block_title" class="col-12 col-form-label">List Title <span class="text-danger">*</span></label>
						<div class="col-12">
							<?php echo form_input($block_title);?>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-12">
							<select name="term_id" id="term_id" class="selectpicker" data-live-search="true"  data-style="btn-white">
								<option selected="true" disabled="disabled">-- Select category --</option>
								<?php if(!empty($terms)): foreach ($terms as $term): ?>
								<option value="<?php echo $term['term_taxonomy_id'];?>"><?php echo $term['name'];?></option>
								<?php endforeach; else:?>
								<option>No Records Found</option>
								<?php endif;?>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javsscript:void(0);" id="add_block_item" class="btn btn-default waves-effect waves-light" data-modalid="#LowestFare">Add Block</a>
				</div>
			</div><!-- /.modal-content -->
			<?php echo form_close();?>
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
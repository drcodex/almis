
<?php if(!empty($flight_offers)): foreach ($flight_offers as $item): ?>
<div class="card-box card-block m-b-10">
	<div class="table-box opport-box">
		<div class="table-detail title-detail">
			<div class="member-info">
				<h4 class="m-t-0 m-b-0">
					<a href="#">
						<span class="label label-pink pull-left m-r-10"><?php echo $item->position;?></span>
					</a>
					<b><?php echo $item->title;?></b>
				</h4>
			</div>
		</div>
		<div class="table-detail table-actions-bar">
			<a href="javsscript:void(0);" class="table-action-btn pull-right" id="delete_block_item" data-position="<?php echo $item->position;?>" data-offerblockid="<?php echo $item->id;?>" data-spinner-color="#7e57c2" data-spinner-size="15px" data-spinner-lines="8" class="table-action-btn"><i class="md md-delete"></i></a>
		</div>
	</div>
</div>
<?php endforeach; else:?>
	<p class="m-t-0 m-b-0">No flight offer list found</p>
<?php endif;?>
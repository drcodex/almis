	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="page-title"><?php echo lang('edit_user_heading');?></h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
						<li class="breadcrumb-item active">Users</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="card-box">
						<div class="row">
							<div class="col-12">
								<div class="p-20">
									<div id="infoMessage"><?php echo $message;?></div>
									<?php $attributes = array('class' => 'form-horizontal');?>
									<?php echo form_open(uri_string(), $attributes);?>
									<div class="form-group row">
										<?php echo lang('edit_user_fname_label', 'first_name', 'class="col-2 col-form-label"');?>
										<div class="col-10">
											<?php echo form_input($first_name);?>
										</div>
									</div>
									<div class="form-group row">
										<?php echo lang('edit_user_lname_label', 'last_name', 'class="col-2 col-form-label"');?>
										<div class="col-10">
											<?php echo form_input($last_name);?>
										</div>
									</div>
									<div class="form-group row">
										<?php echo lang('edit_user_company_label', 'company', 'class="col-2 col-form-label"');?>
										<div class="col-10">
											<?php echo form_input($company);?>
										</div>
									</div>
									<div class="form-group row">
										<?php echo lang('edit_user_phone_label', 'phone', 'class="col-2 col-form-label"');?>
										<div class="col-10">
											<?php echo form_input($phone);?>
										</div>
									</div>
									<div class="form-group row">
										<?php echo lang('edit_user_password_label', 'password', 'class="col-2 col-form-label"');?>
										<div class="col-10">
											<?php echo form_input($password);?>
										</div>
									</div>
									<div class="form-group row">
										<?php echo lang('edit_user_password_confirm_label', 'password_confirm', 'class="col-2 col-form-label"');?>
										<div class="col-10">
											<?php echo form_input($password_confirm);?>
										</div>
									</div>
									<?php echo form_hidden('id', $user->id);?>
									<?php echo form_hidden($csrf); ?>
									<?php $submitattr = array(
										'class' => 'btn btn-primary',
										'type' => 'submit'
									);?>
								</div>
							</div>
						</div><!-- end row -->
					</div>
					<div class="card-box">
						<div class="row">
							<div class="col-12">
								<div class="p-20">
								<h4 class="header-title"><?php echo lang('edit_user_groups_heading');?></h4>
									<div class="mt-3 mb-3">
										<?php if ($this->ion_auth->is_admin()): ?>
										<?php foreach ($groups as $group):?>
											<div class="custom-control custom-checkbox">
												<?php
													$gID=$group['id'];
													$checked = null;
													$item = null;
													foreach($currentGroups as $grp) {
														if ($gID == $grp->id) {
															$checked= ' checked="checked"';
														break;
														}
													}
												?>
												<input type="checkbox" class="custom-control-input" name="groups[]" value="<?php echo $group['id'];?>" id="customCheck<?php echo $group['id'];?>" <?php echo $checked;?>>
												<label class="custom-control-label" for="customCheck<?php echo $group['id'];?>">
												<?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?></label>
											</div>
										<?php endforeach?>
										<?php endif ?>
									</div>
									<?php echo form_submit($submitattr, lang('edit_user_submit_btn'));?>
									<?php echo form_close();?>
								</div>
							</div>
						</div><!-- end row -->
					</div>
				</div> <!-- end col -->
			</div>
		</div> <!-- container -->
	</div> <!-- content -->
	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="page-title"><?php echo lang('create_user_heading');?></h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Users</a></li>
						<li class="breadcrumb-item active">Create New User</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="card-box">
						<div class="row">
							<div class="col-12">
								<div class="p-20">
									<div id="infoMessage"><?php echo $message;?></div>
									<?php $attributes = array('class' => 'form-horizontal');?>
									<?php echo form_open("admin/auth/create_user", $attributes);?>
									<div class="form-group row">
										<?php echo lang('create_user_fname_label', 'first_name', 'class="col-2 col-form-label"');?>
										<div class="col-10">
											<?php echo form_input($first_name);?>
										</div>
									</div>
									<div class="form-group row">
										<?php echo lang('create_user_lname_label', 'last_name', 'class="col-2 col-form-label"');?>
										<div class="col-10">
											<?php echo form_input($last_name);?>
										</div>
									</div>
									<?php
										if($identity_column!=='email') {
										echo '<p>';
										echo lang('create_user_identity_label', 'identity');
										echo '<br />';
										echo form_error('identity');
										echo form_input($identity);
										echo '</p>';
										}
									?>
									<div class="form-group row">
										<?php echo lang('create_user_company_label', 'company', 'class="col-2 col-form-label"');?>
										<div class="col-10">
											<?php echo form_input($company);?>
										</div>
									</div>
									<div class="form-group row">
										<?php echo lang('create_user_email_label', 'email', 'class="col-2 col-form-label"');?>
										<div class="col-10">
											<?php echo form_input($email);?>
										</div>
									</div>
									<div class="form-group row">
										<?php echo lang('create_user_phone_label', 'phone', 'class="col-2 col-form-label"');?>
										<div class="col-10">
											<?php echo form_input($phone);?>
										</div>
									</div>
									<div class="form-group row">
										<?php echo lang('create_user_password_label', 'password', 'class="col-2 col-form-label"');?>
										<div class="col-10">
											<?php echo form_input($password);?>
										</div>
									</div>
									<div class="form-group row">
										<?php echo lang('create_user_password_confirm_label', 'password_confirm', 'class="col-2 col-form-label"');?>
										<div class="col-10">
											<?php echo form_input($password_confirm);?>
										</div>
									</div>
									<?php $submitattr = array(
										'class' => 'btn btn-primary',
										'type' => 'submit'
									);?>
									<?php echo form_submit($submitattr, lang('create_user_submit_btn'));?>
									<?php echo form_close();?>
								</div>
							</div>
						</div><!-- end row -->
					</div> <!-- end card-box -->
				</div><!-- end col -->
			</div><!-- end row -->
		</div> <!-- container -->
	</div> <!-- content -->
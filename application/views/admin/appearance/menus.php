<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header-2">
                    <ol class="breadcrumb pull-right mb-0">
                        <li class="breadcrumb-item active">Menus</li>
                    </ol>
                    <h4 class="page-title">Menus</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <?php if(isset($class)){?>
        <div id="infoMessage">
            <p class="<?php echo $class;?>"><?php echo $message;?></p>
        </div>
        <?php }else{?>
        <div id="infoMessage"><?php echo $message;?></div>
        <?php };?>
        <form id="form" name="editflight" method="post" class="form-horizontal">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-12">
								<div id="accordion">
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
											<a href="#collapseOne" class="waves-effect" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne"><span> Packages </span></a>
                                        </div>
                            
                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">
												<div class="form-group row">
													<label for="protection_name"
														class="col-lg-2 col-md-2 col-sm-12 col-form-label">URL</label>
													<div class="col-lg-10 col-md-10 col-sm-12">
												<?php echo form_input($url);?>
													</div>
												</div>
												<div class="form-group row">
													<label for="protection_description"
														class="col-lg-2 col-md-2 col-sm-12 col-form-label">Text</label>
													<div class="col-lg-10 col-md-10 col-sm-12">
												<?php echo form_input($url_text);?>
													</div>
												</div>
												<a href="#" class="btn btn-default btn-block waves-effect waves-light"
													onclick="document.editflight.submit()">Add to menu</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
											<a href="#collapseTwo" class="waves-effect" data-toggle="collapse" aria-expanded="true" aria-controls="collapseTwo"><span> Custom Links </span></a>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                            <div class="card-body">
                                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,
                                                non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
                                                tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil
                                                anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan
                                                excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                                                you probably haven't heard of them accusamus labore sustainable VHS.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- end row -->
                    </div> <!-- end card-box -->
                </div><!-- end col -->
                <div class="col-lg-7 col-md-7 col-sm-12">
                    <div class="card-box table-responsive">
                        <div class="row">
							<div class="col-md-12">
								<div class="custom-dd dd" id="nestable_list_1">
									<ol class="dd-list">
										<li class="dd-item" data-id="1">
											<div class="dd-handle">
												Choose a smartwatch
											</div>
										</li>
										<li class="dd-item" data-id="2">
											<div class="dd-handle">
												Send design for review
											</div>
											<ol class="dd-list">
												<li class="dd-item" data-id="3">
													<div class="dd-handle">
														Coffee with the team
													</div>
												</li>
												<li class="dd-item" data-id="4">
													<div class="dd-handle">
														Ready my new work
													</div>
												</li>
												<li class="dd-item" data-id="5">
													<div class="dd-handle">
														Make a wireframe
													</div>
													<ol class="dd-list">
														<li class="dd-item" data-id="6">
															<div class="dd-handle">
																Video app redesign
															</div>
														</li>
														<li class="dd-item" data-id="7">
															<div class="dd-handle">
																iOS apps design completed
															</div>
														</li>
														<li class="dd-item" data-id="8">
															<div class="dd-handle">
																Dashboard design started
															</div>
														</li>
													</ol>
												</li>
												<li class="dd-item" data-id="9">
													<div class="dd-handle">
														Homepage design
													</div>
												</li>
												<li class="dd-item" data-id="10">
													<div class="dd-handle">
														Developed UI Kit
													</div>
												</li>
											</ol>
										</li>

									</ol>
								</div>
							</div><!-- end col -->
                        </div>
                    </div>
                </div>
            </div><!-- end row -->
            <?php echo form_close();?>
    </div> <!-- container -->
</div> <!-- content -->
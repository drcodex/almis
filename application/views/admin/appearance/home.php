<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item active">Home Appearance</li>
						</ol>
						<h4 class="page-title">Home Appearance</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<form id="form" name="email_config" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-9">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<h4 class="m-t-0 m-b-30 header-title">Best Flight Offers</h4>
									<hr>
									<select multiple="multiple" class="multi-select" id="destination_select" name="destination_select[]" data-plugin="multiselect">
										<?php if(!empty($destinations)): foreach ($destinations as $destination):?>
											<option value="<?php echo $destination['id'];?>" <?php if(!empty($destination['relation_value'])): ?>
											<?php echo 'selected' ?>
											<?php else: ?>
											<?php endif ?>><?php echo $destination['name'];?></option>
										<?php endforeach; else:?>
										No Destination Found
										<?php endif;?>
									</select>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<h4 class="m-t-0 m-b-10 page-title">Flight Offers</h4>
									<a class="btn btn-default waves-effect waves-light load_modal" data-modaluri="home_flight_offers" data-toggle="modal" data-target="#myModal">Add Offer List</a>
									<hr>
								</div>
								<div class="col-12 home_flight_offers">
									<?php if(!empty($flight_offers)): foreach ($flight_offers as $item): ?>
									<div class="card-box card-block m-b-10">
										<div class="table-box opport-box">
											<div class="table-detail title-detail">
												<div class="member-info">
													<h4 class="m-t-0 m-b-0">
														<a href="#">
															<span class="label label-pink pull-left m-r-10"><?php echo $item->position;?></span>
														</a>
														<b><?php echo $item->title;?></b>
													</h4>
												</div>
											</div>
											<div class="table-detail table-actions-bar">
												<a href="javsscript:void(0);" class="table-action-btn pull-right" id="delete_block_item" data-position="home_flight_offers" data-offerblockid="<?php echo $item->id;?>" data-spinner-color="#7e57c2" data-spinner-size="15px" data-spinner-lines="8" class="table-action-btn"><i class="md md-delete"></i></a>
											</div>
										</div>
									</div>
									<?php endforeach; else:?>
										<p class="m-t-0 m-b-0">No flight offer list found</p>
									<?php endif;?>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<h4 class="m-t-0 m-b-10 page-title">Lowest Fares</h4>
									<a class="btn btn-default waves-effect waves-light load_modal" data-toggle="modal" data-target="#LowestFare" data-modaluri="home_lowest_fares">Add Lowest Fares List</a>
									<hr>
								</div>
								<div class="col-12 home_lowest_fares">
									<?php if(!empty($lowest_fares)): foreach ($lowest_fares as $item): ?>
									<div class="card-box card-block m-b-10">
										<div class="table-box opport-box">
											<div class="table-detail title-detail">
												<div class="member-info">
													<h4 class="m-t-0 m-b-0">
														<a href="#">
															<span class="label label-pink pull-left m-r-10"><?php echo $item->position;?></span>
														</a>
														<b><?php echo $item->title;?></b>
													</h4>
												</div>
											</div>
											<div class="table-detail table-actions-bar">
												<a href="javsscript:void(0);" class="table-action-btn pull-right" id="delete_block_item" data-position="home_lowest_fares" data-offerblockid="<?php echo $item->id;?>" data-spinner-color="#7e57c2" data-spinner-size="15px" data-spinner-lines="8" class="table-action-btn"><i class="md md-delete"></i></a>
											</div>
										</div>
									</div>
									<?php endforeach; else:?>
										<p class="m-t-0 m-b-0">No lowest fare list found</p>
									<?php endif;?>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<h4 class="m-t-0 m-b-30 header-title">SEO</h4>
									<hr>
									<div class="form-group row">
										<label for="currency_unit" class="col-12 col-form-label">Site Title <span class="text-danger">*</span></label>
										<div class="col-12">
											<?php echo form_input($title);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="currency_unit" class="col-12 col-form-label">Site Description <span class="text-danger">*</span></label>
										<div class="col-12">
											<?php echo form_textarea($description);?>
										</div>
									</div>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div> <!-- end col -->
					<div class="col-3">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<a class="btn btn-default btn-block waves-effect waves-light" onclick="document.email_config.submit()">Save</a>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
	<!-- sample modal content -->
        <div id="result"></div>
	<script>
	$(function(){
		$('.load_modal').click(function(){
			$('#result').empty();
			var modaluri = $(this).data('modaluri');
			var target = $(this).data('target');

			var form_data = new FormData(); 
			form_data.append("modaluri", modaluri)

			var url = "<?php echo base_url();?>admin/appearance/ajax_load_modal";
			$.ajax({
				url: url,
				type : "POST",
				async:"false",
				dataType: "html",
				cache:false,
				contentType: false,
				processData: false,
				data: form_data, // serializes the form's elements.
				success : function (data){
					$('#result').html(data);
					$('.selectpicker').selectpicker('refresh');
					$(target).modal('show');
				}
			});
		});
	});
	$(document).on("click", "#add_block_item", function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
		e.stopPropagation();
	 	var l = Ladda.create(this);
	 	l.start();
		var block_title = $(".modal-body #block_title").val();
		var term_id = $(".modal-body #term_id").val();
		var position = $(".modal-body #position").val();
		var modalid = $(this).data('modalid');
		
		var form_data = new FormData(); 
		form_data.append("block_title", block_title)
		form_data.append("term_id", term_id)
		form_data.append("position", position)
		var url = "<?php echo base_url();?>admin/appearance/ajax_add_block_item";

		$.ajax({
			url: url,
			type: "POST",
			async:"false",
			dataType: "html",
			cache:false,
			contentType: false,
			processData: false,
			data: form_data, // serializes the form's elements.
			success: function(data)
			{				
				data = JSON.parse(data);
				if(data.response == "yes")
				{
					l.stop();
					$('.' + position).html(data.content);
					$(modalid).modal('hide');
					$.Notification.autoHideNotify('custom', 'top right', 'Success', data.message);
				}
			}
		});
	});
	$(document).on("click", "#delete_block_item", function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
		e.stopPropagation();
	 	var l = Ladda.create(this);
	 	l.start();
		var block_id = $(this).data('offerblockid');
		var position = $(this).data('position');
		
		var form_data = new FormData();
		form_data.append("block_id", block_id)
		form_data.append("position", position)
		var url = "<?php echo base_url();?>admin/appearance/ajax_delete_block_item";

		$.ajax({
			url: url,
			type: "POST",
			async:"false",
			dataType: "html",
			cache:false,
			contentType: false,
			processData: false,
			data: form_data, // serializes the form's elements.
			success: function(data)
			{				
				data = JSON.parse(data);
				if(data.response == "yes")
				{
					l.stop();
					$('.' + position).html(data.content);
					$.Notification.autoHideNotify('error', 'top right', 'Success', data.message);
				}
			}
		});
	});
	</script>
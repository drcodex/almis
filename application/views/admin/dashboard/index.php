	<!-- Start content -->
	<div class="content dashboard">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item active">Dashboard</li>
						</ol>
						<h5 class="page-title">Dashboard</h5>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-xl-4">
					<div class="card-box">
						<div class="bar-widget">
							<div class="table-box">
								<div class="table-detail">
									<div class="iconbox bg-info">
										<i class="icon-earphones-alt"></i>
									</div>
								</div>

								<div class="table-detail">
									<h5 class="m-t-0 m-b-5"><b>Inquiries</b></h5>
									<p class="text-muted m-b-0 m-t-0"><a href="<?php echo base_url();?>admin/bookings/inquires" class="text-danger waves-effect waves-light">see more <i class="dripicons-dots-3"></i></a></p>
								</div>
								<div class="table-detail text-right">
									<span class="dash-tokens"><?php echo $count_inquiries;?></span>
								</div>

							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-12 col-xl-4">
					<div class="card-box">
						<div class="bar-widget">
							<div class="table-box">
								<div class="table-detail">
									<div class="iconbox bg-custom">
										<i class="icon-rocket"></i>
									</div>
								</div>

								<div class="table-detail">
									<h5 class="m-t-0 m-b-5"><b>Flights</b></h5>
									<p class="text-muted m-b-0 m-t-0"><a href="<?php echo base_url();?>admin/flights" class="text-danger waves-effect waves-light">see more <i class="dripicons-dots-3"></i></a></p>
								</div>
								<div class="table-detail text-right">
									<span class="dash-tokens"><?php echo $count_flights;?></span>
								</div>

							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-12 col-xl-4">
					<div class="card-box">
						<div class="bar-widget">
							<div class="table-box">
								<div class="table-detail">
									<div class="iconbox bg-danger">
										<i class="icon-plane"></i>
									</div>
								</div>

								<div class="table-detail">
									<h5 class="m-t-0 m-b-5"><b>Airlines</b></h5>
									<p class="text-muted m-b-0 m-t-0"><a href="<?php echo base_url();?>admin/airlines" class="text-danger waves-effect waves-light">see more <i class="dripicons-dots-3"></i></a></p>
								</div>
								<div class="table-detail text-right">
									<span class="dash-tokens"><?php echo $count_airlines;?></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-xl-4">
					<div class="card-box">
						<div class="bar-widget">
							<div class="table-box">
								<div class="table-detail">
									<div class="iconbox bg-pink">
										<i class="icon-location-pin"></i>
									</div>
								</div>

								<div class="table-detail">
									<h5 class="m-t-0 m-b-5"><b>Destinations</b></h5>
									<p class="text-muted m-b-0 m-t-0"><a href="<?php echo base_url();?>admin/destinations" class="text-danger waves-effect waves-light">see more <i class="dripicons-dots-3"></i></a></p>
								</div>
								<div class="table-detail text-right">
									<span class="dash-tokens"><?php echo $count_destinations;?></span>
								</div>

							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-12 col-xl-4">
					<div class="card-box">
						<div class="bar-widget">
							<div class="table-box">
								<div class="table-detail">
									<div class="iconbox bg-warning">
										<i class="icon-map"></i>
									</div>
								</div>

								<div class="table-detail">
									<h5 class="m-t-0 m-b-5"><b>Hajj Packages</b></h5>
									<p class="text-muted m-b-0 m-t-0"><a href="<?php echo base_url();?>admin/packages/hajj" class="text-danger waves-effect waves-light">see more <i class="dripicons-dots-3"></i></a></p>
								</div>
								<div class="table-detail text-right">
									<span class="dash-tokens"><?php echo $count_hajj_packages;?></span>
								</div>

							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-12 col-xl-4">
					<div class="card-box">
						<div class="bar-widget">
							<div class="table-box">
								<div class="table-detail">
									<div class="iconbox bg-success">
										<i class="icon-map"></i>
									</div>
								</div>

								<div class="table-detail">
									<h5 class="m-t-0 m-b-5"><b>Umrah Packages</b></h5>
									<p class="text-muted m-b-0 m-t-0"><a href="<?php echo base_url();?>admin/packages/umrah" class="text-danger waves-effect waves-light">see more <i class="dripicons-dots-3"></i></a></p>
								</div>
								<div class="table-detail text-right">
									<span class="dash-tokens"><?php echo $count_umrah_packages;?></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- container -->
	</div> <!-- content -->
	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item active">Inquires</li>
						</ol>
						<h4 class="page-title">Inquires</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="card-box table-responsive">
						<table class="table table-bordered">
							<thead>
							<tr>
								<th>Title</th>
								<th>Details</th>
							</tr>
							</thead>
							<tbody>
								<tr>
									<td>Full Name</td>
									<td><?php echo $single_booking->first_name;?></td>
								</tr>
								<tr>
									<td>Email</td>
									<td><?php echo $single_booking->email;?></td>
								</tr>
								<tr>
									<td>Phone</td>
									<td><?php echo $single_booking->phone;?></td>
								</tr>
								<tr>
									<td>Flight From</td>
									<td><?php echo $single_booking->flight_from;?></td>
								</tr>
								<tr>
									<td>Flight To</td>
									<td><?php echo $single_booking->flight_to;?></td>
								</tr>
								<tr>
									<td>Departure Date</td>
									<td><?php echo $single_booking->takeoff_time;?></td>
								</tr>
								<tr>
									<td>Arrival Date</td>
									<td><?php echo $single_booking->landing_time;?></td>
								</tr>
								<tr>
									<td>Adults</td>
									<td><?php echo $single_booking->adults;?></td>
								</tr>
								<tr>
									<td>Kids</td>
									<td><?php echo $single_booking->kids;?></td>
								</tr>
								<tr>
									<td>Infants</td>
									<td><?php echo $single_booking->infants;?></td>
								</tr>
								<tr>
									<td>Type</td>
									<td><?php echo $single_booking->type;?></td>
								</tr>
								<tr>
									<td>Aircraft Class</td>
									<td><?php echo $single_booking->class;?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div> <!-- container -->
	</div> <!-- content -->
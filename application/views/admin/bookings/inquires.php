	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item active">Inquires</li>
						</ol>
						<h4 class="page-title">Inquires</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="card-box table-responsive">
						<table id="datatable" class="table table-hover mails m-0 table table-actions-bar">
							<thead>
							<tr>
								<th>Full Name</th>
								<th>Phone</th>
								<th>Flight</th>
								<th>Departure</th>
								<th>Arrival</th>
								<th>Action</th>
							</tr>
							</thead>
							<tbody>
								<?php if(!empty($get_bookings)): foreach ($get_bookings as $booking): ?>
								<tr>
									<td><?php echo $booking->first_name;?></td>
									<td><?php echo $booking->phone;?></td>
									<td><?php echo $booking->flight_from;?> to <?php echo $booking->flight_to;?></td>
									<td><?php echo $booking->takeoff_time;?></td>
									<td><?php echo $booking->landing_time;?></td>
									<td><?php echo anchor("admin/bookings/view_inquiry/".$booking->id, '<i class="md md-remove-red-eye"></i>', 'class="table-action-btn"') ;?></td>
								</tr>
								<?php endforeach; else:?>
								<tr>
									<td class="center text-center" colspan="6">No Records Found</td>
								</tr>
								<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div> <!-- container -->
	</div> <!-- content -->
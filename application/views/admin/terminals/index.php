<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item active">Terminals</li>
						</ol>
						<h4 class="page-title">Terminals</h4>
						<a href="<?php echo base_url();?>admin/terminals/new_terminal" class="btn btn-default waves-effect waves-light">Add New Terminal</a>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<?php if(isset($class)){?>
			<div id="infoMessage">
				<p class="<?php echo $class;?>"><?php echo $message;?></p>
			</div>
			<?php }else{?>
				<div id="infoMessage"><?php echo $message;?></div>
			<?php };?>
			<div class="row">
				<div class="col-12">
					<div class="card-box table-responsive">
						<table id="datatable" class="table table-hover mails m-0 table table-actions-bar">
							<thead>
								<tr>
									<th>Short Code</th>
									<th>Terminal</th>
									<th>City</th>
									<th>Country</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($all_terminals)): foreach ($all_terminals as $terminal): ?>
								<tr>
									<td><?php echo $terminal->terminal_short_code;?></td>
									<td><?php echo $terminal->terminal_name;?> (<?php echo $terminal->terminal_short_code;?>)</td>
									<td><?php echo $terminal->city_name;?></td>
									<td><?php echo $terminal->country_name;?></td>
									<td><?php echo anchor("admin/terminals/edit_terminal/".$terminal->terminal_id, '<i class="md md-edit"></i>', 'class="table-action-btn"') ;?><?php echo anchor("admin/terminals/delete_terminal/".$terminal->terminal_id, '<i class="md md-delete"></i>', 'class="table-action-btn"') ;?></td>
								</tr>
								<?php endforeach; else:?>
								<tr>
									<td class="center text-center" colspan="6">No Records Found</td>
								</tr>
								<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div> <!-- end row -->
		</div> <!-- container -->
	</div> <!-- content -->
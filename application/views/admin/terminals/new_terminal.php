<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header-2">
						<ol class="breadcrumb pull-right mb-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/terminals/all_terminals">Terminals</a></li>
							<li class="breadcrumb-item active">Add New Terminal</li>
						</ol>
						<h4 class="page-title">Add New Terminal</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<form id="form" name="editflight" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-12">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<div class="form-group row">
										<label for="terminal_name" class="col-lg-12 col-md-12 col-sm-12 col-form-label">Terminal Name <span class="text-danger">*</span></label>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<?php echo form_input($terminal_name);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="terminal_short_code" class="col-lg-12 col-md-12 col-sm-12 col-form-label">Terminal Code <span class="text-danger">*</span></label>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<?php echo form_input($terminal_short_code);?>
										</div>
									</div>
									<div class="form-group row">
										<label for="city_country" class="col-lg-12 col-md-12 col-sm-12 col-form-label">Terminal City <span class="text-danger">*</span></label>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<select name="city_country" class="selectpicker" data-live-search="true"  data-style="btn-white">
												<option selected="true" disabled="disabled">-- Select city --</option>
											<?php if(!empty($all_cities)): foreach ($all_cities as $cities): ?>
												<option value="<?php echo $cities->id;?>|<?php echo $cities->country_id;?>"><?php echo $cities->city_name;?>, <?php echo $cities->country_name;?></option>
												<?php endforeach; else:?>
												<option>No Records Found</option>
												<?php endif;?>
											</select>
										</div>
									</div>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div><!-- end col -->
					<div class="col-lg-3 col-md-3 col-sm-12">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<a href="#" class="btn btn-default btn-block waves-effect waves-light" onclick="document.editflight.submit()">Add</a>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			<?php echo form_close();?>
		</div> <!-- container -->
	</div> <!-- content -->
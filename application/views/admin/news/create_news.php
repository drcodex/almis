<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
		<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="page-title">News</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">News</a></li>
						<li class="breadcrumb-item active">Create News</li>
					</ol>
				</div>
			</div>
			<div id="infoMessage"><?php echo $message;?></div>
			<form id="form" method="post" class="form-horizontal">
				<div class="row">
					<div class="col-9">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<div class="p-20">
										<div class="form-group row">
											<label for="news_title" class="col-2 col-form-label">News Title</label>
											<div class="col-10">
												<?php echo form_input($news_title);?>
											</div>
										</div>
										<div class="form-group row">
											<label for="news_slug" class="col-2 col-form-label">News Slug</label>
											<div class="col-10">
												<?php echo form_input($news_slug);?>
											</div>
										</div>
										<div class="form-group row">
											<label for="news_details" class="col-2 col-form-label">News Details</label>
											<div class="col-10">
												<?php echo form_textarea($news_details);?>
											</div>
										</div>
										<!--<div class="form-group row">
											<label for="news_seo_title" class="col-2 col-form-label">SEO Title</label>
											<div class="col-10">
												<?php echo form_input($news_seo_title);?>
											</div>
										</div>
										<div class="form-group row">
											<label for="news_seo_title" class="col-2 col-form-label">SEO Slug</label>
											<div class="col-10">
												<?php echo form_input($news_seo_slug);?>
											</div>
										</div>
										<div class="form-group row">
											<label for="news_seo_title" class="col-2 col-form-label">SEO Description</label>
											<div class="col-10">
												<?php echo form_input($news_seo_description);?>
											</div>
										</div>-->
									</div>
								</div>
							</div><!-- end row -->
						</div> <!-- end card-box -->
					</div><!-- end col -->
					<div class="col-3">
						<div class="card-box">
							<div class="row">
								<div class="col-12">
									<div class="p-20">
										<input type="submit" value="Publish News" class="btn btn-primary">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			<?php echo form_close();?>
			<div class="row">
				<div class="col-9">
					<div class="card-box">
						<div class="row">
							<div class="col-md-12 portlets">
								<div class="m-b-30">
									<form action="#" class="dropzone" id="dropzone">
										<div class="fallback">
											<input name="file" type="file" multiple />
										</div>
									</form>
								</div>
							</div>
						</div>
					<!-- end row -->
					</div>
				</div>
			</div>
		</div> <!-- container -->
	</div> <!-- content -->
	<footer class="footer text-right">
		&copy; 2018. All rights reserved.
	</footer>
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
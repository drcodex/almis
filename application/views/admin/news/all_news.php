<div id="infoMessage"><?php echo $message;?></div>
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-8">
					<h4 class="page-title"><?php echo lang('index_heading');?></h4>
				</div>
				<div class="col-sm-4">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
						<li class="breadcrumb-item active">Users</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="card-box">
						<div class="row">
							<div class="col-12">
							<?php echo anchor('admin/news/create_news', lang('index_create_user_link'), 'class="btn btn-default btn-md waves-effect waves-light m-b-30"')?>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-hover mails m-0 table table-actions-bar">
								<thead>
									<tr>
										<th>Title</th>
										<th>Date</th>
										<th><?php echo lang('index_status_th');?></th>
										<th><?php echo lang('index_action_th');?></th>
									</tr>
								</thead>
								<tbody>
								<?php if(!empty($posts)): foreach ($posts as $post): ?>
									<tr>
										<td><a href="<?php base_url();?>edit_news/<?php echo $post->ID;?>"><?php echo $post->news_title;?></a></td>
										<td><?php echo $post->news_date;?></td>
										<td><?php echo $post->news_status;?></td>
										<td><?php echo anchor("admin/news/edit_news/".$post->ID, '<i class="md md-edit"></i>', 'class="table-action-btn"') ;?><?php echo anchor("admin/news/delete_news/".$post->ID, '<i class="md md-delete"></i>', 'class="table-action-btn"') ;?></td>
									</tr>
								<?php endforeach; else:?>
									<tr>
										<td class="center text-center" colspan="6">No Records Found</td>
									</tr>
								<?php endif;?>
								
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div> <!-- end col -->
			</div>
		</div> <!-- container -->
	</div> <!-- content -->
	<footer class="footer text-right">
		&copy; 2016 - 2018. All rights reserved.
	</footer>
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
// autocomplete functionality
if (jQuery('input#flight_from-autocomplete').length > 0) {
    jQuery('input#flight_from-autocomplete').typeahead({
      displayText: function(item) {
           return item.terminal_name
      },
      afterSelect: function(item) {
            this.$element[0].value = item.terminal_name;
            jQuery("input#flight_from-field-autocomplete").val(item.country_id);
      },
      source: function (query, process) {
        jQuery.ajax({
                url: baseurl + "ajax/fetch_from",
                data: {query:query},
                dataType: "json",
                type: "POST",
                success: function (data) {
                    process(data)
                }
            })
      }   
    });
}
if (jQuery('input#flight_to-autocomplete').length > 0) {
  jQuery('input#flight_to-autocomplete').typeahead({
    displayText: function(item) {
         return item.terminal_name
    },
    afterSelect: function(item) {
          this.$element[0].value = item.terminal_name;
          jQuery("input#flight_to-field-autocomplete").val(item.country_id);
    },
    source: function (query, process) {
      jQuery.ajax({
              url: baseurl + "ajax/fetch_to",
              data: {query:query},
              dataType: "json",
              type: "POST",
              success: function (data) {
                  process(data)
              }
          })
    }   
  });
}

